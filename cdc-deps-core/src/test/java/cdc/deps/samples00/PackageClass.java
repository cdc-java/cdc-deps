package cdc.deps.samples00;

class PackageClass {
    // public enum PublicNestedEnum {
    // }
    //
    // public static enum PublicNestedStaticEnum {
    // }
    //
    // protected enum ProtectedNestedEnum {
    // }
    //
    // protected static enum ProtectedNestedStaticEnum {
    // }
    //
    // private enum PrivateNestedEnum {
    // }
    //
    // private static enum PrivateNestedStaticEnum {
    // }
    //
    // enum PackageNestedEnum {
    // }
    //
    // static enum PackageNestedStaticEnum {
    // }

    // public class PublicNestedClass {
    // }

    // public static class PublicNestedStaticClass {
    // }
    //
    protected class ProtectedNestedClass {
        // final PublicClass.ProtectedNestedClass x0 = null;
        // final PublicClass.PackageNestedClass x1 = null;
        // final PublicClass.PackageNestedStaticClass x2 = null;
        // final PublicClass.ProtectedNestedStaticClass x3 = null;
    }
    //
    // protected static class ProtectedNestedStaticClass {
    // }
    //
    // private class PrivateNestedClass {
    // }
    //
    // private static class PrivateNestedStaticClass {
    // }
    //
    // class PackageNestedClass {
    // }
    //
    // static class PackageNestedStaticClass {
    // }
}