package cdc.deps.samples00;

public class PublicClass {
    // public enum PublicNestedEnum {
    // }
    //
    // public static enum PublicNestedStaticEnum {
    // }
    //
    // protected enum ProtectedNestedEnum {
    // }
    //
    // protected static enum ProtectedNestedStaticEnum {
    // }
    //
    // private enum PrivateNestedEnum {
    // }
    //
    // private static enum PrivateNestedStaticEnum {
    // }
    //
    // enum PackageNestedEnum {
    // }
    //
    // static enum PackageNestedStaticEnum {
    // }

    public class PublicNestedClass {
        // Ignore
    }

    public static class PublicNestedStaticClass {
        // Ignore
    }

    protected class ProtectedNestedClass {
        @SuppressWarnings("unused")
        private class PrivateNestedClass {
            // Ignore
        }
    }

    protected static class ProtectedNestedStaticClass {
        // Ignore
    }

    @SuppressWarnings("unused")
    private class PrivateNestedClass {
        // Ignore
    }

    @SuppressWarnings("unused")
    private static class PrivateNestedStaticClass {
        // Ignore
    }

    class PackageNestedClass {
        // Ignore
    }

    static class PackageNestedStaticClass {
        // Ignore
    }
}