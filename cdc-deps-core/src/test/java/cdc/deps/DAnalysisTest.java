package cdc.deps;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.deps.io.xml.DAnalysisXmlWriter;

class DAnalysisTest {

    private static final Logger LOGGER = LogManager.getLogger(DAnalysisTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void testGroup() {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g0 = analysis.createGroup("foo.jar", null);
        assertNotNull(g0);

        final DGroup ga = analysis.createGroup("A.class", analysis.getGroup("foo.jar"));
        assertNotNull(ga);
        assertEquals(ga.getParent(), g0);

        final DGroup gb = analysis.createGroup("B.class", analysis.getGroup("foo.jar"));
        assertNotNull(gb);
        assertEquals(gb.getParent(), g0);

        assertEquals(2, g0.getChildren().size());

        analysis.remove(gb);
        assertEquals(1, g0.getChildren().size());
        assertEquals(1, analysis.getRoots().size());

        analysis.remove(g0);
        assertEquals(0, analysis.getRoots().size());
    }

    @Test
    void testCategory() {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g = analysis.createGroup("foo.jar", null);
        assertNotNull(g);

        assertNull(g.getCategory());
        g.setCategory("CAT");
        assertEquals("CAT", g.getCategory());
    }

    @Test
    void testFeatures() {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g = analysis.createGroup("foo.jar", null);
        assertNotNull(g);

        assertTrue(g.getFeatures().isEmpty());
        g.setEnabled("F1", true);
        assertEquals(1, g.getFeatures().size());
        g.setEnabled("F1", false);
        assertEquals(0, g.getFeatures().size());
        g.setEnabled("F1", true);
        g.setEnabled("F1", true);
        g.setEnabled("F2", true);
        g.setEnabled("F2", true);
        assertEquals(2, g.getFeatures().size());
    }

    @Test
    void testRemoveGroup() {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g1 = analysis.createGroup("g1", null);
        assertNotNull(g1);
        assertTrue(g1.isRoot());

        final DGroup g2 = analysis.createGroup("g2", g1);
        assertNotNull(g2);
        assertFalse(g2.isRoot());

        final DGroup g3a = analysis.createGroup("g3a", g2);
        assertNotNull(g3a);
        assertFalse(g3a.isRoot());

        final DGroup g3b = analysis.createGroup("g3b", g2);
        assertNotNull(g3b);
        assertFalse(g3b.isRoot());

        final DDependency dep23a = analysis.addPrimitiveDependency(g2, g3a);
        assertNotNull(dep23a);
        assertEquals(1, analysis.getDependencies().size());

        final DDependency dep23b = analysis.addPrimitiveDependency(g2, g3b);
        assertNotNull(dep23b);
        assertEquals(2, analysis.getDependencies().size());

        final DDependency dep3a3b = analysis.addPrimitiveDependency(g3a, g3b);
        assertNotNull(dep3a3b);
        assertEquals(3, analysis.getDependencies().size());

        analysis.remove(g3a);
        assertEquals(3, analysis.getElements().size());
        assertEquals(1, analysis.getDependencies().size());
        assertEquals(1, dep23b.getTotalCount());

        analysis.remove(g1);
        assertEquals(0, analysis.getElements().size());
    }

    @Test
    void testCollapseGroup() {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g1 = analysis.createGroup("g1", null);
        assertNotNull(g1);
        assertTrue(g1.isRoot());

        final DGroup g2 = analysis.createGroup("g2", g1);
        assertNotNull(g2);
        assertFalse(g2.isRoot());

        final DGroup g3a = analysis.createGroup("g3a", g2);
        assertNotNull(g3a);
        assertFalse(g3a.isRoot());

        final DGroup g3b = analysis.createGroup("g3b", g2);
        assertNotNull(g3b);
        assertFalse(g3b.isRoot());

        final DDependency dep23a = analysis.addPrimitiveDependency(g2, g3a);
        assertNotNull(dep23a);
        assertEquals(1, analysis.getDependencies().size());
        assertEquals(1, dep23a.getTotalCount());

        final DDependency dep23b = analysis.addPrimitiveDependency(g2, g3b);
        assertNotNull(dep23b);
        assertEquals(2, analysis.getDependencies().size());
        assertEquals(1, dep23b.getTotalCount());

        final DDependency dep3a3b = analysis.addPrimitiveDependency(g3a, g3b);
        assertNotNull(dep3a3b);
        assertEquals(3, analysis.getDependencies().size());
        assertEquals(1, dep3a3b.getTotalCount());

        analysis.collapse(g3a);
        assertEquals(3, analysis.getElements().size());
        assertEquals(1, analysis.getDependencies().size());
        assertEquals(2, dep23b.getTotalCount());

        analysis.collapse(g1);
        assertEquals(1, analysis.getElements().size());
    }

    @Test
    void testPackageDerivedDependencies() throws IOException {
        final DAnalysis analysis = new DAnalysis();

        final DPackage p1 = analysis.createPackage("p1", null, null);
        assertNotNull(p1);
        final DPackage p2 = analysis.createPackage("p2", null, null);
        assertNotNull(p2);
        final DItem p1i1 = analysis.createItem("p1/I1", null, p1);
        assertNotNull(p1i1);
        assertEquals(p1, p1i1.getPackage());
        final DItem p2i1 = analysis.createItem("p2/I1", null, p2);
        assertNotNull(p2i1);
        assertEquals(p2, p2i1.getPackage());
        final DDependency p1i1p2i1 = analysis.addPrimitiveDependency(p1i1, p2i1);
        assertNotNull(p1i1p2i1);
        assertEquals(1, p1i1p2i1.getCount(DDependencyLevel.PRIMITIVE));
        assertEquals(0, p1i1p2i1.getCount(DDependencyLevel.DERIVED));
        assertEquals(1, analysis.getDependencies().size());

        analysis.createPackageDerivedDependencies();
        DAnalysisXmlWriter.write(analysis, OUT);
        assertEquals(2, analysis.getDependencies().size());
    }

    @Test
    void testGlobal() throws IOException {
        final DAnalysis analysis = new DAnalysis();
        assertNotNull(analysis);

        final DGroup g0 = analysis.createGroup("foo.jar", null);
        assertNotNull(g0);

        final DGroup g01 = analysis.createGroup("p.A.class", analysis.getGroup("foo.jar"));
        assertNotNull(g01);
        assertEquals(g01.getParent(), g0);

        final DGroup g02 = analysis.createGroup("p.B.class", analysis.getGroup("foo.jar"));
        assertNotNull(g02);
        assertEquals(g02.getParent(), g0);

        final DPackage p0 = analysis.createPackage("p", null, null);
        assertNotNull(p0);

        final DItem p0a = analysis.createItem("p.A", analysis.getGroup("p.A.class"), analysis.getPackage("p"));
        assertNotNull(p0a);
        assertEquals(p0a.getParent(), p0);
        assertTrue(p0a.getOutgoingDependencies().isEmpty());

        final DItem p0b = analysis.createItem("p.B", analysis.getGroup("p.B.class"), analysis.getPackage("p"));
        assertNotNull(p0b);
        assertEquals(p0b.getParent(), p0);

        final DDependency dab = analysis.addPrimitiveDependency(analysis.getItem("p.A"), analysis.getItem("p.B"));
        assertNotNull(dab);
        assertEquals(dab.getSource(), p0a);
        assertEquals(dab.getTarget(), p0b);
        // assertEquals(dab.getLevel(), DDependencyLevel.PRIMITIVE);
        assertTrue(p0a.getOutgoingDependencies().size() == 1);
        assertTrue(p0a.getOutgoingDependencies().contains(dab));

        final DItem i0 = analysis.createItem("i0", null, null);
        assertNotNull(i0);

        analysis.getItem("p.A").setEnabled("CLASS", true);
        analysis.getItem("p.A").setEnabled("DEPRECATED", true);

        DAnalysisDebug.print(analysis, OUT);

        DAnalysisXmlWriter.write(analysis, OUT);
    }
}