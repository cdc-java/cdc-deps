package cdc.deps.samples01;

public final class NestedClass {
    public static Nested instance = new Nested();

    private NestedClass() {
    }

    public static class Nested {

        public Nested() {
        }
    }
}