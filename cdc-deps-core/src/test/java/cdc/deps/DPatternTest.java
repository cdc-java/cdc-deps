package cdc.deps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DPatternTest {
    @Test
    void testPattern() {
        {
            final DPattern p = new DPattern("Cat:FEATURE");
            assertEquals("", p.getName());
            assertEquals("Cat", p.getCategory());
            assertEquals("FEATURE", p.getFeature());
            assertEquals("Cat:FEATURE", p.toString());
        }

        {
            final DPattern p = new DPattern("Cat:");
            assertEquals("", p.getName());
            assertEquals("Cat", p.getCategory());
            assertEquals("", p.getFeature());
            assertEquals("Cat:", p.toString());
        }

        {
            final DPattern p = new DPattern(":FEATURE");
            assertEquals("", p.getName());
            assertEquals("", p.getCategory());
            assertEquals("FEATURE", p.getFeature());
            assertEquals(":FEATURE", p.toString());
        }

        {
            final DPattern p = new DPattern("Name");
            assertEquals("Name", p.getName());
            assertEquals("", p.getCategory());
            assertEquals("", p.getFeature());
            assertEquals("Name", p.toString());
        }
    }
}