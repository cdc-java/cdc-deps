<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:key name="element-key" match="group|package|item" use="@name"/>
   <xsl:key name="item-ref-key" match="item-ref" use="@name"/>

   <xsl:template name="cdc-char-count">
      <xsl:param name="text"/>
      <xsl:param name="char"/>
      <xsl:value-of select="string-length($text) - string-length(translate($text, $char, ''))"/>
   </xsl:template>

   <xsl:template name="cdc-basename">
      <xsl:param name="path"/>
      <xsl:choose>
         <xsl:when test="contains($path, '/')">
            <xsl:call-template name="cdc-basename">
               <xsl:with-param name="path" select="substring-after($path, '/')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$path"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="up-path">
      <xsl:param name="path"/>
      <xsl:variable name="depth">
         <xsl:call-template name="cdc-char-count">
            <xsl:with-param name="text" select="$path"/>
            <xsl:with-param name="char" select="'/'"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:call-template name="build-up-path">
         <xsl:with-param name="depth" select="$depth"/>
         <xsl:with-param name="path" select="''"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="build-up-path">
      <xsl:param name="depth"/>
      <xsl:param name="path"/>
      <xsl:choose>
         <xsl:when test="$path = ''">
            <xsl:choose>
               <xsl:when test="$depth = 0">
                  <xsl:value-of select="'..'"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:call-template name="build-up-path">
                     <xsl:with-param name="depth" select="$depth - 1"/>
                     <xsl:with-param name="path" select="'..'"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
            <xsl:choose>
               <xsl:when test="$depth = 0">
                  <xsl:value-of select="concat($path, '/..')"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:call-template name="build-up-path">
                     <xsl:with-param name="depth" select="$depth - 1"/>
                     <xsl:with-param name="path" select="concat($path, '/..')"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="add-image">
      <xsl:param name="name"/>
      <xsl:param name="path" select="'./'"/>
      <xsl:variable name="category">
         <xsl:call-template name="get-category">
            <xsl:with-param name="name" select="$name"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="scope">
         <xsl:call-template name="get-scope">
            <xsl:with-param name="name" select="$name"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:call-template name="add-category-scope-image">
         <xsl:with-param name="category" select="$category"/>
         <xsl:with-param name="scope" select="$scope"/>
         <xsl:with-param name="path" select="$path"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="add-category-scope-image">
      <xsl:param name="category"/>
      <xsl:param name="scope"/>
      <xsl:param name="path" select="''"/>
      <xsl:element name="img">
         <xsl:attribute name="src">
         <xsl:variable name="c">
            <xsl:choose>
               <xsl:when test="$category != ''">
                  <xsl:call-template name="normalize-name">
                     <xsl:with-param name="string" select="$category"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:text>unknown</xsl:text>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>
         <xsl:variable name="s">
            <xsl:choose>
               <xsl:when test="$scope != ''">
                  <xsl:call-template name="normalize-name">
                     <xsl:with-param name="string" select="$scope"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:text>unknown</xsl:text>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>
         <xsl:value-of select="concat($path, 'images/', $c, '-', $s, '.png')"/>
         </xsl:attribute>
      </xsl:element>
   </xsl:template>

   <!-- Normalization of scope and category names -->
   <xsl:template name="normalize-name">
      <xsl:param name="string"/>
      <xsl:variable name="input" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ.'"/>
      <xsl:variable name="output" select="'abcdefghijklmnopqrstuvwxyz-'"/>
      <xsl:value-of select="translate($string, $input, $output)"/>
   </xsl:template>
   
   <!-- Normalization of relative paths -->
   <!-- Used to obtain a valid relative path -->
   <xsl:template name="normalize-relative-path">
      <xsl:param name="string"/>
      <xsl:variable name="input" select="':*?|&lt;&gt;&quot;'"/>
      <xsl:variable name="output" select="'-------'"/>
      <xsl:value-of select="translate($string, $input, $output)"/>
   </xsl:template>

   <xsl:template name="add-gv-image">
      <xsl:param name="name"/>
      <xsl:param name="path" required="no" select="''"/>
      <xsl:if test="$with-images != 'no'">
         <xsl:variable name="image-name">
            <xsl:call-template name="to-image-name">
               <xsl:with-param name="string" select="$name"/>
            </xsl:call-template>
         </xsl:variable>
         <xsl:element name="div">
            <xsl:attribute name="class">gv-image</xsl:attribute>
            <xsl:element name="img">
               <xsl:attribute name="src">
            <xsl:value-of select="concat($path, 'images/', $image-name, '.png')"/>
            </xsl:attribute>
               <xsl:attribute name="usemap">
               <xsl:value-of select="concat('#', $image-name)"/>
            </xsl:attribute>
            </xsl:element>
            <xsl:variable name="cmapx" select="document(concat($output-dir, '/images/', $image-name, '.cmapx'))/*"/>
            <xsl:copy-of select="$cmapx"/>
         </xsl:element>
      </xsl:if>
   </xsl:template>

   <xsl:template name="to-image-name">
      <xsl:param name="string"/>
      <xsl:variable name="input" select="'/:*?|&lt;&gt;&quot;'"/>
      <xsl:variable name="output" select="'--------'"/>
      <xsl:value-of select="translate($string, $input, $output)"/>
   </xsl:template>

   <!-- Return the number of displayed packages -->
   <xsl:template name="num-packages">
      <xsl:variable name="total" select="count(//package)"/>
      <xsl:variable name="num-internal" select="count(//package[@scope = 'INTERNAL'])"/>
      <xsl:variable name="num-external" select="count(//package[@scope = 'EXTERNAL'])"/>
      <xsl:variable name="num-unknown" select="$total - $num-internal - $num-external"/>
      <xsl:choose>
         <xsl:when test="$with-external = 'yes' and $with-unknown = 'yes'">
            <xsl:value-of select="$total"/>
         </xsl:when>
         <xsl:when test="$with-external = 'yes'">
            <xsl:value-of select="$num-internal + $num-external"/>
         </xsl:when>
         <xsl:when test="$with-unknown = 'yes'">
            <xsl:value-of select="$num-internal + $num-unknown"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$num-internal"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Return the number of displayed packages with a given scope -->
   <xsl:template name="num-scoped-packages">
      <xsl:param name="scope"/>
      <!-- Number of packages with scope -->
      <xsl:variable name="num-scoped" select="count(//package[@scope = $scope])"/>
      <xsl:choose>
         <xsl:when test="$scope = 'INTERNAL' or $scope = 'MIXED' or $scope = 'EXTERNAL' and $with-external = 'yes' or $scope = 'UNKNOWN' and $with-unknown = 'yes'">
            <xsl:value-of select="$num-scoped"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="0"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Return the number of visible items in packages with a given scope -->
   <xsl:template name="num-items-in-scoped-packages">
      <xsl:param name="scope"/>
      <!-- Number of INTERNAL items in packages with scope -->
      <xsl:variable name="num-internal" select="count(//package[@scope = $scope]/item[@scope = 'INTERNAL']) + count(//package[@scope = $scope]/item//item[@scope = 'INTERNAL'])"/>
      <!-- Number of visible EXTERNAL items in packages with scope -->
      <xsl:variable name="num-external">
         <xsl:choose>
            <xsl:when test="$with-external = 'yes'">
               <xsl:value-of select="count(//package[@scope = $scope]/item[@scope = 'EXTERNAL']) + count(//package[@scope = $scope]/item//item[@scope = 'EXTERNAL'])"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="0"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <!-- Number of visible UNKNOWN items in packages with scope (Should always be 0) -->
      <xsl:variable name="num-unknown">
         <xsl:choose>
            <xsl:when test="$with-unknown = 'yes'">
               <xsl:value-of select="count(//package[@scope = $scope]/item[@scope = 'UNKNOWN']) + count(//package[@scope = $scope]/item//item[@scope = 'UNKNOWN'])"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="0"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:value-of select="$num-internal + $num-external + $num-unknown"/>
   </xsl:template>


   <!-- Return the number of displayed items -->
   <!-- An Item can not have a MIXED scope -->
   <xsl:template name="num-items">
      <xsl:variable name="total" select="count(//item)"/>
      <xsl:variable name="num-internal" select="count(//item[@scope = 'INTERNAL'])"/>
      <xsl:variable name="num-external" select="count(//item[@scope = 'EXTERNAL'])"/>
      <xsl:variable name="num-unknown" select="$total - $num-internal - $num-external"/>
      <xsl:choose>
         <xsl:when test="$with-external = 'yes' and $with-unknown = 'yes'">
            <xsl:value-of select="$total"/>
         </xsl:when>
         <xsl:when test="$with-external = 'yes'">
            <xsl:value-of select="$num-internal + $num-external"/>
         </xsl:when>
         <xsl:when test="$with-unknown = 'yes'">
            <xsl:value-of select="$num-internal + $num-unknown"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$num-internal"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Return the number of displayed items with a given scope -->
   <!-- An Item can not have a MIXED scope (O is returned in that case) -->
   <xsl:template name="num-scoped-items">
      <xsl:param name="scope"/>
      <!-- Number of items with scope -->
      <xsl:variable name="num-scoped" select="count(//item[@scope = $scope])"/>
      <xsl:choose>
         <xsl:when test="$scope = 'INTERNAL' or $scope = 'EXTERNAL' and $with-external = 'yes' or $scope = 'UNKNOWN' and $with-unknown = 'yes'">
            <xsl:value-of select="$num-scoped"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="0"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Return the number of displayed items in a package -->
   <xsl:template name="num-package-items">
      <xsl:param name="package-name"/>
      <xsl:variable name="package" select="key('element-key', $package-name)"/>
      <xsl:variable name="total" select="count($package/item) + count($package/item//item)"/>
      <xsl:variable name="num-internal" select="count($package/item[@scope = 'INTERNAL']) + count($package/item//item[@scope = 'INTERNAL'])"/>
      <xsl:variable name="num-external" select="count($package/item[@scope = 'EXTERNAL']) + count($package/item//item[@scope = 'EXTERNAL'])"/>
      <xsl:variable name="num-unknown" select="$total - $num-internal - $num-external"/>
      <xsl:choose>
         <xsl:when test="$with-external = 'yes' and $with-unknown = 'yes'">
            <xsl:value-of select="$total"/>
         </xsl:when>
         <xsl:when test="$with-external = 'yes'">
            <xsl:value-of select="$num-internal + $num-external"/>
         </xsl:when>
         <xsl:when test="$with-unknown = 'yes'">
            <xsl:value-of select="$num-internal + $num-unknown"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$num-internal"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- Return the number of displayed items in a group -->
   <xsl:template name="num-group-items">
      <xsl:param name="group-name"/>
      <xsl:variable name="group" select="key('element-key', $group-name)"/>
      <xsl:variable name="total" select="count($group//item-ref)"/>
      <xsl:value-of select="$total"/>
   </xsl:template>
   
   <xsl:template name="num-group-packages">
      <xsl:param name="group-name"/>
      <xsl:variable name="group" select="key('element-key', $group-name)"/>
      <xsl:variable name="total" select="count($group/package-content)"/>
      <xsl:value-of select="$total"/>
   </xsl:template>

   <!-- @param name Name of the searched element (group, package or item) -->
   <!-- @return The category of the element -->
   <xsl:template name="get-category">
      <xsl:param name="name"/>
      <xsl:variable name="elt" select="key('element-key', $name)"/>
      <xsl:choose>
         <xsl:when test="name($elt) = 'item' or name($elt) = 'package' or name($elt) = 'group'">
            <xsl:value-of select="$elt/@category"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'ERROR'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- @param name Name of the searched element (group, package or item) -->
   <!-- @return The scope of the element -->
   <xsl:template name="get-scope">
      <xsl:param name="name"/>

      <xsl:variable name="elt" select="key('element-key', $name)"/>
      <xsl:choose>
         <xsl:when test="name($elt) = 'item' or name($elt) = 'package' or name($elt) = 'group'">
            <xsl:value-of select="$elt/@scope"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'ERROR'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="get-kind">
      <xsl:param name="name"/>
      <xsl:variable name="elt" select="key('element-key', $name)"/>
      <xsl:choose>
         <xsl:when test="name($elt) = 'item' or name($elt) = 'package' or name($elt) = 'group'">
            <xsl:value-of select="name($elt)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'ERROR'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>


   <!-- A MIXED scope is always accepted -->
   <xsl:template name="accepts-scope">
      <xsl:param name="scope"/>
      <xsl:choose>
         <xsl:when test="$scope = 'EXTERNAL'">
            <xsl:choose>
               <xsl:when test="$with-external != 'yes'">
                  <xsl:value-of select="'no'"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="'yes'"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:when>
         <xsl:when test="($scope = 'UNKNOWN') or not($scope)">
            <xsl:choose>
               <xsl:when test="$with-unknown != 'yes'">
                  <xsl:value-of select="'no'"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="'yes'"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'yes'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="accepts-item">
      <xsl:param name="name"/>
      <xsl:call-template name="accepts-scope">
         <xsl:with-param name="scope">
            <xsl:call-template name="get-scope">
               <xsl:with-param name="name" select="$name"/>
            </xsl:call-template>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>


   <!-- Returns the name of the root group containing an item -->
   <!-- @param name Name of the item -->
   <xsl:template name="get-item-root-group-name">
      <xsl:param name="name"/>
      <xsl:for-each select="key('item-ref-key', $name)/ancestor-or-self::group">
         <xsl:if test="name(..) != 'group'">
            <xsl:value-of select="@name"/>
         </xsl:if>
      </xsl:for-each>
   </xsl:template>

</xsl:stylesheet>