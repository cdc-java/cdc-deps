package cdc.deps.io.xml;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.DAnalysisInfo;
import cdc.deps.DDependency;
import cdc.deps.DDependencyLevel;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DNamespace;
import cdc.deps.DPackage;
import cdc.deps.graphs.CyclePosition;
import cdc.deps.graphs.DAnalysisCycles;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.InvalidStateException;

/**
 * Utility class used to save an analysis to an XML file.
 * <p>
 * We write more things than strictly necessary to facilitate xslt post processing.
 *
 *
 * @author Damien Carbonne
 *
 */
public final class DAnalysisXmlWriter {
    private static final Logger LOGGER = LogManager.getLogger(DAnalysisXmlWriter.class);

    private final DAnalysis analysis;
    private final DAnalysisCycles cycles;
    private final XmlWriter writer;

    private DAnalysisXmlWriter(DAnalysis analysis,
                               XmlWriter writer) {
        this.analysis = analysis;
        this.cycles = new DAnalysisCycles(analysis,
                                          DAnalysisCycles.Feature.ITEMS,
                                          DAnalysisCycles.Feature.PACKAGES,
                                          DAnalysisCycles.Feature.ROOT_GROUPS);
        this.writer = writer;
    }

    public static void write(DAnalysis analysis,
                             XmlWriter writer) throws IOException {
        final DAnalysisXmlWriter w = new DAnalysisXmlWriter(analysis, writer);
        w.write();
    }

    public static void write(DAnalysis analysis,
                             File file) throws IOException {
        try (final XmlWriter writer = new XmlWriter(file, "UTF-8")) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            write(analysis, writer);
        }
    }

    public static void write(DAnalysis analysis,
                             String filename) throws IOException {
        write(analysis, new File(filename));
    }

    public static void write(DAnalysis analysis,
                             PrintStream out) throws IOException {
        try (final XmlWriter writer = new XmlWriter(out, "UTF-8")) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            write(analysis, writer);
        }
    }

    private void write() throws IOException {
        try {
            writer.beginDocument();
            writer.beginElement(DAnalysisXml.DEPS);
            writer.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.addAttribute("xsi:noNamespaceSchemaLocation", "cdc-deps.xsd");

            writeInfo(analysis.getInfo());

            // Write root groups
            for (final DGroup group : analysis.getSortedRoots(DGroup.class)) {
                writeGroupDef(group);
            }

            // Write root packages
            for (final DPackage pack : analysis.getSortedRoots(DPackage.class)) {
                writePackageDef(pack);
            }

            // Writes root items
            for (final DItem item : analysis.getSortedRoots(DItem.class)) {
                writeItemDef(item);
            }
            writer.endElement();
            writer.endDocument();
        } catch (final InvalidStateException e) {
            LOGGER.catching(e);
        }
    }

    private void writeInfo(DAnalysisInfo info) throws IOException {
        writer.beginElement(DAnalysisXml.INFO);
        if (info.getCreationDate() != null) {
            writer.addAttribute(DAnalysisXml.CREATION_DATE, DAnalysisInfo.toString(info.getCreationDate()));
        }
        writer.beginElement(DAnalysisXml.ANALYZER);
        writer.addAttribute(DAnalysisXml.NAME, info.getAnalyzerName());
        for (final DAnalysisInfo.Argument arg : info.getAnalysisArguments()) {
            writer.beginElement(DAnalysisXml.ARG);
            writer.addAttribute(DAnalysisXml.NAME, arg.getName());
            if (arg.getValue() != null) {
                writer.addAttribute(DAnalysisXml.VALUE, arg.getValue());
            }
            writer.endElement();
        }
        writer.endElement();
        writer.endElement();
    }

    private void addElementAttributes(DElement element) throws IOException {
        // Name
        writer.addAttribute(DAnalysisXml.NAME, element.getName());
        // Scope
        if (element.getScope() != null) {
            writer.addAttribute(DAnalysisXml.SCOPE, element.getScope().name());
        } else {
            writer.addAttribute(DAnalysisXml.SCOPE, DElementScope.UNKNOWN.name());
        }
        // Category
        if (element.getCategory() != null && element.getCategory().length() > 0) {
            writer.addAttribute(DAnalysisXml.CATEGORY, element.getCategory());
        }
        // Features
        final Set<String> features = element.getFeatures();
        if (features != null && !features.isEmpty()) {
            final StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (final String feature : features) {
                if (!first) {
                    builder.append(DAnalysisXml.SEPARATOR);
                }
                builder.append(feature);
                first = false;
            }
            writer.addAttribute(DAnalysisXml.FEATURES, builder.toString());
        }
        // Cycle
        final CyclePosition cyclePosition = cycles.getCyclePosition(element);
        if (cyclePosition != CyclePosition.NO_CYCLE) {
            writer.addAttribute(DAnalysisXml.CYCLE_POSITION, cyclePosition.name());
        }
    }

    /**
     * Write recursively a group definition.
     *
     * @param group The group.
     * @throws IOException When an I/O exception occurs.
     */
    private void writeGroupDef(DGroup group) throws IOException {
        writer.beginElement(DAnalysisXml.GROUP);
        addElementAttributes(group);
        // Children
        for (final DElement item : group.getSortedChildren(DElement.class)) {
            if (item.getKind() == DElementKind.GROUP) {
                writeGroupDef((DGroup) item);
            } else {
                writeElementRef(item);
            }
        }
        writeDependencies(group);
        writeUsage(group);
        if (group.isRoot()) {
            writePackageContent(group);
        }
        writer.endElement();
    }

    /**
     * Write a reference to an element.
     *
     * @param element The element.
     * @throws IOException When an I/O exception occurs.
     */
    private void writeElementRef(DElement element) throws IOException {
        if (element != null) {
            switch (element.getKind()) {
            case GROUP:
                writer.beginElement(DAnalysisXml.GROUP_REF);
                break;
            case ITEM:
                writer.beginElement(DAnalysisXml.ITEM_REF);
                break;
            case PACKAGE:
                writer.beginElement(DAnalysisXml.PACKAGE_REF);
                break;
            default:
                break;
            }
            writer.addAttribute(DAnalysisXml.NAME, element.getName());
            writer.endElement();
        }
    }

    private void writeRootGroupRef(DGroup group) throws IOException {
        if (group != null) {
            writer.beginElement(DAnalysisXml.ROOT_GROUP_REF);
            writer.addAttribute(DAnalysisXml.NAME, group.getName());
            writer.endElement();
        }
    }

    /**
     * Write recursively a package definition.
     *
     * @param p The package.
     * @throws IOException When an I/O exception occurs.
     */
    private void writePackageDef(DPackage p) throws IOException {
        writer.beginElement(DAnalysisXml.PACKAGE);
        addElementAttributes(p);
        writeElementRef(p.getOwner());
        for (final DGroup group : p.getSortedItemRootGroups()) {
            writeRootGroupRef(group);
        }
        // Children
        for (final DNamespace ns : p.getSortedChildren(DNamespace.class)) {
            final DElement element = ns;
            if (element.getKind() == DElementKind.PACKAGE) {
                writePackageDef((DPackage) element);
            } else if (element.getKind() == DElementKind.ITEM) {
                writeItemDef((DItem) element);
            }
        }
        writeDependencies(p);
        writeUsage(p);
        writer.endElement();
    }

    /**
     * Write recursively an item definition.
     *
     * @param item The item.
     * @throws IOException When an I/O exception occurs.
     */
    private void writeItemDef(DItem item) throws IOException {
        writer.beginElement(DAnalysisXml.ITEM);
        addElementAttributes(item);
        writeElementRef(item.getOwner());
        writeRootGroupRef(item.getRootOwner());
        // Children
        for (final DNamespace ns : item.getSortedChildren(DNamespace.class)) {
            final DElement element = ns;
            if (element.getKind() == DElementKind.ITEM) {
                writeItemDef((DItem) element);
            }
        }
        writeDependencies(item);
        writeUsage(item);
        writer.endElement();
    }

    /**
     * Write dependencies of an element.
     *
     * @param element The element.
     * @throws IOException When an I/O exception occurs.
     */
    private void writeDependencies(DElement element) throws IOException {
        final List<DDependency> deps = new ArrayList<>();
        deps.addAll(element.getOutgoingDependencies());
        Collections.sort(deps, DDependency.TARGET_NAME_COMPARATOR);
        for (final DDependency dep : deps) {
            final DElement target = dep.getTarget();
            writer.beginElement(DAnalysisXml.DEP);
            if (dep.getCount(DDependencyLevel.PRIMITIVE) != 0) {
                writer.addAttribute(DAnalysisXml.PRIMITIVE_COUNT, dep.getCount(DDependencyLevel.PRIMITIVE));
            }
            if (dep.getCount(DDependencyLevel.DERIVED) != 0) {
                writer.addAttribute(DAnalysisXml.DERIVED_COUNT, dep.getCount(DDependencyLevel.DERIVED));
            }
            writer.addAttribute(DAnalysisXml.TARGET, target.getName());
            writer.addAttribute(DAnalysisXml.TARGET_POSITION, dep.getTargetRelativePosition().name());
            writer.endElement();
        }
    }

    private void writeUsage(DElement element) throws IOException {
        final List<DDependency> deps = new ArrayList<>();
        deps.addAll(element.getIngoingDependencies());
        Collections.sort(deps, DDependency.SOURCE_NAME_COMPARATOR);
        for (final DDependency dep : deps) {
            final DElement source = dep.getSource();
            writer.beginElement(DAnalysisXml.USE);
            if (dep.getCount(DDependencyLevel.PRIMITIVE) != 0) {
                writer.addAttribute(DAnalysisXml.PRIMITIVE_COUNT, dep.getCount(DDependencyLevel.PRIMITIVE));
            }
            if (dep.getCount(DDependencyLevel.DERIVED) != 0) {
                writer.addAttribute(DAnalysisXml.DERIVED_COUNT, dep.getCount(DDependencyLevel.DERIVED));
            }
            writer.addAttribute(DAnalysisXml.SOURCE, source.getName());
            writer.addAttribute(DAnalysisXml.SOURCE_POSITION, dep.getSourceRelativePosition().name());
            writer.endElement();
        }
    }

    private void writePackageContent(DGroup group) throws IOException {
        final Map<DPackage, Integer> stats = group.getItemsCount();
        final List<DPackage> packages = new ArrayList<>();
        packages.addAll(stats.keySet());
        Collections.sort(packages, DElement.NAME_COMPARATOR);

        for (final DPackage p : packages) {
            writer.beginElement("package-content");
            writer.addAttribute(DAnalysisXml.NAME, p.getName());
            writer.addAttribute("local-items", stats.get(p));
            writer.addAttribute("total-items", p.getDeepContentCount(DItem.class));
            writer.endElement();
        }
    }
}