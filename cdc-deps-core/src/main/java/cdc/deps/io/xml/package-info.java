/**
 * XML related utilities.
 *
 * @author Damien Carbonne
 */
package cdc.deps.io.xml;