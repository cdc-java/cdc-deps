package cdc.deps.io.xml;

final class DAnalysisXml {
    static final String ANALYZER = "analyzer";
    static final String ARG = "arg";
    static final String CATEGORY = "category";
    static final String CREATION_DATE = "creation-date";
    static final String CYCLE_POSITION = "cycle-position";
    static final String DEP = "dep";
    static final String DEPS = "deps";
    static final String DERIVED_COUNT = "derived-count";
    static final String FEATURES = "features";
    static final String GROUP = "group";
    static final String GROUP_REF = "group-ref";
    static final String INFO = "info";
    static final String ITEM = "item";
    static final String ITEM_REF = "item-ref";
    static final String NAME = "name";
    static final String PACKAGE = "package";
    static final String PACKAGE_REF = "package-ref";
    static final String PRIMITIVE_COUNT = "primitive-count";
    static final String ROOT_GROUP_REF = "root-group-ref";
    static final String SCOPE = "scope";
    static final String SOURCE = "source";
    static final String SOURCE_POSITION = "source-position";
    static final String TARGET = "target";
    static final String TARGET_POSITION = "target-position";
    static final String USE = "use";
    static final String VALUE = "value";

    static final String SEPARATOR = " ";

    private DAnalysisXml() {
    }
}