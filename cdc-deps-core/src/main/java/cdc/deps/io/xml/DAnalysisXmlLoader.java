package cdc.deps.io.xml;

import java.util.Date;
import java.util.StringTokenizer;

import cdc.deps.DAnalysis;
import cdc.deps.DAnalysisInfo;
import cdc.deps.DDependency;
import cdc.deps.DDependencyLevel;
import cdc.deps.DElement;
import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DNamespace;
import cdc.deps.DPackage;
import cdc.io.data.Child;
import cdc.io.data.Element;
import cdc.io.data.NodeType;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.util.lang.FailureReaction;

public final class DAnalysisXmlLoader extends AbstractResourceLoader<DAnalysis> {
    private final DAnalysis analysis;

    public DAnalysisXmlLoader(DAnalysis analysis,
                              FailureReaction reaction) {
        super(reaction);
        this.analysis = analysis;
    }

    @Override
    protected DAnalysis loadRoot(Element root) {
        // First pass
        // Load definition of logical elements (packages and items),
        // recursively.
        for (final Child child : root.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element element = (Element) child;
                switch (element.getName()) {
                case DAnalysisXml.INFO:
                    parseInfo(element);
                    break;
                case DAnalysisXml.PACKAGE:
                    parsePackageDef(null, element);
                    break;
                case DAnalysisXml.ITEM:
                    parseItemDef(null, element);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }

        // Second pass
        // Load definition of physical elements (groups), recursively.
        // They may depend on namespaces (packages and items), which are already
        // loaded during first pass
        for (final Child child : root.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element element = (Element) child;
                if (DAnalysisXml.GROUP.equals(element.getName())) {
                    parseGroupDef(null, element);
                } else {
                    // Ignore
                }
            }
        }

        // Third pass
        // Load dependencies, recursively
        for (final Child child : root.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element element = (Element) child;
                switch (element.getName()) {
                case DAnalysisXml.GROUP:
                    parseGroupDeps(null, element);
                    break;
                case DAnalysisXml.PACKAGE:
                    parsePackageDeps(null, element);
                    break;
                case DAnalysisXml.ITEM:
                    parseItemDeps(null, element);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
        return analysis;
    }

    /**
     *
     * @param element The data element corresponding to info.
     */
    private void parseInfo(Element element) {
        if (element.hasAttribute(DAnalysisXml.CREATION_DATE)) {
            final Date date = DAnalysisInfo.toDate(element.getAttributeValue(DAnalysisXml.CREATION_DATE, null));
            analysis.getInfo().setCreationDate(date);
        }
        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                if (DAnalysisXml.ANALYZER.equals(e.getName())) {
                    parseAnalyzer(e);
                } else {
                    // Ignore
                }
            }
        }
    }

    private void parseAnalyzer(Element element) {
        analysis.getInfo().setAnalyzerName(element.getAttributeValue(DAnalysisXml.NAME, null));
        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                if (DAnalysisXml.ARG.equals(e.getName())) {
                    final String name = e.getAttributeValue(DAnalysisXml.NAME, null);
                    final String value = e.getAttributeValue(DAnalysisXml.VALUE, null);
                    analysis.getInfo().addAnalysisArgument(name, value);
                } else {
                    // Ignore
                }
            }
        }
    }

    /**
     * Parse a group definition. <br>
     * This will parse children groups and types, recursively.
     *
     * @param group The parent group. <em>May be null.</em>
     * @param element The data element corresponding to the group.
     */
    private void parseGroupDef(DGroup group,
                               Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DGroup g = analysis.createGroup(name, group);
        parseScope(g, element);
        parseCategory(g, element);
        parseFeatures(g, element);

        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                switch (e.getName()) {
                case DAnalysisXml.GROUP:
                    parseGroupDef(g, e);
                    break;
                case DAnalysisXml.PACKAGE_REF:
                    parsePackageRef(g, e);
                    break;
                case DAnalysisXml.ITEM_REF:
                    parseItemRef(g, e);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
    }

    private void parseGroupDeps(DGroup group,
                                Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DGroup g = analysis.getGroup(name);
        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                switch (e.getName()) {
                case DAnalysisXml.GROUP:
                    parseGroupDeps(g, e);
                    break;
                case DAnalysisXml.DEP:
                    parseDep(g, e);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
    }

    /**
     * Parse a package reference in a group.
     *
     * @param group The parent group.<em>Must NOT be null.</em>
     * @param element The data element corresponding to the package reference.
     */
    private void parsePackageRef(DGroup group,
                                 Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        analysis.findOrCreatePackage(name, group, null);
    }

    /**
     * Parse a type reference in a group.
     *
     * @param group The parent group.<em>Must NOT be null.</em>
     * @param element The data element corresponding to the type reference.
     */
    private void parseItemRef(DGroup group,
                              Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        analysis.findOrCreateItem(null, name, group);
    }

    /**
     * Parse a package definition, ignoring dependencies. <br>
     * This will parse children packages and types, recursively.
     *
     * @param parent The parent package. <em>May be null.</em>
     * @param element The data element corresponding to the package.
     */
    private void parsePackageDef(DPackage parent,
                                 Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DPackage p = analysis.createPackage(name, null, parent);
        parseScope(p, element);
        parseCategory(p, element);
        parseFeatures(p, element);

        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                switch (e.getName()) {
                case DAnalysisXml.PACKAGE:
                    parsePackageDef(p, e);
                    break;
                case DAnalysisXml.ITEM:
                    parseItemDef(p, e);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
    }

    /**
     * Parse package dependencies. This will also parse children packages and
     * items dependencies.
     *
     * @param parent The parent package. <em>May be null.</em>
     * @param element The data element corresponding to the package.
     */
    private void parsePackageDeps(DPackage parent,
                                  Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DPackage p = analysis.getPackage(name);
        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                switch (e.getName()) {
                case DAnalysisXml.PACKAGE:
                    parsePackageDeps(p, e);
                    break;
                case DAnalysisXml.ITEM:
                    parseItemDeps(p, e);
                    break;
                case DAnalysisXml.DEP:
                    parseDep(p, e);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
    }

    /**
     * Parse an item definition, ignoring dependencies. <br>
     * This will parse children items, recursively.
     *
     * @param parent The parent namespace (package or item).
     *            <em>May be null.</em>
     * @param element The data element corresponding to the item.
     */
    private void parseItemDef(DNamespace parent,
                              Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DItem item = analysis.createItem(name, null, parent);
        parseScope(item, element);
        parseCategory(item, element);
        parseFeatures(item, element);

        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                if (DAnalysisXml.ITEM.equals(e.getName())) {
                    parseItemDef(item, e);
                } else {
                    // Ignore
                }
            }
        }

    }

    /**
     * Parse item dependencies. This will also parse children items
     * dependencies.
     *
     * @param parent The parent namespace (package or type).
     *            <em>May be null.</em>
     * @param element The data element corresponding to the type.
     */
    private void parseItemDeps(DNamespace parent,
                               Element element) {
        final String name = element.getAttributeValue(DAnalysisXml.NAME, null);
        final DItem type = analysis.getItem(name);
        for (final Child child : element.getChildren()) {
            if (child.getType() == NodeType.ELEMENT) {
                final Element e = (Element) child;
                switch (e.getName()) {
                case DAnalysisXml.ITEM:
                    parseItemDeps(type, e);
                    break;
                case DAnalysisXml.DEP:
                    parseDep(type, e);
                    break;
                default:
                    // Ignore
                    break;
                }
            }
        }
    }

    /**
     * Parse a dependency definition.
     *
     * @param source The parent namespace (package or type), declaring the
     *            dependency. <em>Must NOT be null.</em>
     * @param element The data element corresponding to the dependency.
     */
    private void parseDep(DElement source,
                          Element element) {
        final String targetName = element.getAttributeValue(DAnalysisXml.TARGET, null);
        final DElement target = analysis.getElement(targetName);
        if (target == null) {
            onError("parseDep(" + element + ") FAILED to find target");
        } else if (target != source) {
            final DDependency dep = analysis.addDependency(source, target);
            assert dep != null;
            final int primitive = element.getAttributeAsInt(DAnalysisXml.PRIMITIVE_COUNT, 0);
            final int derived = element.getAttributeAsInt(DAnalysisXml.DERIVED_COUNT, 0);
            dep.setCount(DDependencyLevel.PRIMITIVE, primitive);
            dep.setCount(DDependencyLevel.DERIVED, derived);
        } else {
            onError("parseDep(" + element + ") FAILED self dependency");
        }
    }

    private static void parseScope(DElement delement,
                                   Element element) {
        final DElementScope scope = element.getAttributeAsEnum(DAnalysisXml.SCOPE, DElementScope.class, DElementScope.UNKNOWN);
        if (delement instanceof DItem) {
            ((DItem) delement).setScope(scope);
        } else if (delement instanceof DGroup) {
            ((DGroup) delement).setScope(scope);
        }
    }

    private static void parseCategory(DElement delement,
                                      Element element) {
        delement.setCategory(element.getAttributeValue(DAnalysisXml.CATEGORY, null));
    }

    private static void parseFeatures(DElement delement,
                                      Element element) {
        final String features = element.getAttributeValue(DAnalysisXml.FEATURES, null);
        if (features != null) {
            final StringTokenizer tokenizer = new StringTokenizer(features, DAnalysisXml.SEPARATOR);
            while (tokenizer.hasMoreTokens()) {
                final String feature = tokenizer.nextToken();
                delement.setEnabled(feature, true);
            }
        }
    }
}