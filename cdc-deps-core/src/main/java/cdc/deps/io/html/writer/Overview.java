package cdc.deps.io.html.writer;

import cdc.deps.io.html.model.DMain;

/**
 * Overview generator.
 *
 * @author Damien Carbonne
 */
class Overview extends MainFrame<DMain> {
    protected Overview(HtmlWriterGenerator context) {
        super(context,
              context.buildOverview());
    }
}