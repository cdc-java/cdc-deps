package cdc.deps.io.html.writer;

import java.io.IOException;

import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DImageSection;
import cdc.deps.io.html.model.DMain;
import cdc.deps.io.html.model.DMenuItem;
import cdc.deps.io.html.model.DName;
import cdc.deps.io.html.model.DSection;
import cdc.deps.io.html.model.DTableRow;
import cdc.deps.io.html.model.DTableSection;
import cdc.io.html.HtmlWriter;
import cdc.util.strings.StringUtils;

/**
 * Base class used to generate a main frame.
 * <p>
 * It contains:
 * <ul>
 * <li>A head (meta data)
 * <li>A menu
 * <li>A title
 * <li>A subtitle
 * <li>A list of sections.
 * </ul>
 *
 * @author Damien Carbonne
 * @param <D> The DMain specific type.
 */
public class MainFrame<D extends DMain> extends AbstractGenerator {
    protected final D data;
    protected final String upPath;

    protected MainFrame(HtmlWriterGenerator context,
                        D data,
                        String upPath) {
        super(context);
        this.data = data;
        this.upPath = upPath;
    }

    protected MainFrame(HtmlWriterGenerator context,
                        D data) {
        this(context, data, ".");
    }

    @Override
    public final void accept(HtmlWriter writer) throws IOException {
        writer.html();
        head(writer, data.getTitle().getText(), upPath);

        writer.body();
        header(writer);

        writer.h1().attrClass(Params.Classes.BAR);
        content(writer, data.getTitle());
        writer.end();

        main(writer);
        writer.end(); // body

        writer.end(); // html
    }

    private void header(HtmlWriter writer) throws IOException {
        writer.header().attrRole(Params.Roles.BANNER);
        writer.nav().attrRole(Params.Roles.NAVIGATION);

        writer.div().attrClass(Params.Classes.FIXED_NAV);
        writer.div().attrClass(Params.Classes.TOP_NAV);

        writer.ul().attrClass(Params.Classes.NAV_LIST);
        for (final DMenuItem item : data.getMenu().getItems()) {
            writer.li();
            if (item.isHighlighted()) {
                writer.attrClass(Params.Classes.NAV_BAR_CELL_REV);
            }
            content(writer, item);
            writer.end();
        }
        writer.end(); // ul

        writer.div().attrClass(Params.Classes.ABOUT);
        writer.div().attrStyle("margin-top: 14px;");
        writer.strong().text(context.margs.title).end();
        writer.end(); // div
        writer.end(); // div
        writer.end(); // div

        writer.div().attrClass(Params.Classes.SUB_NAV).end();
        writer.end(); // div

        noticeLogo(writer, upPath);
        writer.end(); // nav
        writer.end(); // header
    }

    private void main(HtmlWriter writer) throws IOException {
        writer.main().attrClass(Params.Classes.MAIN_CONTAINER).attrRole(Params.Roles.MAIN);

        writer.div().attrClass(Params.Classes.HEADER);
        writer.h2().attrClass(Params.Classes.TITLE);
        content(writer, data.getSubtitle());
        writer.end(); // h2
        writer.end(); // div

        writer.div().attrClass(Params.Classes.CONTENT_CONTAINER);
        sections(writer);
        writer.end(); // div

        writer.end(); // main
    }

    protected void sections(HtmlWriter writer) throws IOException {
        for (final DSection section : data.getSections()) {
            sectionTitle(writer, section.getTitle());
            if (section instanceof DImageSection) {
                final DImageSection s = (DImageSection) section;
                addGvImage(writer, s.getName(), upPath);
            } else if (section instanceof DTableSection) {
                final DTableSection s = (DTableSection) section;
                writer.table(); // TODO style
                if (!StringUtils.isNullOrEmpty(s.getTableClass())) {
                    writer.attrClass(s.getTableClass());
                }

                if (s.getHeader() != null) {
                    writer.thead();
                    writer.tr();
                    for (final DName name : s.getHeader().getCells()) {
                        writer.th();
                        content(writer, name);
                        writer.end(); // th
                    }
                    writer.end(); // tr
                    writer.end(); // thead
                }

                writer.tbody();
                for (final DTableRow row : s.getData()) {
                    writer.tr();
                    for (int index = 0; index < row.getCells().size(); index++) {
                        final DName name = row.getCells().get(index);
                        final String cls = s.getDataClass(index);
                        writer.td();
                        if (!StringUtils.isNullOrEmpty(cls)) {
                            writer.attrClass(cls);
                        }
                        content(writer, name);
                        writer.end(); // td
                    }
                    writer.end(); // tr
                }
                writer.end(); // tbody

                writer.end(); // table
            } else {
                writer.text("TODO");
                // TODO
            }
        }
    }

    protected void sectionTitle(HtmlWriter writer,
                                String title) throws IOException {
        writer.h3().text(title).end();
    }
}