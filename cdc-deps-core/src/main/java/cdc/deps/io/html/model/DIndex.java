package cdc.deps.io.html.model;

import java.util.ArrayList;
import java.util.List;

import cdc.deps.DElement;

/**
 * Description of an index.
 * <p>
 * It is composed of:
 * <ul>
 * <li>A <b>title</b>.
 * <li>A <b>menu</b>.
 * <li>A <b>subtitle</b>.
 * <li>A list of <b>index entries</b>.
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class DIndex<E extends DElement> {
    private final DName title;
    private final DMenu menu;
    private final DName subtitle;
    private final List<DIndexEntry<E>> entries = new ArrayList<>();

    protected DIndex(DName title,
                     DMenu menu,
                     DName subtitle,
                     List<DIndexEntry<E>> entries) {
        this.title = title;
        this.menu = menu;
        this.subtitle = subtitle;
        this.entries.addAll(entries);
    }

    public DName getTitle() {
        return title;
    }

    public DMenu getMenu() {
        return menu;
    }

    public DName getSubtitle() {
        return subtitle;
    }

    public List<DIndexEntry<E>> getEntries() {
        return entries;
    }

    public static <E extends DElement> Builder<E> builder(Class<E> cls) {
        return new Builder<>();
    }

    public static class Builder<E extends DElement> {
        protected DName title;
        protected DMenu menu;
        private DName subtitle;
        private final List<DIndexEntry<E>> entries = new ArrayList<>();

        protected Builder() {
            super();
        }

        public Builder<E> title(DName title) {
            this.title = title;
            return this;
        }

        public Builder<E> menu(DMenu menu) {
            this.menu = menu;
            return this;
        }

        public Builder<E> subtitle(DName subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder<E> entry(DIndexEntry<E> entry) {
            this.entries.add(entry);
            return this;
        }

        public Builder<E> entries(List<DIndexEntry<E>> entries) {
            this.entries.addAll(entries);
            return this;
        }

        public DIndex<E> build() {
            return new DIndex<>(title,
                                menu,
                                subtitle,
                                entries);
        }
    }
}