package cdc.deps.io.html.model;

import cdc.deps.io.html.AbstractDepsToHtml;

/**
 * Section containing an image.
 *
 * @author Damien Carbonne
 */
public class DImageSection extends DSection {
    private final String name;

    protected DImageSection(String title,
                            String name) {
        super(title);
        this.name = AbstractDepsToHtml.toImageName(name);
    }

    public String getName() {
        return name;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends DSection.Builder<B> {
        protected String name;

        protected Builder() {
            super();
        }

        @Override
        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B name(String name) {
            this.name = name;
            return self();
        }

        @Override
        public DImageSection build() {
            return new DImageSection(title, name);
        }
    }
}