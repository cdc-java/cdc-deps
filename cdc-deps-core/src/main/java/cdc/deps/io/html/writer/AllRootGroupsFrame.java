package cdc.deps.io.html.writer;

/**
 * All root groups frame generator.
 *
 * @author Damien Carbonne
 */
class AllRootGroupsFrame extends IndexFrame {
    AllRootGroupsFrame(HtmlWriterGenerator context) {
        super(context,
              context.buildAllRootGroups());
    }
}