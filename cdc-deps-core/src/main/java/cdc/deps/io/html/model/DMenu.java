package cdc.deps.io.html.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A Menu is a list of {@link DMenuItem}s.
 *
 * @author Damien Carbonne
 */
public class DMenu {
    private final List<DMenuItem> items = new ArrayList<>();

    public DMenu(List<DMenuItem> items) {
        this.items.addAll(items);
    }

    public List<DMenuItem> getItems() {
        return items;
    }
}