package cdc.deps.io.html.writer;

import java.io.File;
import java.io.IOException;

import cdc.deps.Config;
import cdc.deps.DElementScope;
import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DName;
import cdc.io.html.HtmlWriter;
import cdc.io.html.HtmlWriterConsumer;

abstract class AbstractGenerator implements HtmlWriterConsumer {
    protected final HtmlWriterGenerator context;

    protected AbstractGenerator(HtmlWriterGenerator context) {
        this.context = context;
    }

    protected static void content(HtmlWriter writer,
                                  DName name) throws IOException {
        if (name.getImgSrc() != null) {
            writer.img().attrSrc(name.getImgSrc()).end();
        }
        if (name.getHref() == null) {
            writer.text(name.getText());
        } else {
            writer.a().attrHref(name.getHref());
            if (name.getTarget() != null) {
                writer.attrTarget(name.getTarget());
            }
            writer.text(name.getText()).end();
        }
    }

    /**
     * Generate the head (meta) section, common to all files.
     *
     * @param writer The writer.
     * @param title The page title.
     * @param upPath The path to root directory.
     * @throws IOException When an IO exception occurs.
     */
    protected static void head(HtmlWriter writer,
                               String title,
                               String upPath) throws IOException {
        writer.head();
        writer.meta()
              .attrHttpEquiv("content-type")
              .attrContent("text/html; charset=" + writer.getEncoding())
              .end();
        writer.link()
              .attrRel("icon")
              .attrType("image/png")
              .attrHref(upPath + "/" + Params.Dirs.IMAGES + "/cdc-16.png")
              .attrSizes("16x16")
              .end();
        writer.link()
              .attrRel("stylesheet")
              .attrHref(upPath + "/cdc-deps-multi.css")
              .end();
        writer.title().text(title).end();
        writer.end(); // head
    }

    protected static void noticeLogo(HtmlWriter writer,
                                     String upPath) throws IOException {
        writer.div().attrClass("nav-padding");
        writer.table().attrClass("top").attrStyle("width:100%");
        writer.tr();

        writer.td().end();

        writer.td().attrClass("top-notice");
        writer.span().text("Generated by").end();
        writer.br().end();
        writer.span().attrClass("signature").text("CDC Deps-" + Config.VERSION).end();
        writer.end(); // td

        writer.td().attrClass("top-logo");
        writer.img().attrSrc(upPath + "/" + Params.Dirs.IMAGES + "/cdc-48.png").end(); // img
        writer.end(); // td

        writer.end(); // tr
        writer.end(); // table
        writer.end(); // div
    }

    protected void addGvImage(HtmlWriter writer,
                              String name,
                              String upPath) throws IOException {
        writer.div();
        writer.attrClass(Params.Classes.GV_IMAGE);
        writer.img()
              .attrSrc(upPath + "/" + Params.Dirs.IMAGES + "/" + name + ".png")
              .attrUsemap("#" + name)
              .end();
        writer.include(new File(context.imagesDir, name + ".cmapx"), true, 1);
        writer.end(); // div
    }

    private void addImage(HtmlWriter writer,
                          File path,
                          String category,
                          DElementScope scope) throws IOException {
        final File f = path == null
                ? context.getImagePath(category, scope)
                : new File(path, context.getImagePath(category, scope).getPath());
        writer.img().attrSrc(f.getPath()).end();
    }

    protected void addImage(HtmlWriter writer,
                            String category,
                            DElementScope scope) throws IOException {
        addImage(writer, null, category, scope);
    }
}