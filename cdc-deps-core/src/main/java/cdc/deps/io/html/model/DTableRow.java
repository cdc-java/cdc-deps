package cdc.deps.io.html.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DTableRow {
    private final List<DName> cells;

    public DTableRow(List<DName> cells) {
        this.cells = new ArrayList<>(cells);
    }

    public DTableRow(DName... cells) {
        this.cells = new ArrayList<>();
        Collections.addAll(this.cells, cells);
    }

    public DTableRow(String... cells) {
        this.cells = new ArrayList<>();
        for (final String cell : cells) {
            this.cells.add(DName.builder().text(cell).build());
        }
    }

    public List<DName> getCells() {
        return cells;
    }
}