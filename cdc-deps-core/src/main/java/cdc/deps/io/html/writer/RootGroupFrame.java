package cdc.deps.io.html.writer;

import cdc.deps.DGroup;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;

class RootGroupFrame extends IndexFrame {
    protected RootGroupFrame(HtmlWriterGenerator context,
                             DGroup root) {
        super(context,
              context.buildRootGroup(root),
              AbstractDepsToHtml.upPath(Params.Dirs.GROUPS + "/" + root.getName()));
    }
}