package cdc.deps.io.html.writer;

import cdc.deps.DItem;
import cdc.deps.DNamePart;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DMainElement;

class ItemOverview extends MainFrame<DMainElement<DItem>> {
    protected ItemOverview(HtmlWriterGenerator context,
                           DItem item) {
        super(context,
              context.buildItemOverview(item),
              AbstractDepsToHtml.upPath(Params.Dirs.PACKAGES + "/" + item.getNamePart(DNamePart.PATH)));
    }
}