package cdc.deps.io.html.model;

import cdc.deps.DElement;

/**
 * Description of an index entry.
 * <p>
 * It is {@link DName} with an associated element.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class DIndexEntry<E extends DElement> extends DName {
    /** The associated element. */
    private final E element;

    protected DIndexEntry(String href,
                          String target,
                          String icon,
                          E element) {
        super(element.getName(),
              href,
              target,
              icon);
        this.element = element;
    }

    public E getElement() {
        return element;
    }

    public static <E extends DElement> Builder<E, ?> builder(E element) {
        return new Builder<>(element);
    }

    public static class Builder<E extends DElement, B extends Builder<E, B>> extends DName.Builder<B> {
        protected final E element;

        protected Builder(E element) {
            super();
            this.element = element;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected B self() {
            return (B) this;
        }

        @Override
        public DIndexEntry<E> build() {
            return new DIndexEntry<>(href, target, imgSrc, element);
        }
    }
}