package cdc.deps.io.html.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class of a main frames.
 * <p>
 * It is composed of:
 * <ul>
 * <li>A <b>title</b>.
 * <li>A <b>menu</b>.
 * <li>A <b>subtitle</b>.
 * <li>A list of <b>sections</b>.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class DMain {
    private final DName title;
    private final DMenu menu;
    private final DName subtitle;
    private final List<DSection> sections = new ArrayList<>();

    public DMain(DName title,
                 DMenu menu,
                 DName subtitle,
                 List<DSection> sections) {
        this.title = title;
        this.menu = menu;
        this.subtitle = subtitle;
        this.sections.addAll(sections);
    }

    public DName getTitle() {
        return title;
    }

    public DMenu getMenu() {
        return menu;
    }

    public DName getSubtitle() {
        return subtitle;
    }

    public List<DSection> getSections() {
        return sections;
    }
}