package cdc.deps.io.html;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.DAnalysisInfo;
import cdc.deps.DAnalysisInfo.Argument;
import cdc.deps.DElement;
import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DNamePart;
import cdc.deps.DPackage;
import cdc.deps.graphs.CyclePosition;
import cdc.deps.graphs.DAnalysisCycles;
import cdc.deps.io.gv.DepsToGv;
import cdc.deps.io.html.AbstractDepsToHtml.MainArgs.Feature;
import cdc.deps.io.html.model.DImageSection;
import cdc.deps.io.html.model.DIndex;
import cdc.deps.io.html.model.DIndexEntry;
import cdc.deps.io.html.model.DMain;
import cdc.deps.io.html.model.DMainElement;
import cdc.deps.io.html.model.DMenu;
import cdc.deps.io.html.model.DMenuItem;
import cdc.deps.io.html.model.DName;
import cdc.deps.io.html.model.DSection;
import cdc.deps.io.html.model.DTableRow;
import cdc.deps.io.html.model.DTableSection;
import cdc.deps.io.xml.DAnalysisXmlLoader;
import cdc.gv.tools.GvFormat;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.OptionEnum;
import cdc.util.files.Resources;
import cdc.util.files.SearchPath;
import cdc.util.lang.FailureReaction;
import cdc.util.strings.StringUtils;
import cdc.util.time.Chronometer;

public abstract class AbstractDepsToHtml {
    private static final Logger LOGGER = LogManager.getLogger(AbstractDepsToHtml.class);
    protected static final String SEP = "/";
    public final MainArgs margs;
    public final File imagesDir;
    public final DAnalysis analysis;
    private DAnalysisCycles cycles;

    public static class MainArgs {
        /** XML input file (mandatory). */
        public URL input;
        /** Output directory (mandatory). */
        public File outputDir;
        /** Directories where executables / data files can be found. */
        public SearchPath paths = SearchPath.EMPTY;
        /** Character Set to use with GraphViz. */
        public String charset;
        /** Name of the xml file describing styles. */
        public URL styles;
        /** Max number of dependencies to display in tooltips. */
        public int tooltipMaxDeps;
        /** Document title. */
        public String title;

        public final FeatureMask<Feature> features = new FeatureMask<>();

        public Engine engine;

        public void setEnabled(Feature feature,
                               boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }

        /**
         * Enumeration of possible boolean options.
         */
        public enum Feature implements OptionEnum {
            MULTI_THREAD("multi-thread", "Use multiple threads."),

            WITH_IMAGES("with-images", "Generate and reference images in the html file."),

            /**
             * Do not show elements whose scope is UNKNOWN. This may be the case of
             * root packages that do not contain any analyzed or referenced
             * elements.
             */
            NO_UNKNOWN("no-unknown", "Do not show elements whose scope is UNKNOWN."),

            /**
             * Do not show elements whose scope is EXTERNAL. Dependencies to these
             * elements are not shown.
             */
            NO_EXTERNAL("no-external", "Do not show elements whose scope is EXTERNAL."),

            MULTI("multi", "Generate several html files (frames)."),

            ROOT_GROUPS_CLOSURE("root-groups-closure", "Show dependencies and transitive closure of root groups."),

            PACKAGES_CLOSURE("packages-closure", "Show dependencies and transitive closure of packages."),

            INVOKE_IF_NEWER("invoke-if-newer", "Invoke engine if target files don't exist or are older than input file.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        public enum Engine {
            XSLT,
            HTML_WRITER,
            HTML_FLOW,
            ST4
        }
    }

    protected AbstractDepsToHtml(MainArgs margs) {
        this.margs = margs;
        this.analysis = new DAnalysis();
        this.imagesDir = new File(margs.outputDir.getPath(), Params.Dirs.IMAGES);
    }

    protected static void mkdirs(File file) throws IOException {
        Files.createDirectories(file.toPath());
    }

    private void buildCycles() {
        if (cycles == null) {
            time(() -> cycles = new DAnalysisCycles(analysis,
                                                    DAnalysisCycles.Feature.ITEMS,
                                                    DAnalysisCycles.Feature.PACKAGES,
                                                    DAnalysisCycles.Feature.ROOT_GROUPS),
                 "Compute cycles",
                 "Computed cycles");
        }
    }

    public DIndex<DGroup> buildAllGroups() {
        final DName title = DName.builder()
                                 .text(Params.Names.ALL_GROUPS)
                                 .build();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.GROUPS)
                                    .build();
        final List<DIndexEntry<DGroup>> entries = new ArrayList<>();
        for (final DGroup group : analysis.getElements(DGroup.class, DElement.NAME_COMPARATOR)) {
            entries.add(DIndexEntry.builder(group).build());
        }
        return DIndex.builder(DGroup.class)
                     .title(title)
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    public DIndex<DItem> buildAllItems() {
        final DName title = DName.builder()
                                 .text(Params.Names.ALL_ITEMS)
                                 .build();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ITEMS)
                                    .build();
        final List<DIndexEntry<DItem>> entries = new ArrayList<>();
        for (final DItem item : analysis.getElements(DItem.class, DElement.NAME_COMPARATOR)) {
            if (accept(item)) {
                entries.add(DIndexEntry.builder(item)
                                       .href(Params.Dirs.PACKAGES + "/" + item.getName() + ".html")
                                       .target(Params.Frames.RIGHT)
                                       .build());
            }
        }
        return DIndex.builder(DItem.class)
                     .title(title)
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    public DIndex<DPackage> buildAllPackages() {
        final DName title = DName.builder()
                                 .text(margs.title)
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ALL_PACKAGES)
                                    .build();
        final List<DIndexEntry<DPackage>> entries = new ArrayList<>();

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_ROOT_GROUPS)
                               .href(Params.Files.ALL_ROOT_GROUPS_FRAME)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_GROUPS)
                               .href(Params.Files.ALL_GROUPS_FRAME)
                               .target(Params.Frames.LEFT_BOTTOM)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_ITEMS)
                               .href(Params.Files.ALL_ITEMS_FRAME)
                               .target(Params.Frames.LEFT_BOTTOM)
                               .build());

        for (final DPackage p : analysis.getElements(DPackage.class, DElement.NAME_COMPARATOR)) {
            if (accept(p)) {
                entries.add(DIndexEntry.builder(p)
                                       .href(Params.Dirs.PACKAGES + "/" + p.getName() + "/" + Params.Files.PACKAGE_FRAME)
                                       .target(Params.Frames.LEFT_BOTTOM)
                                       .build());
            }
        }

        return DIndex.builder(DPackage.class)
                     .title(title)
                     .menu(new DMenu(menuItems))
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    public DIndex<DGroup> buildAllRootGroups() {
        final DName title = DName.builder()
                                 .text(margs.title)
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ALL_ROOT_GROUPS)
                                    .build();
        final List<DIndexEntry<DGroup>> entries = new ArrayList<>();

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_GROUPS)
                               .href(Params.Files.ALL_GROUPS_FRAME)
                               .target(Params.Frames.LEFT_BOTTOM)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_PACKAGES)
                               .href(Params.Files.ALL_PACKAGES_FRAME)
                               .target(Params.Frames.LEFT_TOP)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.ALL_ITEMS)
                               .href(Params.Files.ALL_ITEMS_FRAME)
                               .target(Params.Frames.LEFT_BOTTOM)
                               .build());

        for (final DGroup group : analysis.getSortedRoots(DGroup.class)) {
            entries.add(DIndexEntry.builder(group)
                                   .href(Params.Dirs.GROUPS + "/" + group.getName() + "/" + Params.Files.ROOT_GROUP_FRAME)
                                   .target(Params.Frames.LEFT_BOTTOM)
                                   .build());
        }

        return DIndex.builder(DGroup.class)
                     .title(title)
                     .menu(new DMenu(menuItems))
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    public DIndex<DGroup> buildRootGroup(DGroup root) {
        final DName title = DName.builder()
                                 .text(root.getName())
                                 .href(Params.Files.ROOT_GROUP_OVERVIEW)
                                 .target(Params.Frames.RIGHT)
                                 .build();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ALL_ROOT_GROUPS)
                                    .build();
        final List<DIndexEntry<DGroup>> entries = new ArrayList<>();

        for (final DGroup group : analysis.getElements(DGroup.class, DElement.NAME_COMPARATOR)) {
            if (group.getRoot() == root) {
                entries.add(DIndexEntry.builder(group)
                                       .href(Params.Files.ROOT_GROUP_OVERVIEW)
                                       .target(Params.Frames.RIGHT)
                                       .build());
            }
        }

        return DIndex.builder(DGroup.class)
                     .title(title)
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    public DIndex<DItem> buildPackage(DPackage p) {
        final DName title = DName.builder()
                                 .text(p.getName())
                                 .href(Params.Files.PACKAGE_OVERVIEW)
                                 .target(Params.Frames.RIGHT)
                                 .build();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ITEMS)
                                    .build();
        final List<DIndexEntry<DItem>> entries = new ArrayList<>();

        for (final DItem item : analysis.getElements(DItem.class, DElement.NAME_COMPARATOR)) {
            if (item.getPackage() == p) {
                entries.add(DIndexEntry.builder(item)
                                       .href(item.getNamePart(DNamePart.BASE_NAME) + ".html")
                                       .target(Params.Frames.RIGHT)
                                       .build());
            }
        }

        return DIndex.builder(DItem.class)
                     .title(title)
                     .subtitle(subtitle)
                     .entries(entries)
                     .build();
    }

    private static <E> void addIfNotNull(Collection<E> collection,
                                         E element) {
        if (element != null) {
            collection.add(element);
        }
    }

    public DMain buildOverview() {
        final DName title = DName.builder()
                                 .text(Params.Names.OVERVIEW)
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(margs.title)
                                    .build();

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_OVERVIEW)
                               .highlighted(true)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ROOT_GROUP)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_PACKAGE)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ITEM)
                               .build());

        final List<DSection> sections = new ArrayList<>();

        sections.add(createOverviewInfoSection());

        sections.add(DImageSection.builder()
                                  .title("Packages dependencies graph")
                                  .name("all-packages")
                                  .build());

        if (hasGlobalPackagesCycles()) {
            sections.add(DImageSection.builder()
                                      .title("Packages dependencies cycles")
                                      .name("all-packages-global-cycles")
                                      .build());
        }

        sections.add(DImageSection.builder()
                                  .title("Root groups dependencies graph")
                                  .name("all-root-groups")
                                  .build());

        if (hasItemsCycles()) {
            sections.add(DImageSection.builder()
                                      .title("Items dependencies cycles")
                                      .name("all-items-global-cycles")
                                      .build());
        }

        addIfNotNull(sections, createOverviewPackageIndexSection(DElementScope.INTERNAL));
        addIfNotNull(sections, createOverviewPackageIndexSection(DElementScope.MIXED));
        addIfNotNull(sections, createOverviewPackageIndexSection(DElementScope.EXTERNAL));
        addIfNotNull(sections, createOverviewPackageIndexSection(DElementScope.UNKNOWN));
        // TODO internal/external/... packages sections

        return new DMain(title,
                         new DMenu(menuItems),
                         subtitle,
                         sections);
    }

    private DTableSection createOverviewInfoSection() {
        final List<DTableRow> data = new ArrayList<>();
        data.add(new DTableRow("Creation Date", DAnalysisInfo.toString(analysis.getInfo().getCreationDate())));
        data.add(new DTableRow("Analyzer", analysis.getInfo().getAnalyzerName()));
        for (final Argument arg : analysis.getInfo().getAnalysisArguments()) {
            if (StringUtils.isNullOrEmpty(arg.getValue())) {
                data.add(new DTableRow(arg.getName()));
            } else {
                data.add(new DTableRow(arg.getName(), arg.getValue()));
            }
        }
        if (!margs.isEnabled(Feature.WITH_IMAGES)) {
            data.add(new DTableRow("no-images"));
        }
        if (margs.isEnabled(Feature.NO_EXTERNAL)) {
            data.add(new DTableRow("no-external"));
        }
        if (margs.isEnabled(Feature.NO_UNKNOWN)) {
            data.add(new DTableRow("no-unknown"));
        }

        return DTableSection.builder()
                            .title("Info")
                            .header(new DTableRow("Name", "Value"))
                            .data(data)
                            .tableClass(Params.Classes.INFO_TABLE)
                            .dataClasses(Params.Classes.INFO_NAME, Params.Classes.INFO_VALUE)
                            .build();
    }

    private DTableSection createOverviewPackageIndexSection(DElementScope scope) {
        final int numberOfScopedPackages = getNumberOfVisiblePackages(scope);
        if (numberOfScopedPackages > 0) {
            final int numberOfScopedItems = getNumberOfVisibleItems(scope);

            final String title;
            switch (scope) {
            case EXTERNAL:
                title = "External packages";
                break;
            case INTERNAL:
                title = "Internal packages";
                break;
            case MIXED:
                title = "Mixed packages";
                break;
            case UNKNOWN:
                title = "Unknown packages";
                break;
            default:
                title = null;
                break;
            }

            final List<DTableRow> data = new ArrayList<>();

            for (final DPackage p : analysis.getElements(DPackage.class, DElement.NAME_COMPARATOR)) {
                if (p.getScope() == scope) {
                    final List<DName> cells = new ArrayList<>();
                    final int numberOfScopedItemsInPackage = getNumberOfVisibleItems(p, scope);

                    cells.add(DName.builder()
                                   .text(p.getName())
                                   .href("packages/" + p.getName() + "/package-overview.html")
                                   .imgSrc(getImagePath(p.getCategory(), scope).getPath())
                                   .build());
                    cells.add(DName.builder()
                                   .text(numberOfScopedItemsInPackage)
                                   .build());
                    data.add(new DTableRow(cells));
                }
            }
            return DTableSection.builder()
                                .title(title)
                                .header(new DTableRow("Packages (" + numberOfScopedPackages + ")",
                                                      "Items (" + numberOfScopedItems + ")"))
                                .data(data)
                                .tableClass(Params.Classes.INDEX_TABLE)
                                .dataClasses(Params.Classes.TARGET, Params.Classes.COUNT)
                                .build();
        } else {
            return null;
        }
    }

    public DMainElement<DGroup> buildRootGroupOverview(DGroup root) {
        final DName title = DName.builder()
                                 .text(root.getName())
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ROOT_GROUP + " " + root.getName())
                                    .build();

        final String upPath = AbstractDepsToHtml.upPath(Params.Dirs.GROUPS + "/" + root.getName());

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_OVERVIEW)
                               .href(upPath + "/" + Params.Files.OVERVIEW)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ROOT_GROUP)
                               .highlighted(true)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_PACKAGE)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ITEM)
                               .build());

        final List<DSection> sections = new ArrayList<>();
        sections.add(DImageSection.builder()
                                  .title("Out dependencies graph")
                                  .name("group-out-" + root.getName())
                                  .build());

        sections.add(DTableSection.builder()
                                  .title("Packages")
                                  .build());
        // TODO

        sections.add(DTableSection.builder()
                                  .title("Items")
                                  .build());
        // TODO

        return new DMainElement<>(title,
                                  new DMenu(menuItems),
                                  subtitle,
                                  sections,
                                  root);
    }

    public DMainElement<DPackage> buildPackageOverview(DPackage p) {
        final DName title = DName.builder()
                                 .text(p.getName())
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.PACKAGE + " " + p.getName())
                                    .build();

        final String upPath = AbstractDepsToHtml.upPath(Params.Dirs.PACKAGES + "/" + p.getName());

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_OVERVIEW)
                               .href(upPath + "/" + Params.Files.OVERVIEW)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ROOT_GROUP)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_PACKAGE)
                               .highlighted(true)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ITEM)
                               .build());

        final List<DSection> sections = new ArrayList<>();
        sections.add(DImageSection.builder()
                                  .title("Out dependencies graph")
                                  .name("package-out-" + p.getName())
                                  .build());

        sections.add(DTableSection.builder()
                                  .title("Out dependencies")
                                  .build());
        // TODO

        sections.add(DTableSection.builder()
                                  .title("In dependencies")
                                  .build());
        // TODO

        sections.add(DImageSection.builder()
                                  .title("Detailed out dependencies graph")
                                  .name("package-items-" + p.getName())
                                  .build());

        sections.add(DTableSection.builder()
                                  .title("Items")
                                  .build());
        // TODO

        sections.add(DTableSection.builder()
                                  .title("Root Groups")
                                  .build());
        // TODO

        return new DMainElement<>(title,
                                  new DMenu(menuItems),
                                  subtitle,
                                  sections,
                                  p);
    }

    public DMainElement<DItem> buildItemOverview(DItem item) {
        final DName title = DName.builder()
                                 .text(item.getName())
                                 .build();
        final List<DMenuItem> menuItems = new ArrayList<>();
        final DName subtitle = DName.builder()
                                    .text(Params.Names.ITEM + " " + item.getName())
                                    .build();

        final String upPath = AbstractDepsToHtml.upPath(Params.Dirs.PACKAGES + SEP + item.getNamePart(DNamePart.PATH));

        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_OVERVIEW)
                               .href(upPath + SEP + Params.Files.OVERVIEW)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ROOT_GROUP)
                               .href(upPath + SEP + Params.Dirs.GROUPS + SEP + item.getRootOwner().getName() + SEP
                                       + Params.Files.ROOT_GROUP_OVERVIEW)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_PACKAGE)
                               .build());
        menuItems.add(DMenuItem.builder()
                               .text(Params.Names.UC_ITEM)
                               .highlighted(true)
                               .build());

        final List<DSection> sections = new ArrayList<>();
        sections.add(DImageSection.builder()
                                  .title("Out dependencies graph")
                                  .name("item-out-" + item.getName())
                                  .build());

        sections.add(DTableSection.builder()
                                  .title("Out dependencies")
                                  .build());
        // TODO

        return new DMainElement<>(title,
                                  new DMenu(menuItems),
                                  subtitle,
                                  sections,
                                  item);
    }

    @FunctionalInterface
    protected static interface Procedure<E extends Exception> {
        public void run() throws E;
    }

    protected static <E extends Exception> void time(Procedure<E> procedure,
                                                     String before,
                                                     String after) throws E {
        LOGGER.info(before);
        final Chronometer chrono = new Chronometer();
        try {
            chrono.start();
            procedure.run();
        } finally {
            chrono.suspend();
            LOGGER.info("{} ({})", after, chrono);
        }
    }

    public final void generate() throws IOException {
        mkdirs(margs.outputDir);
        analysis.clear();
        final DAnalysisXmlLoader loader = new DAnalysisXmlLoader(analysis, FailureReaction.FAIL);
        time(() -> loader.loadXml(margs.input),
             "Load analysis " + margs.input,
             "Loaded analysis " + margs.input);

        time(this::copyFiles, "Copy files", "Copied files");
        if (margs.isEnabled(MainArgs.Feature.WITH_IMAGES)) {
            time(this::generateGvImages, "Generate images", "Generated images");
        }
        time(this::generateHtml, "Generate HTML", "Generated HTML");
    }

    protected abstract void generateHtml() throws IOException;

    public boolean accept(DElement element) {
        final DElementScope scope = element.getScope();
        switch (scope) {
        case EXTERNAL:
            return !margs.isEnabled(AbstractDepsToHtml.MainArgs.Feature.NO_EXTERNAL);
        case INTERNAL:
        case MIXED:
            return true;
        case UNKNOWN:
        default:
            return !margs.isEnabled(AbstractDepsToHtml.MainArgs.Feature.NO_UNKNOWN);
        }
    }

    public boolean hasGlobalPackagesCycles() {
        buildCycles();
        for (final DPackage p : analysis.getElements(DPackage.class)) {
            if (cycles.getCyclePosition(p) == CyclePosition.GLOBAL_CYCLE) {
                return true;
            }
        }
        return false;
    }

    public boolean hasItemsCycles() {
        buildCycles();
        for (final DItem p : analysis.getElements(DItem.class)) {
            if (cycles.getCyclePosition(p) != CyclePosition.NO_CYCLE) {
                return true;
            }
        }
        return false;
    }

    public final int getNumberOfVisiblePackages(DElementScope scope) {
        if (margs.isEnabled(Feature.NO_EXTERNAL) && scope == DElementScope.EXTERNAL) {
            return 0;
        }
        if (margs.isEnabled(Feature.NO_UNKNOWN) && scope == DElementScope.UNKNOWN) {
            return 0;
        }
        return (int) analysis.getElements()
                             .stream()
                             .filter(DPackage.class::isInstance)
                             .filter(e -> e.getScope() == scope)
                             .count();
    }

    public final int getNumberOfVisibleItems(DPackage p,
                                             DElementScope scope) {
        final long internal = analysis.getElements()
                                      .stream()
                                      .filter(DItem.class::isInstance)
                                      .filter(x -> ((DItem) x).getScope() == DElementScope.INTERNAL)
                                      .filter(x -> ((DItem) x).getPackage() == p && ((DItem) x).getPackage().getScope() == scope)
                                      .count();

        final long external;
        if (margs.isEnabled(Feature.NO_EXTERNAL)) {
            external = 0;
        } else {
            external = analysis.getElements()
                               .stream()
                               .filter(DItem.class::isInstance)
                               .filter(x -> ((DItem) x).getScope() == DElementScope.EXTERNAL)
                               .filter(x -> ((DItem) x).getPackage() == p && ((DItem) x).getPackage().getScope() == scope)
                               .count();
        }

        final long unknown;
        if (margs.isEnabled(Feature.NO_UNKNOWN)) {
            unknown = 0;
        } else {
            unknown = analysis.getElements()
                              .stream()
                              .filter(DItem.class::isInstance)
                              .filter(x -> ((DItem) x).getScope() == DElementScope.UNKNOWN)
                              .filter(x -> ((DItem) x).getPackage() == p && ((DItem) x).getPackage().getScope() == scope)
                              .count();
        }

        return (int) (internal + external + unknown);
    }

    public final int getNumberOfVisibleItems(DElementScope scope) {
        final long internal = analysis.getElements()
                                      .stream()
                                      .filter(DItem.class::isInstance)
                                      .filter(x -> ((DItem) x).getScope() == DElementScope.INTERNAL)
                                      .filter(x -> ((DItem) x).getPackage().getScope() == scope)
                                      .count();

        final long external;
        if (margs.isEnabled(Feature.NO_EXTERNAL)) {
            external = 0;
        } else {
            external = analysis.getElements()
                               .stream()
                               .filter(DItem.class::isInstance)
                               .filter(x -> ((DItem) x).getScope() == DElementScope.EXTERNAL)
                               .filter(x -> ((DItem) x).getPackage().getScope() == scope)
                               .count();
        }

        final long unknown;
        if (margs.isEnabled(Feature.NO_UNKNOWN)) {
            unknown = 0;
        } else {
            unknown = analysis.getElements()
                              .stream()
                              .filter(DItem.class::isInstance)
                              .filter(x -> ((DItem) x).getScope() == DElementScope.UNKNOWN)
                              .filter(x -> ((DItem) x).getPackage().getScope() == scope)
                              .count();
        }

        return (int) (internal + external + unknown);
    }

    public static String normalize(String s) {
        return s.toLowerCase().replace('.', '-');
    }

    private static final Pattern P = Pattern.compile("'\":<>\\?\\*");

    public static String normalizeRelativePath(String path) {
        return P.matcher(path).replaceAll("-");
    }

    public static String toImageName(String name) {
        return cdc.util.files.Files.toValidBasename(name);
    }

    public static String upPath(String path) {
        final int depth = StringUtils.countMatches(path, '/');
        final StringBuilder builder = new StringBuilder();
        builder.append("..");
        for (int i = 0; i < depth; i++) {
            builder.append("/..");
        }
        return builder.toString();
    }

    public File getImagePath(String category,
                             DElementScope scope) {
        return new File(Params.Dirs.IMAGES + SEP + normalize(category) + "-" + normalize(scope.name()) + ".png");
    }

    public File getImagePath(File path,
                             String category,
                             DElementScope scope) {
        return path == null
                ? getImagePath(category, scope)
                : new File(path, getImagePath(category, scope).getPath());
    }

    private void copyFiles() throws IOException {
        mkdirs(imagesDir);
        try {
            final String[] images = Resources.getResourceListing("cdc/deps/images");
            for (final String image : images) {
                Resources.copy("cdc/deps/images/" + image, imagesDir);
            }
        } catch (final Exception e) {
            throw new IOException(e);
        }
        Resources.copy("cdc/ui/images/cdc-16.png", imagesDir);
        Resources.copy("cdc/ui/images/cdc-48.png", imagesDir);

        if (margs.isEnabled(MainArgs.Feature.MULTI)) {
            Resources.copy("cdc/deps/cdc-deps-multi.css", margs.outputDir);
        } else {
            Resources.copy("cdc/deps/cdc-deps-single.css", margs.outputDir);
            Resources.copy("cdc/deps/cdc-deps-single-print.css", margs.outputDir);
        }
    }

    private void generateGvImages() {
        final DepsToGv.MainArgs oargs = new DepsToGv.MainArgs();
        oargs.formats.add(GvFormat.PNG);
        oargs.formats.add(GvFormat.CMAPX);
        oargs.input = margs.input;
        oargs.output = imagesDir;
        oargs.imagesDir = imagesDir;
        oargs.styles = margs.styles;
        oargs.charset = margs.charset;
        oargs.paths = oargs.paths.append(margs.paths);
        oargs.paths = oargs.paths.append(imagesDir);
        oargs.tooltipMaxDeps = margs.tooltipMaxDeps;
        oargs.setEnabled(DepsToGv.MainArgs.Feature.NO_EXTERNAL, margs.isEnabled(MainArgs.Feature.NO_EXTERNAL));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.NO_UNKNOWN, margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.ADAPT_EDGE_WIDH_TO_COUNT, true);
        oargs.setEnabled(DepsToGv.MainArgs.Feature.PACKAGED_ITEMS, true);
        oargs.setEnabled(DepsToGv.MainArgs.Feature.MULTI_THREAD, margs.isEnabled(MainArgs.Feature.MULTI_THREAD));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.INVOKE_IF_NEWER, margs.isEnabled(MainArgs.Feature.INVOKE_IF_NEWER));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.MULTI, margs.isEnabled(MainArgs.Feature.MULTI));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.ROOT_GROUPS_CLOSURE, margs.isEnabled(MainArgs.Feature.ROOT_GROUPS_CLOSURE));
        oargs.setEnabled(DepsToGv.MainArgs.Feature.PACKAGES_CLOSURE, margs.isEnabled(MainArgs.Feature.PACKAGES_CLOSURE));

        oargs.setEnabled(DepsToGv.MainArgs.Target.ALL_PACKAGES, true);
        oargs.setEnabled(DepsToGv.MainArgs.Target.ALL_ROOT_GROUPS, true);
        oargs.setEnabled(DepsToGv.MainArgs.Target.ALL_ITEMS, true);
        oargs.setEnabled(DepsToGv.MainArgs.Feature.SHOW_ONLY_GLOBAL_CYCLES, true);

        DepsToGv.execute(oargs);

        oargs.setEnabled(DepsToGv.MainArgs.Target.ALL_ITEMS, false);
        oargs.setEnabled(DepsToGv.MainArgs.Target.PACKAGE_ITEMS, true);
        oargs.setEnabled(DepsToGv.MainArgs.Target.ROOT_GROUP_OUT, true);
        oargs.setEnabled(DepsToGv.MainArgs.Target.PACKAGE_OUT, true);
        oargs.setEnabled(DepsToGv.MainArgs.Target.ITEM_OUT, true);
        oargs.setEnabled(DepsToGv.MainArgs.Feature.SHOW_ONLY_GLOBAL_CYCLES, false);

        DepsToGv.execute(oargs);
    }
}