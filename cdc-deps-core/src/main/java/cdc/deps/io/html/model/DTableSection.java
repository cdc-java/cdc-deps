package cdc.deps.io.html.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DTableSection extends DSection {
    private final DTableRow header;
    private final List<DTableRow> data;
    private final String tableClass;
    private final List<String> dataClasses;

    protected DTableSection(String title,
                            DTableRow header,
                            List<DTableRow> data,
                            String tableClass,
                            List<String> dataClasses) {
        super(title);
        this.header = header;
        this.data = data;
        this.tableClass = tableClass;
        this.dataClasses = dataClasses;
    }

    public DTableRow getHeader() {
        return header;
    }

    public List<DTableRow> getData() {
        return data;
    }

    public String getTableClass() {
        return tableClass;
    }

    public List<String> getDataClasses() {
        return dataClasses;
    }

    public String getDataClass(int index) {
        if (index >= 0 && index < dataClasses.size()) {
            return dataClasses.get(index);
        } else {
            return null;
        }
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends DSection.Builder<B> {
        protected DTableRow header;
        protected final List<DTableRow> data = new ArrayList<>();
        protected String tableClass;
        protected final List<String> dataClasses = new ArrayList<>();

        protected Builder() {
            super();
        }

        @Override
        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B header(DTableRow header) {
            this.header = header;
            return self();
        }

        public B data(DTableRow data) {
            this.data.add(data);
            return self();
        }

        public B data(List<DTableRow> data) {
            this.data.addAll(data);
            return self();
        }

        public B tableClass(String tableClass) {
            this.tableClass = tableClass;
            return self();
        }

        public B dataClass(String dataClass) {
            this.dataClasses.add(dataClass);
            return self();
        }

        public B dataClasses(List<String> dataClasses) {
            this.dataClasses.addAll(dataClasses);
            return self();
        }

        public B dataClasses(String... dataClasses) {
            Collections.addAll(this.dataClasses, dataClasses);
            return self();
        }

        @Override
        public DTableSection build() {
            return new DTableSection(title,
                                     header,
                                     data,
                                     tableClass,
                                     dataClasses);
        }
    }
}