package cdc.deps.io.html.xslt;

import java.io.File;
import java.io.IOException;

import cdc.deps.Config;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.io.tools.XmlToHtml;
import cdc.util.files.Files;
import cdc.util.files.Resources;
import cdc.util.time.Chronometer;

public class HtmlXsltGenerator extends AbstractDepsToHtml {
    public HtmlXsltGenerator(MainArgs margs) {
        super(margs);
    }

    @Override
    protected void generateHtml() throws IOException {
        final XmlToHtml.MainArgs oargs = new XmlToHtml.MainArgs();
        oargs.xml = margs.input;
        if (margs.isEnabled(MainArgs.Feature.MULTI)) {
            oargs.output = null;
            oargs.xslt = Resources.getResource("cdc/deps/cdc-deps-multi.xsl");
        } else {
            oargs.output = new File(margs.outputDir, Files.getNakedBasename(margs.input.getPath()) + ".html");
            oargs.xslt = Resources.getResource("cdc/deps/cdc-deps-single.xsl");
        }
        oargs.params.put("output-dir", margs.outputDir.toURI().toURL().getPath());
        oargs.params.put("with-images", margs.isEnabled(MainArgs.Feature.WITH_IMAGES) ? "yes" : "no");
        oargs.params.put("with-unknown", margs.isEnabled(MainArgs.Feature.NO_UNKNOWN) ? "no" : "yes");
        oargs.params.put("with-external", margs.isEnabled(MainArgs.Feature.NO_EXTERNAL) ? "no" : "yes");
        oargs.params.put("title", margs.title);
        oargs.params.put("version", Config.VERSION);
        final Chronometer chrono = new Chronometer();
        chrono.start();
        try {
            XmlToHtml.execute(oargs);
        } catch (final IOException e) {
            throw e;
        } catch (final Exception e) {
            throw new IOException(e);
        }
    }
}