package cdc.deps.io.html.model;

import java.util.List;

/**
 * Description of a main frame associated to an element.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class DMainElement<E> extends DMain {
    private final E element;

    public DMainElement(DName title,
                        DMenu menu,
                        DName subtitle,
                        List<DSection> sections,
                        E element) {
        super(title, menu, subtitle, sections);
        this.element = element;
    }

    public E getElement() {
        return element;
    }
}