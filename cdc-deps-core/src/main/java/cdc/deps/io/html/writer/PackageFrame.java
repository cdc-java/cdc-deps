package cdc.deps.io.html.writer;

import cdc.deps.DPackage;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;

class PackageFrame extends IndexFrame {
    protected PackageFrame(HtmlWriterGenerator context,
                           DPackage p) {
        super(context,
              context.buildPackage(p),
              AbstractDepsToHtml.upPath(Params.Dirs.PACKAGES + "/" + p.getName()));
    }
}