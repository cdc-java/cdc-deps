package cdc.deps.io.html.writer;

/**
 * All groups frame generator.
 *
 * @author Damien Carbonne
 */
class AllGroupsFrame extends IndexFrame {
    protected AllGroupsFrame(HtmlWriterGenerator context) {
        super(context,
              context.buildAllGroups());
    }
}