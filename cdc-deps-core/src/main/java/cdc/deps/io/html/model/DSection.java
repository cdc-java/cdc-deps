package cdc.deps.io.html.model;

/**
 * Base class of sections.
 * <p>
 * Each section has a <b>title</b>.
 *
 * @author Damien Carbonne
 */
public class DSection {
    protected final String title;

    protected DSection(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> {
        protected String title;

        protected Builder() {
            super();
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B title(String title) {
            this.title = title;
            return self();
        }

        public DSection build() {
            return new DSection(title);
        }
    }
}