package cdc.deps.io.html.writer;

/**
 * All items frame generator.
 *
 * @author Damien Carbonne
 */
class AllItemsFrame extends IndexFrame {
    protected AllItemsFrame(HtmlWriterGenerator context) {
        super(context,
              context.buildAllItems());
    }
}