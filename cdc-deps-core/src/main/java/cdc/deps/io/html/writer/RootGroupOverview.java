package cdc.deps.io.html.writer;

import cdc.deps.DGroup;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DMainElement;

class RootGroupOverview extends MainFrame<DMainElement<DGroup>> {
    protected RootGroupOverview(HtmlWriterGenerator context,
                                DGroup group) {
        super(context,
              context.buildRootGroupOverview(group),
              AbstractDepsToHtml.upPath(Params.Dirs.GROUPS + "/" + group.getName()));
    }
}