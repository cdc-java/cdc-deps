package cdc.deps.io.html.writer;

import cdc.deps.DPackage;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DMainElement;

class PackageOverview extends MainFrame<DMainElement<DPackage>> {
    protected PackageOverview(HtmlWriterGenerator context,
                              DPackage p) {
        super(context,
              context.buildPackageOverview(p),
              AbstractDepsToHtml.upPath(Params.Dirs.PACKAGES + "/" + p.getName()));
    }
}