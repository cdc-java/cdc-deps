package cdc.deps.io.html.model;

/**
 * Description of a menu item.
 * <p>
 * It is a name.
 *
 * @author Damien Carbonne
 */
public class DMenuItem extends DName {
    /** Should this menu item be highlighted? */
    private final boolean highlighted;

    protected DMenuItem(String text,
                        String href,
                        String target,
                        String icon,
                        boolean highlighted) {
        super(text,
              href,
              target,
              icon);
        this.highlighted = highlighted;
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends DName.Builder<B> {
        protected boolean highlighted;

        protected Builder() {
            super();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected B self() {
            return (B) this;
        }

        public B highlighted(boolean highlighted) {
            this.highlighted = highlighted;
            return self();
        }

        @Override
        public DMenuItem build() {
            return new DMenuItem(text, href, target, imgSrc, highlighted);
        }
    }
}