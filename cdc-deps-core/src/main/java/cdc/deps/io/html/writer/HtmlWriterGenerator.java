package cdc.deps.io.html.writer;

import java.io.File;
import java.io.IOException;

import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DNamePart;
import cdc.deps.DPackage;
import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.Params;
import cdc.io.html.HtmlWriter;
import cdc.io.html.HtmlWriterConsumer;

public class HtmlWriterGenerator extends AbstractDepsToHtml {
    public HtmlWriterGenerator(MainArgs margs) {
        super(margs);
    }

    @Override
    protected void generateHtml() throws IOException {
        generate(new File(margs.outputDir, Params.Files.INDEX), new Index(this));
        generate(new File(margs.outputDir, Params.Files.ALL_ROOT_GROUPS_FRAME), new AllRootGroupsFrame(this));
        generate(new File(margs.outputDir, Params.Files.ALL_GROUPS_FRAME), new AllGroupsFrame(this));
        generate(new File(margs.outputDir, Params.Files.ALL_PACKAGES_FRAME), new AllPackagesFrame(this));
        generate(new File(margs.outputDir, Params.Files.ALL_ITEMS_FRAME), new AllItemsFrame(this));
        generate(new File(margs.outputDir, Params.Files.OVERVIEW), new Overview(this));

        for (final DGroup group : analysis.getSortedRoots(DGroup.class)) {
            final String relativePath = AbstractDepsToHtml.normalizeRelativePath(group.getName());
            final File file = new File(margs.outputDir,
                                       Params.Dirs.GROUPS + SEP + relativePath + SEP + Params.Files.ROOT_GROUP_FRAME);
            mkdirs(file.getParentFile());
            generate(file, new RootGroupFrame(this, group));
        }

        for (final DGroup group : analysis.getSortedRoots(DGroup.class)) {
            final String relativePath = AbstractDepsToHtml.normalizeRelativePath(group.getName());
            final File file = new File(margs.outputDir,
                                       Params.Dirs.GROUPS + SEP + relativePath + SEP + Params.Files.ROOT_GROUP_OVERVIEW);
            mkdirs(file.getParentFile());
            generate(file, new RootGroupOverview(this, group));
        }

        for (final DPackage p : analysis.getElements(DPackage.class)) {
            if (accept(p)) {
                final String relativePath = AbstractDepsToHtml.normalizeRelativePath(p.getName());
                final File file = new File(margs.outputDir,
                                           Params.Dirs.PACKAGES + SEP + relativePath + SEP + Params.Files.PACKAGE_FRAME);
                mkdirs(file.getParentFile());
                generate(file, new PackageFrame(this, p));
            }
        }

        for (final DPackage p : analysis.getElements(DPackage.class)) {
            if (accept(p)) {
                final String relativePath = AbstractDepsToHtml.normalizeRelativePath(p.getName());
                final File file = new File(margs.outputDir,
                                           Params.Dirs.PACKAGES + SEP + relativePath + SEP + Params.Files.PACKAGE_OVERVIEW);
                mkdirs(file.getParentFile());
                generate(file, new PackageOverview(this, p));
            }
        }

        for (final DItem item : analysis.getElements(DItem.class)) {
            if (accept(item) && item.getScope() == DElementScope.INTERNAL) {
                final String relativePath = AbstractDepsToHtml.normalizeRelativePath(item.getNamePart(DNamePart.PATH));
                final File file = new File(margs.outputDir,
                                           Params.Dirs.PACKAGES + SEP + relativePath + SEP + item.getNamePart(DNamePart.BASE_NAME)
                                                   + ".html");
                mkdirs(file.getParentFile());
                generate(file, new ItemOverview(this, item));
            }
        }
    }

    private static void generate(File file,
                                 HtmlWriterConsumer consumer) throws IOException {
        time(() -> {
            try (final HtmlWriter writer = new HtmlWriter(file)) {
                writer.setIndentString("  ");
                consumer.accept(writer);
            }
        },
             "Generate " + file,
             "Generated " + file);
    }
}