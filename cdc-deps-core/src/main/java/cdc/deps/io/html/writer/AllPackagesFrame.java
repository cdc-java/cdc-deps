package cdc.deps.io.html.writer;

/**
 * All packages frame generator.
 *
 * @author Damien Carbonne
 */
class AllPackagesFrame extends IndexFrame {
    protected AllPackagesFrame(HtmlWriterGenerator context) {
        super(context,
              context.buildAllPackages());
    }
}