package cdc.deps.io.html.writer;

import java.io.IOException;

import cdc.deps.io.html.Params;
import cdc.io.html.HtmlWriter;

/**
 * Index generator.
 *
 * @author Damien Carbonne
 */
class Index extends AbstractGenerator {
    protected Index(HtmlWriterGenerator context) {
        super(context);
    }

    @Override
    public void accept(HtmlWriter writer) throws IOException {
        writer.html();
        head(writer, context.margs.title, ".");

        writer.body();
        main(writer);
        writer.end(); // body

        writer.end(); // html
    }

    public void main(HtmlWriter writer) throws IOException {
        writer.main().attrRole(Params.Roles.MAIN);

        writer.div().attrClass(Params.Classes.MAIN_CONTAINER);

        writer.div().attrClass(Params.Classes.LEFT_CONTAINER);

        writer.div().attrClass(Params.Classes.LEFT_TOP_CONTAINER);
        writer.iframe().attrSrc(Params.Files.ALL_PACKAGES_FRAME).attrName(Params.Frames.LEFT_TOP).end();
        writer.end(); // div

        writer.div().attrClass(Params.Classes.LEFT_BOTTOM_CONTAINER);
        writer.iframe().attrSrc(Params.Files.ALL_ITEMS_FRAME).attrName(Params.Frames.LEFT_BOTTOM).end();
        writer.end(); // div

        writer.end(); // div

        writer.div().attrClass(Params.Classes.RIGHT_CONTAINER);
        writer.iframe().attrSrc(Params.Files.OVERVIEW).attrName(Params.Frames.RIGHT).end();
        writer.end(); // div

        writer.end(); // div

        writer.end(); // main
    }
}