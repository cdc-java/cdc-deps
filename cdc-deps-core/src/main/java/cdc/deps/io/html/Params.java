package cdc.deps.io.html;

public final class Params {
    private Params() {
    }

    public static final class Files {
        private Files() {
        }

        public static final String ALL_GROUPS_FRAME = "all-groups-frame.html";
        public static final String ALL_ITEMS_FRAME = "all-items-frame.html";
        public static final String ALL_PACKAGES_FRAME = "all-packages-frame.html";
        public static final String ALL_ROOT_GROUPS_FRAME = "all-root-groups-frame.html";
        public static final String INDEX = "index.html";
        public static final String OVERVIEW = "overview.html";
        public static final String PACKAGE_FRAME = "package-frame.html";
        public static final String PACKAGE_OVERVIEW = "package-overview.html";
        public static final String ROOT_GROUP_FRAME = "root-group-frame.html";
        public static final String ROOT_GROUP_OVERVIEW = "root-group-overview.html";
    }

    public static final class Frames {
        private Frames() {
        }

        public static final String LEFT_BOTTOM = "leftBottomFrame";
        public static final String LEFT_TOP = "leftTopFrame";
        public static final String RIGHT = "rightFrame";
    }

    public static final class Classes {
        private Classes() {
        }

        public static final String ABOUT = "about";
        public static final String BAR = "bar";
        public static final String CONTENT_CONTAINER = "content-container";
        public static final String COUNT = "count";
        public static final String FIXED_NAV = "fixed-nav";
        public static final String GV_IMAGE = "gv-image";
        public static final String HEADER = "header";
        public static final String INDEX_CONTAINER = "index-container";
        public static final String INDEX_NAV = "index-nav";
        public static final String INDEX_TABLE = "index-table";
        public static final String INFO_NAME = "info-name";
        public static final String INFO_TABLE = "info-table";
        public static final String INFO_VALUE = "info-value";
        public static final String LEFT_CONTAINER = "left-container";
        public static final String LEFT_TOP_CONTAINER = "left-top-container";
        public static final String LEFT_BOTTOM_CONTAINER = "left-bottom-container";
        public static final String MAIN_CONTAINER = "main-container";
        public static final String NAV_BAR_CELL_REV = "nav-bar-cell-rev";
        public static final String NAV_LIST = "nav-list";
        public static final String RIGHT_CONTAINER = "right-container";
        public static final String SUB_NAV = "sub-nav";
        public static final String TARGET = "target";
        public static final String TITLE = "title";
        public static final String TOP_NAV = "top-nav";

        // "info-table"
        // "info-name"
        // "info-value"
        // "index-table"
    }

    public static final class Names {
        private Names() {
        }

        public static final String ALL_GROUPS = "All Groups";
        public static final String ALL_ITEMS = "All Items";
        public static final String ALL_PACKAGES = "All Packages";
        public static final String ALL_ROOT_GROUPS = "All Root Groups";
        public static final String GROUP = "Group";
        public static final String GROUPS = "Groups";
        // "Info"
        public static final String ITEM = "Item";
        public static final String ITEMS = "Items";
        // "Name"
        public static final String OVERVIEW = "Overview";
        public static final String PACKAGE = "Package";
        public static final String PACKAGES = "Packages";
        public static final String ROOT_GROUP = "Root Group";
        // "Value"

        public static final String UC_OVERVIEW = "OVERVIEW";
        public static final String UC_ROOT_GROUP = "ROOT GROUP";
        public static final String UC_PACKAGE = "PACKAGE";
        public static final String UC_ITEM = "ITEM";
    }

    public static final class Dirs {
        private Dirs() {
        }

        public static final String GROUPS = "groups";
        public static final String IMAGES = "images";
        public static final String PACKAGES = "packages";
    }

    public static final class Roles {
        private Roles() {
        }

        public static final String BANNER = "banner";
        public static final String MAIN = "main";
        public static final String NAVIGATION = "navigation";
    }

}