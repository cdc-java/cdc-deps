package cdc.deps.io.html.model;

import cdc.util.lang.Checks;

/**
 * Description of a name, which can be a simple string or an hyperlink,
 * and have an optional icon:
 * <ul>
 * <li><b>text</b>: string to display.
 * <li><b>href</b>: optional hyperlink url.
 * <li><b>target</b>: optional place where the url must be displayed.<br>
 * Meaningful when href is used.
 * <li><b>icon</b>: optional name of the associated image.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class DName {
    /** The name text. */
    private final String text;
    /** The optional name href. */
    private final String href;
    /** The optional name target. */
    private final String target;
    /** The optional name img src. */
    private final String imgSrc;

    protected DName(String text,
                    String href,
                    String target,
                    String imgSrc) {
        this.text = Checks.isNotNullOrEmpty(text, "text");
        this.href = href;
        this.target = target;
        this.imgSrc = imgSrc;
    }

    public String getText() {
        return text;
    }

    public String getHref() {
        return href;
    }

    public String getTarget() {
        return target;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> {
        protected String text;
        protected String href;
        protected String target;
        protected String imgSrc;

        protected Builder() {
            super();
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B text(String text) {
            this.text = text;
            return self();
        }

        public B text(int value) {
            this.text = Integer.toString(value);
            return self();
        }

        public B href(String href) {
            this.href = href;
            return self();
        }

        public B target(String target) {
            this.target = target;
            return self();
        }

        public B imgSrc(String imgSrc) {
            this.imgSrc = imgSrc;
            return self();
        }

        public DName build() {
            return new DName(text,
                             href,
                             target,
                             imgSrc);
        }
    }
}