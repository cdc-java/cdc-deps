package cdc.deps.io.html.writer;

import java.io.IOException;

import cdc.deps.io.html.Params;
import cdc.deps.io.html.model.DIndex;
import cdc.deps.io.html.model.DIndexEntry;
import cdc.deps.io.html.model.DMenuItem;
import cdc.io.html.HtmlWriter;

/**
 * Base class used to generate an index frame.
 * <p>
 * It contains:
 * <ul>
 * <li>A head (meta data)
 * <li>A H1 title
 * <li>An optional menu
 * <li>A H2 subtitle
 * <li>A list
 * </ul>
 *
 * @author Damien Carbonne
 */
class IndexFrame extends AbstractGenerator {
    private final DIndex<?> data;
    private final String upPath;

    protected IndexFrame(HtmlWriterGenerator context,
                         DIndex<?> data,
                         String upPath) {
        super(context);
        this.data = data;
        this.upPath = upPath;
    }

    protected IndexFrame(HtmlWriterGenerator context,
                         DIndex<?> data) {
        this(context, data, ".");
    }

    @Override
    public void accept(HtmlWriter writer) throws IOException {
        writer.html();
        head(writer, data.getTitle().getText(), upPath);

        writer.body();

        writer.h1().attrClass(Params.Classes.BAR);
        content(writer, data.getTitle());
        writer.end(); // h1

        if (data.getMenu() != null) {
            nav(writer);
        }
        main(writer);
        writer.end(); // body

        writer.end(); // html
    }

    public void nav(HtmlWriter writer) throws IOException {
        writer.nav().attrClass(Params.Classes.INDEX_NAV);
        writer.ul();
        for (final DMenuItem item : data.getMenu().getItems()) {
            writer.li();
            content(writer, item);
            writer.end(); // li
        }
        writer.end(); // ul
        writer.end(); // nav
    }

    private void main(HtmlWriter writer) throws IOException {
        writer.main().attrClass(Params.Classes.INDEX_CONTAINER).attrRole(Params.Roles.MAIN);

        writer.h2();
        content(writer, data.getSubtitle());
        writer.end(); // h2

        writer.ul();
        for (final DIndexEntry<?> entry : data.getEntries()) {
            writer.li();
            content(writer, entry);
            writer.end(); // li
        }
        writer.end(); // ul

        writer.end(); // main
    }
}