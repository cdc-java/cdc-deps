package cdc.deps.io.office;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DDependencyLevel;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.lang.CollectionUtils;

/**
 * Utilities to export a dependency analysis to workbooks.
 * <p>
 * All data are written to one file, in several sheets, whatever the format is (including csv).
 *
 * @author Damien Carbonne
 *
 */
public final class DAnalysisWorkbookWriter {
    private DAnalysisWorkbookWriter() {
    }

    /**
     * Writes an analysis to an office file.
     * <p>
     * Supported formats are defined in {@link cdc.office.ss.WorkbookKind WorkbookKind}.<br>
     * Filename extension must match a supported one.
     *
     * @param analysis The analysis.
     * @param file The file.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code file} kind is not recognized.
     */
    public static void write(DAnalysis analysis,
                             File file) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        try (final WorkbookWriter<?> writer = factory.create(file, WorkbookWriterFeatures.STANDARD_FAST)) {
            writeElements(analysis, writer, "Packages", DElementKind.PACKAGE);
            writeElements(analysis, writer, "Items", DElementKind.ITEM);
            writeElements(analysis, writer, "Groups", DElementKind.GROUP);
            writeDependencies(analysis, writer);
        }
    }

    /**
     * Writes an analysis to an office file.
     * <p>
     * Supported formats are defined in {@link cdc.office.ss.WorkbookKind WorkbookKind}.<br>
     * Filename extension must match a supported one.
     *
     * @param analysis The analysis.
     * @param filename The file name.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code filename} kind is not recognized.
     */
    public static void write(DAnalysis analysis,
                             String filename) throws IOException {
        write(analysis, new File(filename));
    }

    private static void writeElements(DAnalysis analysis,
                                      WorkbookWriter<?> writer,
                                      String sheetName,
                                      DElementKind kind) throws IOException {

        writer.beginSheet(sheetName);
        writer.beginRow(TableSection.HEADER);
        writer.addCells("Id",
                        "Name",
                        "Scope",
                        "Category",
                        "Features",
                        "Parent Id",
                        "Parent Name",
                        "Parent Category");
        for (final DElement element : sort(analysis.getRoots())) {
            writeDef(element, writer, kind);
        }
    }

    private static String getFeatures(DElement element) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final String feature : element.getFeatures()) {
            if (!first) {
                builder.append(' ');
            }
            builder.append(feature);
            first = false;
        }
        return builder.toString();
    }

    private static void writeDef(DElement element,
                                 WorkbookWriter<?> writer,
                                 DElementKind kind) throws IOException {
        if (element.getKind() == kind) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(element.getId());
            writer.addCell(element.getName());
            writer.addCell(element.getScope());
            writer.addCell(element.getCategory());
            writer.addCell(getFeatures(element));
            if (element.getParent() == null) {
                writer.addCell(0);
                writer.addEmptyCell();
                writer.addEmptyCell();
            } else {
                writer.addCell(element.getParent().getId());
                writer.addCell(element.getParent().getName());
                writer.addCell(element.getParent().getCategory());
            }
        }

        // Recurse
        for (final DElement child : sort(element.getChildren())) {
            if (element.getKind() != DElementKind.GROUP || child.getKind() == DElementKind.GROUP) {
                writeDef(child, writer, kind);
            }
        }
    }

    private static void writeDependencies(DAnalysis analysis,
                                          WorkbookWriter<?> writer) throws IOException {
        writer.beginSheet("Dependencies");
        writer.beginRow(TableSection.HEADER);
        writer.addCells("Source Id",
                        "Source Name",
                        "Target Id",
                        "Target Name",
                        "Level",
                        "Primitive Count",
                        "Derived Count");

        // Iterate on roots
        for (final DElement element : sort(analysis.getRoots())) {
            writeDeps(element, writer);
        }
    }

    private static void writeDeps(DElement element,
                                  WorkbookWriter<?> writer) throws IOException {
        // Explicit dependencies
        for (final DDependency dep : sortDeps(element.getOutgoingDependencies())) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(dep.getSource().getId());
            writer.addCell(dep.getSource().getName());
            writer.addCell(dep.getTarget().getId());
            writer.addCell(dep.getTarget().getName());
            writer.addCell("DEPENDENCY");
            writer.addCell(dep.getCount(DDependencyLevel.PRIMITIVE));
            writer.addCell(dep.getCount(DDependencyLevel.DERIVED));
        }

        // Composition dependencies
        for (final DElement child : sort(element.getChildren())) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(element.getId());
            writer.addCell(element.getName());
            writer.addCell(child.getId());
            writer.addCell(child.getName());
            writer.addCell("COMPOSITION");
            writer.addCell(1);
            writer.addCell(0);
        }

        // Recurse
        for (final DElement child : sort(element.getChildren())) {
            if (element.getKind() != DElementKind.GROUP || child.getKind() == DElementKind.GROUP) {
                writeDeps(child, writer);
            }
        }
    }

    private static List<? extends DElement> sort(Collection<? extends DElement> elements) {
        return CollectionUtils.toSortedList(elements, DElement.ID_COMPARATOR);
    }

    private static List<DDependency> sortDeps(Collection<DDependency> dependencies) {
        return CollectionUtils.toSortedList(dependencies, DDependency.TARGET_ID_COMPARATOR);
    }
}