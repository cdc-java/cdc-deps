package cdc.deps.io.office;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DDependencyLevel;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.office.csv.CsvWriter;
import cdc.util.lang.CollectionUtils;

/**
 * Utilities to export a dependency analysis to CSV files.
 *
 * @author Damien Carbonne
 *
 */
public final class DAnalysisCsvWriter {
    private DAnalysisCsvWriter() {
    }

    public static void writeElements(DAnalysis analysis,
                                     File file) throws IOException {
        writeElements(analysis, new CsvWriter(file));
    }

    public static void writeElements(DAnalysis analysis,
                                     String filename) throws IOException {
        writeElements(analysis, new CsvWriter(new File(filename)));
    }

    public static void writeElements(DAnalysis analysis,
                                     CsvWriter writer) throws IOException {
        writer.writeln("Id",
                       "Name",
                       "Scope",
                       "Category",
                       "Features",
                       "Parent Id",
                       "Parent Name",
                       "Parent Category");
        // Write root packages
        for (final DElement element : sort(analysis.getRoots())) {
            if (element.getKind() == DElementKind.PACKAGE) {
                writeDef(element, writer);
            }
        }

        // Write root items
        for (final DElement element : sort(analysis.getRoots())) {
            if (element.getKind() == DElementKind.ITEM) {
                writeDef(element, writer);
            }
        }

        // Write root groups
        for (final DElement element : sort(analysis.getRoots())) {
            if (element.getKind() == DElementKind.GROUP) {
                writeDef(element, writer);
            }
        }
        writer.close();
    }

    public static void writeDependencies(DAnalysis analysis,
                                         File file) throws IOException {
        writeDependencies(analysis, new CsvWriter(file));
    }

    public static void writeDependencies(DAnalysis analysis,
                                         String filename) throws IOException {
        writeDependencies(analysis, new CsvWriter(new File(filename)));
    }

    public static void writeDependencies(DAnalysis analysis,
                                         CsvWriter writer) throws IOException {
        writer.writeln("Source Id",
                       "Source Name",
                       "Target Id",
                       "Target Name",
                       "Level",
                       "Primitive Count",
                       "Derived Count");

        // Iterate on roots
        for (final DElement element : sort(analysis.getRoots())) {
            writeDeps(element, writer);
        }
        writer.close();
    }

    private static void writeDef(DElement element,
                                 CsvWriter writer) throws IOException {
        writer.write(element.getId());
        writer.write(element.getName());
        writer.write(element.getScope());
        writer.write(element.getCategory());
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final String feature : element.getFeatures()) {
            if (!first) {
                builder.append(' ');
            }
            builder.append(feature);
            first = false;
        }
        writer.write(builder.toString());
        if (element.getParent() == null) {
            writer.write(0);
            writer.write("");
            writer.write("");
        } else {
            writer.write(element.getParent().getId());
            writer.write(element.getParent().getName());
            writer.write(element.getParent().getCategory());
        }
        writer.writeln();
        for (final DElement child : sort(element.getChildren())) {
            if (element.getKind() != DElementKind.GROUP || child.getKind() == DElementKind.GROUP) {
                writeDef(child, writer);
            }
        }
    }

    private static void writeDeps(DElement element,
                                  CsvWriter writer) throws IOException {
        // Explicit dependencies
        for (final DDependency dep : sortDeps(element.getOutgoingDependencies())) {
            writer.write(dep.getSource().getId());
            writer.write(dep.getSource().getName());
            writer.write(dep.getTarget().getId());
            writer.write(dep.getTarget().getName());
            writer.write("DEPENDENCY");
            writer.write(dep.getCount(DDependencyLevel.PRIMITIVE));
            writer.write(dep.getCount(DDependencyLevel.DERIVED));
            writer.writeln();
        }

        // Composition dependencies
        for (final DElement child : sort(element.getChildren())) {
            writer.write(element.getId());
            writer.write(element.getName());
            writer.write(child.getId());
            writer.write(child.getName());
            writer.write("COMPOSITION");
            writer.write(1);
            writer.write(0);
            writer.writeln();
        }

        // Recurse
        for (final DElement child : sort(element.getChildren())) {
            if (element.getKind() != DElementKind.GROUP || child.getKind() == DElementKind.GROUP) {
                writeDeps(child, writer);
            }
        }
    }

    private static List<? extends DElement> sort(Collection<? extends DElement> elements) {
        return CollectionUtils.toSortedList(elements, DElement.ID_COMPARATOR);
    }

    private static List<DDependency> sortDeps(Collection<DDependency> dependencies) {
        return CollectionUtils.toSortedList(dependencies, DDependency.TARGET_ID_COMPARATOR);
    }
}