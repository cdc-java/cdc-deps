package cdc.deps.io.gv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;

import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DNamePart;
import cdc.deps.graphs.DDirection;
import cdc.deps.graphs.DElementClosureGraph;
import cdc.deps.graphs.DElementDirectGraph;
import cdc.deps.graphs.DElementGraph;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.graphs.EdgeDirection;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvRankDir;
import cdc.util.files.Files;
import cdc.util.lang.UnexpectedValueException;

/**
 * Generator for graph centered on a reference element.
 * <p>
 * The reference element is displayed, with out or in dependencies.
 *
 * @author Damien Carbonne
 *
 */
final class ElementGenerator extends AbstractGenerator {
    private final DElement ref;
    private final DDirection direction;
    private final boolean closure;
    private DElementGraph filtered;

    private ElementGenerator(DepsToGv context,
                             DElement ref,
                             DDirection direction,
                             int index,
                             int total,
                             boolean closure) {
        super(context, index, total);
        this.ref = ref;
        this.direction = direction;
        this.closure = closure;
    }

    private boolean isRefRelated(DDependency dependency) {
        return dependency.getSource() == ref || dependency.getTarget() == ref;
    }

    private Saturation getSaturation(DDependency dependency) {
        return isRefRelated(dependency) ? Saturation.NORMAL : Saturation.DIMMED;
    }

    private boolean isRefRelated(DElement element) {
        if (element == ref) {
            return true;
        } else {
            for (final DDependency edge : filtered.getEdges(ref, EdgeDirection.OUTGOING)) {
                if (edge.getTarget() == element) {
                    return true;
                }
            }
            return false;
        }
    }

    private Saturation getSaturation(DElement element) {
        return isRefRelated(element) ? Saturation.NORMAL : Saturation.DIMMED;
    }

    /**
     * Returns true when an element is accepted for graph generation.
     *
     * @param context The context.
     * @param kind The element kind.
     * @param element The element.
     * @return True if element is accepted for graph generation.
     */
    private static boolean isAccepted(DepsToGv context,
                                      DElementKind kind,
                                      DElement element) {
        return element.getKind() == kind
                && (kind != DElementKind.GROUP || element.getParent() == null)
                && (element.getScope().isMixedOrInternal()
                        || (element.getScope() == DElementScope.EXTERNAL
                                && !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL)));
    }

    /**
     * Returns the number of files that will be generated for analysis.
     *
     * @param context The context.
     * @param kind The element kind.
     * @return The number of files to generate for analysis.
     */
    private static int getTotal(DepsToGv context,
                                DElementKind kind) {
        int count = 0;
        for (final DElement element : context.analysis.getElements()) {
            if (isAccepted(context, kind, element)) {
                count++;
            }
        }
        return count;
    }

    static void addAll(DepsToGv context,
                       DElementKind kind,
                       DDirection direction,
                       boolean closure,
                       List<Callable<Void>> callables) {
        final int total = getTotal(context, kind);
        int index = 0;
        for (final DElement element : context.analysis.getElements()) {
            if (isAccepted(context, kind, element)) {
                index++;
                final Callable<Void> callable = new ElementGenerator(context,
                                                                     element,
                                                                     direction, index, total, closure);
                callables.add(callable);
            }
        }
    }

    /**
     * Generates all files for a given analysis context, element kind and direction.
     *
     * @param context The analysis context.
     * @param kind The element kind.
     * @param direction The dependencies direction.
     * @param closure If {@code true}, closure are included.
     *            Otherwise, only direct dependencies are included.
     */
    static void generateAll(DepsToGv context,
                            DElementKind kind,
                            DDirection direction,
                            boolean closure) {
        final List<Callable<Void>> callables = new ArrayList<>();
        addAll(context, kind, direction, closure, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        final String basename = Files.toValidBasename(ref.getName());
        return ref.getKind().getName() + "-" + direction.getName() + "-" + basename;
    }

    @Override
    protected DElement getRefElement() {
        return ref;
    }

    @Override
    protected void execute() throws Exception {
        DepsToGv.LOGGER.info(format(index, total, getGraphName()));

        try {
            if (closure) {
                filtered = new DElementClosureGraph(context.analysis,
                                                    ref,
                                                    direction,
                                                    !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL),
                                                    !context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));
            } else {
                filtered = new DElementDirectGraph(context.analysis,
                                                   ref,
                                                   direction,
                                                   !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL),
                                                   !context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));
            }
            final HashSet<DElement> highlightedElements = new HashSet<>();
            highlightedElements.add(ref);

            final File tmp = newTmpFile();
            try (final GvWriter writer = new GvWriter(tmp)) {
                writer.beginGraph(getGraphName(), true, getGraphAttributes().setRankDir(GvRankDir.TB));

                switch (ref.getKind()) {
                case ITEM:
                    printPackagesContent(writer,
                                         filtered.getNodes(),
                                         filtered.getEdges(),
                                         highlightedElements,
                                         context.margs.isEnabled(MainArgs.Feature.PACKAGED_ITEMS) ? ItemsProcessing.PACKAGED_ITEMS
                                                 : ItemsProcessing.NAKED_ITEMS,
                                         DependenciesProcessing.DEPENDENCIES);

                    printInterPackagesContent(writer, filtered.getEdges());
                    break;

                case PACKAGE:
                    writer.addComment("Packages");
                    for (final DElement element : filtered.getNodes()) {
                        writer.addNode(getId(element),
                                       getNodeAtts(element,
                                                   DNamePart.NAME,
                                                   getSaturation(element),
                                                   element == ref ? Highlight.HIGHLIGHT : Highlight.NORMAL));
                    }

                    writer.addComment("Dependencies");
                    for (final DDependency dependency : filtered.getEdges()) {
                        generateDependencyNode(dependency, writer);
                        generateDependencyEdges(dependency, getSaturation(dependency), writer);
                    }
                    break;

                case GROUP:
                    writer.addComment("Root Groups");
                    for (final DElement element : filtered.getNodes()) {
                        writer.addNode(getId(element),
                                       getNodeAtts(element,
                                                   DNamePart.NAME,
                                                   getSaturation(element),
                                                   element == ref ? Highlight.HIGHLIGHT : Highlight.NORMAL));
                    }

                    writer.addComment("Dependencies");
                    for (final DDependency dependency : filtered.getEdges()) {
                        generateDependencyNode(dependency, writer);
                        generateDependencyEdges(dependency, getSaturation(dependency), writer);
                    }
                    break;

                default:
                    throw new UnexpectedValueException(ref.getKind());
                }

                writer.endGraph();
                writer.flush();

                replaceIfNecessary(tmp, getGvFile());
                context.generateGvImages(getGvFile());
            }
        } catch (final IOException e) {
            DepsToGv.LOGGER.catching(e);
        }
    }
}