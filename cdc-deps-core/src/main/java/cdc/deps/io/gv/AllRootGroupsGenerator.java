package cdc.deps.io.gv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DNamePart;
import cdc.deps.graphs.DAnalysisGraph;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.gv.GvWriter;

/**
 * Generator that generates one graph with all selected root groups and their dependencies.
 *
 * @author Damien Carbonne
 */
final class AllRootGroupsGenerator extends AbstractGenerator {
    private AllRootGroupsGenerator(DepsToGv context) {
        super(context, 1, 1);
    }

    static void addAll(DepsToGv context,
                       List<Callable<Void>> callables) {
        final Callable<Void> callable = new AllRootGroupsGenerator(context);
        callables.add(callable);
    }

    /**
     * Generates all files (one in that case) for a given analysis context.
     *
     * @param context The analysis context.
     */
    static void generateAll(DepsToGv context) {
        final List<Callable<Void>> callables = new ArrayList<>();
        addAll(context, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        return wrapGraphName("all-root-groups");
    }

    @Override
    protected DElement getRefElement() {
        return null;
    }

    @Override
    protected String getPathToBaseDir() {
        return "groups/";
    }

    @Override
    protected void execute() throws Exception {
        // Construct the corresponding graph
        final DAnalysisGraph graph = new DAnalysisGraph(context.analysis);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES, false);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS, true);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS_DEPS, true);
        graph.setEnabled(DAnalysisGraph.Feature.INTERNAL, true);
        graph.setEnabled(DAnalysisGraph.Feature.EXTERNAL, !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL));
        graph.setEnabled(DAnalysisGraph.Feature.UNKNOWN, !context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));

        final RestrictionSubGraph<DElement, DDependency> filtered = filterCycles(graph);

        final String gname = getGraphName();

        DepsToGv.LOGGER.info(format(index, total, gname));

        final File tmp = newTmpFile();
        try (final GvWriter writer = new GvWriter(tmp)) {
            writer.beginGraph(getGraphName(), true, getGraphAttributes());

            writer.addComment("Root Groups");
            for (final DElement element : filtered.getNodes()) {
                writer.addNode(getId(element),
                               getNodeAtts(element,
                                           DNamePart.NAME,
                                           Saturation.NORMAL,
                                           Highlight.NORMAL));
            }

            writer.addComment("Dependencies");
            for (final DDependency dependency : filtered.getEdges()) {
                generateDependencyNode(dependency, writer);
                generateDependencyEdges(dependency, Saturation.NORMAL, writer);
            }

            writer.endGraph();
            writer.flush();
            replaceIfNecessary(tmp, getGvFile());
            context.generateGvImages(getGvFile());
        }
    }
}