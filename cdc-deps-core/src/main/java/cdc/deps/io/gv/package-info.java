/**
 * Generation of miscellaneous GraphViz files and images from an analysis.
 *
 * @author Damien Carbonne
 */
package cdc.deps.io.gv;