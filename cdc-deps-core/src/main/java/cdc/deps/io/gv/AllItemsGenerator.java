package cdc.deps.io.gv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.graphs.DAnalysisGraph;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.gv.GvWriter;

/**
 * Generator that generates one graph with all selected items and their dependencies.
 *
 * @author Damien Carbonne
 */
final class AllItemsGenerator extends AbstractGenerator {
    private AllItemsGenerator(DepsToGv context) {
        super(context, 1, 1);
    }

    static void addAll(DepsToGv context,
                       List<Callable<Void>> callables) {
        final Callable<Void> callable = new AllItemsGenerator(context);
        callables.add(callable);
    }

    /**
     * Generates all files for a given analysis context.
     *
     * @param context The analysis context.
     */
    static void generateAll(DepsToGv context) {
        final ArrayList<Callable<Void>> callables = new ArrayList<>();
        addAll(context, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        return wrapGraphName("all-items");
    }

    @Override
    protected DElement getRefElement() {
        return null;
    }

    @Override
    protected String getPathToBaseDir() {
        return "packages/";
    }

    @Override
    protected void execute() throws Exception {
        // Construct the corresponding graph
        final DAnalysisGraph graph = new DAnalysisGraph(context.analysis);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS, true);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, true);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES, false);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.INTERNAL, true);
        graph.setEnabled(DAnalysisGraph.Feature.EXTERNAL, !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL));
        graph.setEnabled(DAnalysisGraph.Feature.UNKNOWN, !context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));

        final RestrictionSubGraph<DElement, DDependency> filtered = filterCycles(graph);

        DepsToGv.LOGGER.info(format(index, total, getGraphName()));

        final File tmp = newTmpFile();
        try (final GvWriter writer = new GvWriter(tmp)) {
            writer.beginGraph(getGraphName(), true, getGraphAttributes());

            printPackagesContent(writer,
                                 filtered.getNodes(),
                                 filtered.getEdges(),
                                 null,
                                 context.margs.isEnabled(MainArgs.Feature.PACKAGED_ITEMS) ? ItemsProcessing.PACKAGED_ITEMS
                                         : ItemsProcessing.NAKED_ITEMS,
                                 DependenciesProcessing.DEPENDENCIES);

            printInterPackagesContent(writer, filtered.getEdges());

            writer.endGraph();
            writer.flush();
            replaceIfNecessary(tmp, getGvFile());
            context.generateGvImages(getGvFile());
        }
    }
}