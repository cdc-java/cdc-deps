package cdc.deps.io.gv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DDependency;
import cdc.deps.DDependencyLevel;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DItem;
import cdc.deps.DNamePart;
import cdc.deps.DNamespace;
import cdc.deps.DPackage;
import cdc.deps.graphs.CyclePosition;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.deps.io.gv.DepsToGv.MainArgs.Feature;
import cdc.deps.io.gv.styles.Style;
import cdc.deps.io.gv.styles.Style.Mode;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvArrowType;
import cdc.gv.atts.GvClusterAttributes;
import cdc.gv.atts.GvClusterRank;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvEdgeStyle;
import cdc.gv.atts.GvGraphAttributes;
import cdc.gv.atts.GvLabelJust;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.atts.GvNodeShape;
import cdc.gv.atts.GvNodeStyle;
import cdc.gv.atts.GvRankType;
import cdc.gv.atts.GvSubgraphAttributes;
import cdc.gv.colors.GvColor;
import cdc.gv.colors.GvX11Colors;
import cdc.gv.labels.GvHtmlLabel;
import cdc.gv.labels.GvTableAttributes;
import cdc.util.files.Files;
import cdc.util.lang.UnexpectedValueException;
import cdc.util.strings.StringUtils;

/**
 * Abstract base class used to generate one Graph.
 *
 * @author Damien Carbonne
 *
 */
abstract class AbstractGenerator implements Callable<Void> {
    private static final Logger LOGGER = LogManager.getLogger(AbstractGenerator.class);
    protected final DepsToGv context;
    /** Index of the graph to generate with this generator. */
    protected final int index;
    /** Total number of graphs to generate with this generator. */
    protected final int total;

    protected static final GvSubgraphAttributes MAX_NODE_GRAPH = new GvSubgraphAttributes().setRank(GvRankType.MAX);

    private static final GvColor GLOBAL_CYCLE_NORMAL = new GvColor("#FF0000");
    private static final GvColor LOCAL_CYCLE_NORMAL = new GvColor("#FFAA00");
    private static final GvColor NO_CYCLE_NORMAL = new GvColor("#000000");

    private static final GvColor GLOBAL_CYCLE_DIMMED = new GvColor("#FFAAAA");
    private static final GvColor LOCAL_CYCLE_DIMMED = new GvColor("#FFCCAA");
    private static final GvColor NO_CYCLE_DIMMED = new GvColor("#999999");

    protected static final GvNodeAttributes INVIS_NODE =
            new GvNodeAttributes().setStyle(GvNodeStyle.INVIS)
                                  .setShape(GvNodeShape.BOX)
                                  .setHeight(0.0)
                                  .setWidth(0.0)
                                  .setLabel("");

    protected static final GvEdgeAttributes INVIS_EDGE =
            new GvEdgeAttributes().setStyle(GvEdgeStyle.INVIS);

    public AbstractGenerator(DepsToGv context,
                             int index,
                             int total) {
        this.context = context;
        this.index = index;
        this.total = total;
    }

    @Override
    public final Void call() throws Exception {
        try {
            execute();
        } catch (final Exception e) {
            LOGGER.error("call() FAILD for: " + getGvFile(), e);
            throw e;
        }
        return null;
    }

    /**
     * Returns the graph file.
     * <p>
     * It is built as: output directory/graph name + ".gv"
     *
     * @return The graph file.
     */
    public final File getGvFile() {
        return new File(context.margs.output, getGraphName() + ".gv");
    }

    /**
     * Returns the name to use for the generated graph.
     * <p>
     * The file name is deduced from thius name.
     *
     * @return The graph name.
     */
    protected abstract String getGraphName();

    /**
     * Modifies a name, by appending a suffix related to options.
     * <p>
     * This can be used to make a name depend on options.<br>
     * At the moment, cycles display options are taken into account.
     *
     * @param name The name.
     * @return The possibly modified version of {@code name}.
     */
    protected final String wrapGraphName(String name) {
        if (context.margs.isEnabled(DepsToGv.MainArgs.Feature.SHOW_ONLY_CYCLES)) {
            return name + "-cycles";
        } else if (context.margs.isEnabled(DepsToGv.MainArgs.Feature.SHOW_ONLY_GLOBAL_CYCLES)) {
            return name + "-global-cycles";
        } else {
            return name;
        }
    }

    /**
     * @return The reference element (onto which the graph is centered), or {@code null}.
     */
    protected abstract DElement getRefElement();

    /**
     * Returns the path from the reference element to base directory.
     * If there is no reference element, return an empty path.<br>
     * <b>WARNING:</b> This must be kept consistent with html generation conventions.
     *
     * @return The path from the reference element.
     */
    protected String getPathToBaseDir() {
        final DElement ref = getRefElement();
        if (ref == null) {
            return "";
        } else {
            final int shift = (ref.getKind() == DElementKind.PACKAGE || ref.getKind() == DElementKind.GROUP) ? 1 : 0;
            final int count = StringUtils.countMatches(ref.getName(), '/') + shift;
            return Files.toUpPath(count);
        }
    }

    /**
     * Builds the graph.
     *
     * @throws Exception When an exception occurs.
     */
    protected abstract void execute() throws Exception;

    protected enum Highlight {
        NORMAL,
        HIGHLIGHT
    }

    protected enum DependencyPart {
        TAIL,
        HEAD,
        FULL
    }

    protected enum ItemsProcessing {
        NAKED_ITEMS,
        PACKAGED_ITEMS
    }

    protected enum DependenciesProcessing {
        NO_DEPENDENCIES,
        DEPENDENCIES
    }

    protected static String format(int index,
                                   int total,
                                   String message) {
        final int digits = (int) Math.ceil(Math.log10(total + 1.0));
        final String format = "   %" + digits + "d/" + total + " %s%n";
        return String.format(format, index, message);
    }

    protected final boolean showDependenciesWithNodes() {
        return context.margs.isEnabled(MainArgs.Feature.EDGE_LABELS_AS_NODES)
                && context.margs.isEnabled(MainArgs.Feature.SHOW_DEPENDENCIES_COUNTS);
    }

    private boolean showDependenciesWithoutNodes() {
        return !context.margs.isEnabled(MainArgs.Feature.EDGE_LABELS_AS_NODES)
                && context.margs.isEnabled(MainArgs.Feature.SHOW_DEPENDENCIES_COUNTS);
    }

    /**
     * Returns true if an element is visible in the graph.
     * <p>
     * This depends on element scope and enabled features.
     *
     * @param element The element.
     * @return True if element is visible in the graph.
     */
    private boolean isVisible(DElement element) {
        return !(element.getScope() == DElementScope.UNKNOWN && context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN))
                && !(element.getScope() == DElementScope.EXTERNAL && context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL));
    }

    /**
     * Returns the id associated to an element.
     *
     * @param element The element.
     * @return The id associated to element.
     */
    protected static String getId(DElement element) {
        return "N" + element.getId();
    }

    private String getGlobalId(DElement element) {
        return getGraphName() + "-" + element.getKind().name().toLowerCase() + "-" + element.getName();
    }

    /**
     * Returns the id associated to a dependency.
     *
     * @param dependency The dependency.
     * @return The id associated to dependency.
     */
    private static String getId(DDependency dependency) {
        return getId(dependency.getSource()) + "__" + getId(dependency.getTarget());
    }

    private String getURL(DElement element) {
        if (context.margs.isEnabled(Feature.MULTI)) {
            switch (element.getKind()) {
            case ITEM:
                return getPathToBaseDir() + element.getName() + ".html";
            case PACKAGE:
                return getPathToBaseDir() + element.getName() + "/package-overview.html";
            case GROUP:
                return getPathToBaseDir() + Files.toValidRelativePath(element.getName()) + "/root-group-overview.html";
            default:
                throw new UnexpectedValueException(element.getKind());
            }
        } else {
            return "#" + element.getKind().name().toLowerCase() + "-" + element.getName();
        }
    }

    private List<DDependency> filterAndCopy(List<DDependency> list,
                                            EdgeTip filteringTip) {
        final List<DDependency> result = new ArrayList<>();
        if (list != null) {
            for (final DDependency dep : list) {
                final DElement element = dep.getTip(filteringTip);
                if (isVisible(element)) {
                    result.add(dep);
                }
            }
        }
        return result;
    }

    private void appendDependencies(StringBuilder builder,
                                    List<DDependency> deps,
                                    boolean out) {
        final int max = context.margs.tooltipMaxDeps;
        builder.append("\n");
        int pcount = 0;
        int dcount = 0;

        for (int i = 0; i < deps.size(); i++) {
            final DDependency dep = deps.get(i);
            if (i < max) {
                if (out) {
                    builder.append("\n --> ");
                    builder.append(dep.getTarget().getName());
                } else {
                    builder.append("\n <-- ");
                    builder.append(dep.getSource().getName());
                }
                builder.append(" (");
                builder.append(dep.getCount(DDependencyLevel.PRIMITIVE));
                builder.append('+');
                builder.append(dep.getCount(DDependencyLevel.DERIVED));
                builder.append(')');
            } else {
                pcount += dep.getCount(DDependencyLevel.PRIMITIVE);
                dcount += dep.getCount(DDependencyLevel.DERIVED);
            }
        }

        if (max < deps.size()) {
            if (out) {
                builder.append("\n --> ... ");
            } else {
                builder.append("\n <-- ... ");
            }
            builder.append((deps.size() - max));
            builder.append(" more");
            builder.append(" (");
            builder.append(pcount);
            builder.append('+');
            builder.append(dcount);
            builder.append(')');
        }
    }

    private String getTooltip(DElement element) {
        final StringBuilder builder = new StringBuilder();
        builder.append(element.getName());
        if (element.getCategory() != null) {
            builder.append("\ncategory: ");
            builder.append(element.getCategory());
        }

        builder.append("\nkind: ");
        builder.append(element.getKind());
        builder.append("\nscope: ");
        builder.append(element.getScope());

        if (!element.getFeatures().isEmpty()) {
            builder.append("\nfeatures:");
            for (final String feature : element.getSortedFeatures()) {
                builder.append(' ');
                builder.append(feature);
            }
        }

        final List<DDependency> deps = filterAndCopy(element.getOutgoingDependencies(), EdgeTip.TARGET);
        if (!deps.isEmpty()) {
            Collections.sort(deps, DDependency.TARGET_NAME_COMPARATOR);
            appendDependencies(builder, deps, true);
        }

        final List<DDependency> uses = filterAndCopy(element.getIngoingDependencies(), EdgeTip.SOURCE);
        if (!uses.isEmpty()) {
            Collections.sort(uses, DDependency.SOURCE_NAME_COMPARATOR);
            appendDependencies(builder, uses, false);
        }

        return builder.toString();
    }

    private GvEdgeAttributes getDependencyEdgeAtts(DDependency dependency,
                                                   DependencyPart part,
                                                   Saturation saturation) {
        final GvEdgeAttributes result = new GvEdgeAttributes();
        if (part == DependencyPart.TAIL) {
            result.setArrowHead(GvArrowType.NONE);
        } else {
            result.setArrowHead(GvArrowType.NORMAL);
        }
        if (part == DependencyPart.FULL && showDependenciesWithoutNodes()) {
            result.setLabel(dependency.getCount(DDependencyLevel.PRIMITIVE) + " "
                    + dependency.getCount(DDependencyLevel.DERIVED));
            result.setDecorate(true);
        }

        if (context.margs.isEnabled(MainArgs.Feature.ADAPT_EDGE_WIDH_TO_COUNT)) {
            final double ratio = Math.sqrt((double) dependency.getTotalCount() / context.maxCount);
            result.setPenWidth(ratio * 10.0);
        }

        result.setStyle(GvEdgeStyle.SOLID);
        switch (context.getCyclePosition(dependency)) {
        case GLOBAL_CYCLE:
            if (saturation == Saturation.NORMAL) {
                result.setColor(GLOBAL_CYCLE_NORMAL);
                result.setFontColor(GLOBAL_CYCLE_NORMAL);
            } else {
                result.setColor(GLOBAL_CYCLE_DIMMED);
                result.setFontColor(GLOBAL_CYCLE_DIMMED);
            }
            break;
        case LOCAL_CYCLE:
            if (saturation == Saturation.NORMAL) {
                result.setColor(LOCAL_CYCLE_NORMAL);
                result.setFontColor(LOCAL_CYCLE_NORMAL);
            } else {
                result.setColor(LOCAL_CYCLE_DIMMED);
                result.setFontColor(LOCAL_CYCLE_DIMMED);
            }
            break;
        case NO_CYCLE:
            if (saturation == Saturation.NORMAL) {
                result.setColor(NO_CYCLE_NORMAL);
                result.setFontColor(NO_CYCLE_NORMAL);
            } else {
                result.setColor(NO_CYCLE_DIMMED);
                result.setFontColor(NO_CYCLE_DIMMED);
            }
            break;
        default:
            throw new UnexpectedValueException(context.getCyclePosition(dependency));
        }

        return result;
    }

    protected final GvGraphAttributes getGraphAttributes() {
        final GvGraphAttributes result = new GvGraphAttributes();
        result.setOverlap(false)
              .setClusterRank(GvClusterRank.LOCAL)
              .setNodeSep(0.1)
              .setRankSep(0.35)
              .setMaximumSize(100.0, 100.0);// TODO better values

        if (context.margs.isEnabled(MainArgs.Feature.CONCENTRATE)) {
            result.setConcentrate(true);
        }
        if (context.margs.charset != null) {
            result.setCharset(context.margs.charset);
        }
        return result;
    }

    protected final GvClusterAttributes getClusterAttributes(DElement element,
                                                             Highlight highlight) {
        final Style style = DepsToGv.getStyle(element);
        final GvClusterAttributes result = new GvClusterAttributes();
        result.setLabelJust(GvLabelJust.CENTERED)
              .setLabel(getLabel(element, DNamePart.NAME, style, Highlight.NORMAL, true))
              .setColor(DepsToGv.getColor(style, Style.Mode.NORMAL, Style.Key.FOREGROUND_COLOR, GvX11Colors.BLACK))
              .setFontColor(DepsToGv.getColor(style, Style.Mode.NORMAL, Style.Key.FOREGROUND_COLOR, GvX11Colors.BLACK));

        if (highlight == Highlight.HIGHLIGHT) {
            result.setPenWidth(4.0);
        }

        result.setBgColor(DepsToGv.getColor(style, Style.Mode.NORMAL, Style.Key.BACKGROUND_COLOR, null));
        result.setTooltip(getTooltip(element));
        result.setURL(getURL(element));
        return result;
    }

    protected final GvNodeAttributes getDependencyNodeAtts(DDependency dependency) {
        final GvNodeAttributes result = new GvNodeAttributes();
        result.setLabel(dependency.getCount(DDependencyLevel.PRIMITIVE) + " " + dependency.getCount(DDependencyLevel.DERIVED))
              .setShape(GvNodeShape.BOX)
              .setStyle(GvNodeStyle.FILLED)
              .setFillColor(GvX11Colors.WHITE)
              .setMargin(0.03, 0.02)
              .setHeight(0.0)
              .setWidth(0.0);

        switch (context.getCyclePosition(dependency)) {
        case GLOBAL_CYCLE:
            result.setColor(GvX11Colors.RED);
            result.setFontColor(GvX11Colors.RED);
            break;
        case LOCAL_CYCLE:
            result.setColor(GvX11Colors.ORANGE);
            result.setFontColor(GvX11Colors.ORANGE);
            break;
        case NO_CYCLE:
            result.setColor(GvX11Colors.BLACK);
            result.setFontColor(GvX11Colors.BLACK);
            break;
        default:
            // Ignore
            break;
        }

        return result;
    }

    protected final void generateDependencyNode(DDependency dependency,
                                                GvWriter writer) throws IOException {
        if (showDependenciesWithNodes()) {
            writer.addNode(getId(dependency), getDependencyNodeAtts(dependency));
        }
    }

    protected final void generateDependencyEdges(DDependency dependency,
                                                 Saturation saturation,
                                                 GvWriter writer) throws IOException {
        final DElement source = dependency.getSource();
        final DElement target = dependency.getTarget();
        if (showDependenciesWithNodes()) {
            writer.addEdge(getId(source),
                           getId(dependency),
                           getDependencyEdgeAtts(dependency, DependencyPart.TAIL, saturation));
            writer.addEdge(getId(dependency),
                           getId(target),
                           getDependencyEdgeAtts(dependency, DependencyPart.HEAD, saturation));
        } else {
            writer.addEdge(getId(source),
                           getId(target),
                           getDependencyEdgeAtts(dependency, DependencyPart.FULL, saturation));
        }
    }

    protected final String getLabel(DElement element,
                                    DNamePart namePart,
                                    Style style,
                                    Highlight highlight,
                                    boolean html) {
        final String content;
        if (element instanceof DNamespace) {
            content = ((DNamespace) element).getNamePart(namePart);
        } else {
            content = element.getName();
        }
        if (html) {
            final String image = context.getImageFilename(element, style, Style.Mode.NORMAL);

            final GvHtmlLabel label = new GvHtmlLabel();
            label.beginTable(new GvTableAttributes().setBorder(0)
                                                    .setCellBorder(0)
                                                    .setCellPadding(0));
            label.beginRow();
            if (image != null) {
                label.addCellImage(null, image);
            }
            label.beginCell(null);
            label.addText(content);
            label.endCell();
            label.endRow();
            label.endTable();

            return label.toString();
        } else {
            return content;
        }
    }

    protected enum Saturation {
        NORMAL,
        DIMMED;

        public Style.Mode toMode() {
            return this == NORMAL ? Style.Mode.NORMAL : Style.Mode.DIMMED;
        }
    }

    /**
     * Returns the node attributes to be used for an element.
     *
     * @param element The element
     * @param namePart The part of the name to use for label.
     * @param saturation The saturation.
     * @param highlight When HIGHLIGHT, the element is highlighted.
     *            Otherwise, the element is drawn normally.
     * @return The node attributes to use for element.
     */
    protected final GvNodeAttributes getNodeAtts(DElement element,
                                                 DNamePart namePart,
                                                 Saturation saturation,
                                                 Highlight highlight) {

        final Style style = DepsToGv.getStyle(element);
        final Style.Mode mode = saturation.toMode();
        final GvNodeAttributes result = new GvNodeAttributes();
        result.setLabel(getLabel(element, namePart, style, highlight, true))
              .setShape(GvNodeShape.BOX)
              .setMargin(0.025, 0.0)
              .setHeight(0.1);

        switch (highlight) {
        case NORMAL:
            result.setStyle(GvNodeStyle.FILLED);
            break;
        case HIGHLIGHT:
            result.setStyle(GvNodeStyle.FILLED, GvNodeStyle.BOLD);
            result.setPenWidth(4.0);
            break;
        default:
            // Ignore
            break;
        }

        switch (context.getCyclePosition(element)) {
        case GLOBAL_CYCLE:
            if (mode == Mode.DIMMED) {
                result.setColor(GLOBAL_CYCLE_DIMMED);
                result.setFontColor(GLOBAL_CYCLE_DIMMED);
            } else {
                result.setColor(GLOBAL_CYCLE_NORMAL);
                result.setFontColor(GLOBAL_CYCLE_NORMAL);
            }
            break;
        case LOCAL_CYCLE:
            if (mode == Mode.DIMMED) {
                result.setColor(LOCAL_CYCLE_DIMMED);
                result.setFontColor(LOCAL_CYCLE_DIMMED);
            } else {
                result.setColor(LOCAL_CYCLE_NORMAL);
                result.setFontColor(LOCAL_CYCLE_NORMAL);
            }
            break;
        case NO_CYCLE:
            result.setColor(DepsToGv.getColor(style, mode, Style.Key.FOREGROUND_COLOR, GvX11Colors.BLACK));
            result.setFontColor(DepsToGv.getColor(style, mode, Style.Key.FOREGROUND_COLOR, GvX11Colors.BLACK));
            break;
        default:
            // Ignore
            break;
        }

        result.setFillColor(DepsToGv.getColor(style, mode, Style.Key.BACKGROUND_COLOR, GvX11Colors.WHITE));

        result.setURL(getURL(element));
        result.setId(getGlobalId(element));
        result.setTooltip(getTooltip(element));
        return result;
    }

    protected final void printPackagesContent(GvWriter writer,
                                              Iterable<? extends DElement> elements,
                                              Iterable<? extends DDependency> dependencies,
                                              Set<DElement> highlightedElements,
                                              ItemsProcessing itemsProcessing,
                                              DependenciesProcessing dependenciesProcessing) throws IOException {
        // Compute packages
        final HashSet<DPackage> packages = new HashSet<>();
        for (final DElement node : elements) {
            if (node.getKind() == DElementKind.ITEM) {
                packages.add(((DItem) node).getPackage());
            }
        }

        for (final DPackage p : packages) {
            printPackageContent(writer,
                                p,
                                elements,
                                dependencies,
                                highlightedElements,
                                itemsProcessing,
                                dependenciesProcessing);
        }
    }

    /**
     * Print the items contained in one package (p) and in a passed collection
     * (elements). It is possible that the package contains other items.
     * Selected items can be printed naked or packaged. Also print internal dependencies,
     * if asked.
     *
     * @param writer The writer.
     * @param p The package.
     * @param elements The elements.
     * @param dependencies The dependencies.
     * @param highlightedElements The highlighted elements.
     * @param itemsProcessing The items processing.
     * @param dependenciesProcessing The dependency processing
     * @throws IOException If an I/O error occurs.
     */
    private void printPackageContent(GvWriter writer,
                                     DPackage p,
                                     Iterable<? extends DElement> elements,
                                     Iterable<? extends DDependency> dependencies,
                                     Set<DElement> highlightedElements,
                                     ItemsProcessing itemsProcessing,
                                     DependenciesProcessing dependenciesProcessing) throws IOException {
        // Compute items that are in the package
        final HashSet<DItem> packageItems = new HashSet<>();
        for (final DElement node : elements) {
            if (node.getKind() == DElementKind.ITEM && ((DItem) node).getPackage() == p) {
                packageItems.add((DItem) node);
            }
        }

        // Compute dependencies that are in the package
        final HashSet<DDependency> packageDependencies = new HashSet<>();
        for (final DDependency dependency : dependencies) {
            if (dependency.belongsToPackage(p)) {
                packageDependencies.add(dependency);
            }
        }

        writer.addComment("Content of package: " + (p == null ? "null" : p.getName()));
        if (itemsProcessing == ItemsProcessing.NAKED_ITEMS) {
            for (final DItem item : packageItems) {
                final boolean highlighted = highlightedElements != null && highlightedElements.contains(item);
                writer.addNode(getId(item),
                               getNodeAtts(item,
                                           DNamePart.NAME,
                                           Saturation.NORMAL,
                                           highlighted ? Highlight.HIGHLIGHT : Highlight.NORMAL));
            }
            if (dependenciesProcessing == DependenciesProcessing.DEPENDENCIES) {
                for (final DDependency dependency : packageDependencies) {
                    generateDependencyNode(dependency, writer);
                    generateDependencyEdges(dependency, Saturation.NORMAL, writer);
                }
            }
        } else if (itemsProcessing == ItemsProcessing.PACKAGED_ITEMS) {
            boolean highlighted = highlightedElements != null && highlightedElements.contains(p);
            if (p != null) {
                writer.beginCluster(p.getName(),
                                    getClusterAttributes(p, highlighted ? Highlight.HIGHLIGHT : Highlight.NORMAL));
            }
            for (final DItem item : packageItems) {
                highlighted = highlightedElements != null && highlightedElements.contains(item);
                writer.addNode(getId(item),
                               getNodeAtts(item,
                                           DNamePart.BASE_NAME,
                                           Saturation.NORMAL,
                                           highlighted ? Highlight.HIGHLIGHT : Highlight.NORMAL));
            }
            if (dependenciesProcessing == DependenciesProcessing.DEPENDENCIES) {
                for (final DDependency dependency : packageDependencies) {
                    generateDependencyNode(dependency, writer);
                    generateDependencyEdges(dependency, Saturation.NORMAL, writer);
                }
            }
            if (p != null) {
                writer.endCluster();
            }
        }
    }

    protected final void printInterPackagesContent(GvWriter writer,
                                                   Iterable<? extends DDependency> dependencies) throws IOException {
        // Compute dependencies that are between packages
        final HashSet<DDependency> interDependencies = new HashSet<>();
        for (final DDependency dependency : dependencies) {
            if (!dependency.belongsToOnePackage()) {
                interDependencies.add(dependency);
            }
        }

        writer.addComment("Inter packages dependencies");
        for (final DDependency dependency : interDependencies) {
            generateDependencyNode(dependency, writer);
            generateDependencyEdges(dependency, Saturation.NORMAL, writer);
        }
    }

    /**
     * When invoked on a package, collects all items in that package. When
     * invoked on a item, collects all items in the same package. In all cases, collects
     * referenced items.
     *
     * @param namespace The namespace.
     * @param packageItems The items of the package.
     * @param referencedItems The referenced items.
     * @param itemDependencies The dependencies.
     */
    protected final void collectItems(DNamespace namespace,
                                      Set<DItem> packageItems,
                                      Set<DItem> referencedItems,
                                      Set<DDependency> itemDependencies) {
        if (namespace.getKind() == DElementKind.ITEM) {
            if (isVisible(namespace)) {
                packageItems.add((DItem) namespace);
            }
            for (final DDependency dependency : namespace.getOutgoingDependencies()) {
                final DElement target = dependency.getTarget();
                if (target.getKind() == DElementKind.ITEM) {
                    final DItem item = (DItem) target;
                    if (item.getPackage() != namespace && isVisible(item)) {
                        referencedItems.add(item);
                        itemDependencies.add(dependency);
                    }
                }
            }
        }
        for (final DNamespace child : namespace.getChildren()) {
            if (child.getKind() == DElementKind.ITEM) {
                collectItems(child, packageItems, referencedItems, itemDependencies);
            }
        }
    }

    protected final RestrictionSubGraph<DElement, DDependency> filterCycles(GraphAdapter<DElement, DDependency> graph) {
        final RestrictionSubGraph<DElement, DDependency> filtered = new RestrictionSubGraph<>(graph);

        if (context.margs.isEnabled(MainArgs.Feature.SHOW_ONLY_CYCLES)
                || context.margs.isEnabled(MainArgs.Feature.SHOW_ONLY_GLOBAL_CYCLES)) {
            final boolean noLocal = context.margs.isEnabled(MainArgs.Feature.SHOW_ONLY_GLOBAL_CYCLES);

            final HashSet<DElement> nodesToRemove = new HashSet<>();
            for (final DElement node : graph.getNodes()) {
                if (!context.cycles.getCyclesGraph().containsNode(node)
                        || (noLocal && context.getCyclePosition(node) == CyclePosition.LOCAL_CYCLE)) {
                    nodesToRemove.add(node);
                }
            }
            for (final DElement node : nodesToRemove) {
                filtered.removeNode(node);
            }

            final HashSet<DDependency> edgesToRemove = new HashSet<>();
            for (final DDependency edge : graph.getEdges()) {
                if (!context.cycles.getCyclesGraph().containsEdge(edge)) {
                    edgesToRemove.add(edge);
                }
            }
            for (final DDependency edge : edgesToRemove) {
                filtered.removeEdge(edge);
            }
        }
        return filtered;
    }

    /**
     * Executes a list or Callable, either in sequential or parallel mode.
     *
     * @param context The context.
     * @param callables The callables.
     */
    protected static void execute(DepsToGv context,
                                  List<Callable<Void>> callables) {
        context.execute(callables);
    }

    protected File newTmpFile() throws IOException {
        return File.createTempFile("DepsToGv", ".tmp");
    }

    protected void replaceIfNecessary(File tmp,
                                      File target) throws IOException {
        LOGGER.trace("replaceIfNecessary(" + tmp + ", " + target + ")");
        if (!target.exists() || !Files.haveSameContent(tmp, target)) {
            Files.copyFile(tmp, target);
        }
        final boolean deleted = tmp.delete();
        if (!deleted) {
            LOGGER.trace("Failed to delete: " + tmp);
        }
    }
}