package cdc.deps.io.gv.styles;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.data.Child;
import cdc.io.data.Element;
import cdc.io.data.NodeType;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.ui.swing.icons.CompoundIcon;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

public final class Styles {
    private static final Logger LOGGER = LogManager.getLogger(Styles.class);
    private static final Styles INSTANCE = new Styles();
    private final Map<String, Style> map = new HashMap<>();

    private static final String BGCOLOR = "bgcolor";
    private static final String DECORATION = "decoration";
    private static final String DIMMED = "dimmed";
    private static final String FEATURE = "feature";
    private static final String FGCOLOR = "fgcolor";
    private static final String IMAGE = "image";
    private static final String NAME = "name";
    private static final String NORMAL = "normal";
    private static final String REF0 = "ref0";
    private static final String REF1 = "ref1";
    private static final String STYLE = "style";
    private static final String STYLES = "styles";
    private static final String X = "x";
    private static final String Y = "y";

    private Styles() {
    }

    public static Styles getInstance() {
        return INSTANCE;
    }

    public final void register(Style style) {
        Checks.isNotNull(style, STYLE);
        if (map.containsKey(style.getQName())) {
            LOGGER.warn("register(" + style + "), already registered");
        } else {
            map.put(style.getQName(), style);
        }
    }

    public final Set<String> getQNames() {
        return map.keySet();
    }

    public final Style getStyle(String qname,
                                Style def) {
        final Style style = map.get(qname);
        if (style == null && def == null) {
            LOGGER.error("getStyle(" + qname + ") FAILED");
        }
        return style == null ? def : style;
    }

    public static class Loader extends AbstractResourceLoader<Void> {
        public Loader(FailureReaction reaction) {
            super(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            if (STYLES.equals(root.getName())) {
                for (final Child c : root.getChildren()) {
                    if (c.getType() == NodeType.ELEMENT) {
                        final cdc.io.data.Element child = (cdc.io.data.Element) c;
                        if (STYLE.equals(child.getName())) {
                            parseStyle(child, null);
                        }
                    }
                }
            } else {
                unexpectedElement(root, STYLES);
            }
            return null;
        }

        private void parseStyle(Element element,
                                Style parent) {
            final String name = element.getAttributeValue(NAME, null);
            final Style style = new Style(parent, name);
            getInstance().register(style);
            for (final Child c : element.getChildren()) {
                if (c.getType() == NodeType.ELEMENT) {
                    final Element child = (Element) c;
                    switch (child.getName()) {
                    case STYLE:
                        parseStyle(child, style);
                        break;
                    case DECORATION:
                    case IMAGE:
                    case BGCOLOR:
                    case FGCOLOR:
                        parseProperty(child, style, Style.Mode.DEFAULT);
                        break;
                    case NORMAL:
                        parseProperties(child, style, Style.Mode.NORMAL);
                        break;
                    case DIMMED:
                        parseProperties(child, style, Style.Mode.DIMMED);
                        break;
                    default:
                        unexpectedElement(child, STYLE, DIMMED, NORMAL, DECORATION, IMAGE, BGCOLOR, FGCOLOR);
                        break;
                    }
                }
            }
        }

        private void parseProperties(Element element,
                                     Style style,
                                     Style.Mode mode) {
            for (final Child c : element.getChildren()) {
                if (c.getType() == NodeType.ELEMENT) {
                    final Element child = (Element) c;
                    switch (child.getName()) {
                    case DECORATION:
                    case IMAGE:
                    case BGCOLOR:
                    case FGCOLOR:
                        parseProperty(child, style, mode);
                        break;
                    default:
                        unexpectedElement(child, DECORATION, IMAGE, BGCOLOR, FGCOLOR);
                        break;
                    }
                }
            }
        }

        private void parseProperty(Element element,
                                   Style style,
                                   Style.Mode mode) {
            switch (element.getName()) {
            case DECORATION:
                parseDecoration(element, style, mode);
                break;
            case IMAGE:
                style.setLocalValue(mode, Style.Key.IMAGE, element.getText(null));
                style.setLocalValue(mode, Style.Key.IMAGE_HEIGHT_MARGIN, element.getAttributeValue("height-margin", null));
                style.setLocalValue(mode, Style.Key.IMAGE_WIDTH_MARGIN, element.getAttributeValue("width-margin", null));
                break;
            case BGCOLOR:
                style.setLocalValue(mode, Style.Key.BACKGROUND_COLOR, element.getText(null));
                break;
            case FGCOLOR:
                style.setLocalValue(mode, Style.Key.FOREGROUND_COLOR, element.getText(null));
                break;
            default:
                unexpectedElement(element, DECORATION, IMAGE, BGCOLOR, FGCOLOR);
                break;
            }
        }

        private static void parseDecoration(Element element,
                                            Style style,
                                            Style.Mode mode) {
            final String feature = element.getAttributeValue(FEATURE, null);
            final CompoundIcon.Ref ref0 = getRef(element.getElementNamedAt(REF0, 0));
            final CompoundIcon.Ref ref1 = getRef(element.getElementNamedAt(REF1, 0));
            final Element image = element.getElementNamedAt(IMAGE, 0);
            final Decoration decoration = new Decoration(feature, ref0, ref1, image.getText(null));
            style.addLocalDecoration(mode, decoration);
        }

        private static CompoundIcon.Ref getRef(Element element) {
            if (element != null) {
                final double x = element.getAttributeAsDouble(X, 0.0);
                final double y = element.getAttributeAsDouble(Y, 0.0);
                return new CompoundIcon.Ref(x, y);
            } else {
                return new CompoundIcon.Ref(0.0, 0.0);
            }
        }
    }
}