package cdc.deps.io.gv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DItem;
import cdc.deps.graphs.DAnalysisGraph;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.graphs.EdgeDirection;
import cdc.graphs.core.GraphTransitiveClosure;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.gv.GvWriter;
import cdc.util.files.Files;

/**
 * Generator that generates one graph per selected item.
 * <p>
 * This graph contains this selected item and all items that it depends on, directly or indirectly, by transitive closure.
 *
 * @author Damien Carbonne
 */
final class ItemClosureGenerator extends AbstractGenerator {
    private final DItem item;
    private final GraphTransitiveClosure<DElement, DDependency> gtc;

    private ItemClosureGenerator(DepsToGv context,
                                 DItem item,
                                 GraphTransitiveClosure<DElement, DDependency> gtc,
                                 int index,
                                 int total) {
        super(context, index, total);
        this.item = item;
        this.gtc = gtc;
    }

    /**
     * Returns true when an element is accepted for graph generation.
     *
     * @param element The element
     * @return True if element is accepted for graph generation.
     */
    private static boolean isAccepted(DElement element) {
        return element.getKind() == DElementKind.ITEM
                && element.getScope() == DElementScope.INTERNAL;
    }

    /**
     * Returns the number of files that will be generated for analysis.
     *
     * @param analysis The analysis.
     * @return The number of files to generate for analysis.
     */
    private static int getTotal(DAnalysis analysis) {
        int count = 0;
        for (final DElement element : analysis.getElements()) {
            if (isAccepted(element)) {
                count++;
            }
        }
        return count;
    }

    static void addAll(DepsToGv context,
                       List<Callable<Void>> callables) {
        final DAnalysisGraph graph = new DAnalysisGraph(context.analysis);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS, true);
        graph.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, true);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES, false);
        graph.setEnabled(DAnalysisGraph.Feature.PACKAGES_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS_DEPS, false);
        graph.setEnabled(DAnalysisGraph.Feature.INTERNAL, true);
        graph.setEnabled(DAnalysisGraph.Feature.EXTERNAL, !context.margs.isEnabled(MainArgs.Feature.NO_EXTERNAL));
        graph.setEnabled(DAnalysisGraph.Feature.UNKNOWN, !context.margs.isEnabled(MainArgs.Feature.NO_UNKNOWN));

        final GraphTransitiveClosure<DElement, DDependency> gtc = new GraphTransitiveClosure<>(graph);
        final int total = getTotal(context.analysis);
        int index = 0;
        for (final DElement element : context.analysis.getElements()) {
            if (isAccepted(element)) {
                index++;
                final DItem item = (DItem) element;
                final Callable<Void> callable = new ItemClosureGenerator(context, item, gtc, index, total);
                callables.add(callable);
            }
        }
    }

    /**
     * Generates all files for a given analysis context.
     *
     * @param context The analysis context.
     */
    static void generateAll(DepsToGv context) {
        final List<Callable<Void>> callables = new ArrayList<>();
        addAll(context, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        final String basename = Files.toValidBasename(item.getName());
        return "item-closure-" + basename;
    }

    @Override
    protected DElement getRefElement() {
        return item;
    }

    @Override
    protected void execute() throws Exception {
        DepsToGv.LOGGER.info(format(index, total, getGraphName()));

        try {
            final ExplicitSubGraph<DElement, DDependency> closure =
                    gtc.computeTransitiveClosure(item, EdgeDirection.OUTGOING);
            final HashSet<DElement> highlightedElements = new HashSet<>();
            highlightedElements.add(item);

            final File tmp = newTmpFile();
            try (final GvWriter writer = new GvWriter(tmp)) {
                writer.beginGraph(getGraphName(), true, getGraphAttributes());

                printPackagesContent(writer,
                                     closure.getNodes(),
                                     closure.getEdges(),
                                     highlightedElements,
                                     context.margs.isEnabled(MainArgs.Feature.PACKAGED_ITEMS) ? ItemsProcessing.PACKAGED_ITEMS
                                             : ItemsProcessing.NAKED_ITEMS,
                                     DependenciesProcessing.DEPENDENCIES);

                printInterPackagesContent(writer, closure.getEdges());

                writer.endGraph();
                writer.flush();
                replaceIfNecessary(tmp, getGvFile());
                context.generateGvImages(getGvFile());
            }
        } catch (final IOException e) {
            DepsToGv.LOGGER.catching(e);
        }
    }
}