package cdc.deps.io.gv.styles;

import cdc.ui.swing.icons.CompoundIcon;
import cdc.util.lang.Checks;

/**
 * Decoration (icon) corresponding to a feature that can be added to an icon.
 *
 * @author Damien Carbonne
 *
 */
public class Decoration {
    /** Name of the associated feature. */
    private final String feature;
    private final CompoundIcon.Ref ref0;
    private final CompoundIcon.Ref ref1;
    private final String image;

    public Decoration(String feature,
                      CompoundIcon.Ref ref0,
                      CompoundIcon.Ref ref1,
                      String image) {
        Checks.isNotNull(feature, "feature");
        Checks.isNotNull(ref0, "ref0");
        Checks.isNotNull(ref1, "ref1");

        this.feature = feature;
        this.ref0 = ref0;
        this.ref1 = ref1;
        this.image = image;
    }

    public final String getFeature() {
        return feature;
    }

    public final CompoundIcon.Ref getRef0() {
        return ref0;
    }

    public final CompoundIcon.Ref getRef1() {
        return ref1;
    }

    public final String getImage() {
        return image;
    }
}