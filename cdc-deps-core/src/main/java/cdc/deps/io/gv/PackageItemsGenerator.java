package cdc.deps.io.gv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DItem;
import cdc.deps.DNamePart;
import cdc.deps.DPackage;
import cdc.deps.DUtils;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvRankDir;
import cdc.gv.atts.GvRankType;
import cdc.gv.atts.GvSubgraphAttributes;
import cdc.util.files.Files;
import cdc.util.lang.Checks;

/**
 * Generator that generates one graph per selected package.
 * This graph contains all items of the selected package and their dependencies (as items).
 *
 * @author Damien Carbonne
 */
final class PackageItemsGenerator extends AbstractGenerator {
    final DPackage pack;

    private PackageItemsGenerator(DepsToGv context,
                                  DPackage pack,
                                  int index,
                                  int total) {
        super(context, index, total);
        this.pack = pack;
    }

    /**
     * Returns true when an element is accepted for graph generation.
     *
     * @param element The element
     * @return True if element is accepted for graph generation.
     */
    private static boolean isAccepted(DElement element) {
        return element.getKind() == DElementKind.PACKAGE
                && (element.getScope() == DElementScope.INTERNAL
                        || element.getScope() == DElementScope.EXTERNAL
                        || element.getScope() == DElementScope.MIXED);
    }

    /**
     * Returns the number of files that will be generated for analysis.
     *
     * @param analysis The analysis.
     * @return The number of files to generate for analysis.
     */
    private static int getTotal(DAnalysis analysis) {
        int count = 0;
        for (final DElement element : analysis.getElements()) {
            if (isAccepted(element)) {
                count++;
            }
        }
        return count;
    }

    static void addAll(DepsToGv context,
                       List<Callable<Void>> callables) {
        final int total = getTotal(context.analysis);
        int index = 0;
        for (final DElement element : context.analysis.getElements()) {
            if (isAccepted(element)) {
                index++;
                final DPackage pack = (DPackage) element;
                final Callable<Void> callable = new PackageItemsGenerator(context, pack, index, total);
                callables.add(callable);
            }
        }
    }

    /**
     * Generates all files for a given analysis context.
     *
     * @param context The analysis context.
     */
    static void generateAll(DepsToGv context) {
        final List<Callable<Void>> callables = new ArrayList<>();
        addAll(context, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        Checks.isNotNull(pack.getName(), "package.name");
        final String basename = Files.toValidBasename(pack.getName());
        return "package-items-" + basename;
    }

    @Override
    protected DElement getRefElement() {
        return pack;
    }

    @Override
    protected void execute() throws Exception {
        // TODO Use common approach
        // The element is an internal package : let's proceed

        final String gname = getGraphName();

        DepsToGv.LOGGER.info(format(index, total, gname));

        final File tmp = newTmpFile();
        try (final GvWriter writer = new GvWriter(tmp)) {
            writer.beginGraph(gname, true, getGraphAttributes().setRankDir(GvRankDir.TB));
            writer.addComment("Package Items");
            // Set of items contained in the package
            final HashSet<DItem> packageItems = new HashSet<>();
            // Set of referenced items (inside or outside of the package)
            final HashSet<DItem> referencedItems = new HashSet<>();
            final HashSet<DDependency> dependencies = new HashSet<>();
            collectItems(pack, packageItems, referencedItems, dependencies);
            // Set of referenced items
            final HashSet<DItem> cleanReferencedItems = new HashSet<>();
            cleanReferencedItems.addAll(referencedItems);
            cleanReferencedItems.removeAll(packageItems);

            if (context.margs.isEnabled(MainArgs.Feature.PACKAGED_ITEMS)) {
                final Set<DPackage> referencedPackages = DUtils.getPackages(cleanReferencedItems);

                // Target package
                writer.beginCluster(pack.getName(), getClusterAttributes(pack, Highlight.HIGHLIGHT));
                writer.beginSubgraph(null, MAX_NODE_GRAPH);
                writer.addNode(pack.getName(), INVIS_NODE);
                writer.endSubgraph();
                for (final DItem item : packageItems) {
                    writer.addNode(getId(item),
                                   getNodeAtts(item,
                                               DNamePart.BASE_NAME,
                                               Saturation.NORMAL,
                                               Highlight.NORMAL));
                }
                if (showDependenciesWithNodes()) {
                    for (final DDependency dependency : dependencies) {
                        if (dependency.belongsToPackage(pack)) {
                            generateDependencyNode(dependency, writer);
                        }
                    }
                }
                writer.endCluster();

                if (showDependenciesWithNodes()) {
                    for (final DDependency dependency : dependencies) {
                        if (!dependency.belongsToPackage(pack)) {
                            generateDependencyNode(dependency, writer);
                        }
                    }
                }

                // Referenced packages
                for (final DPackage p1 : referencedPackages) {
                    final ArrayList<DItem> pRefItems = new ArrayList<>();
                    for (final DItem item : cleanReferencedItems) {
                        if (item.getPackage() == p1) {
                            pRefItems.add(item);
                        }
                    }
                    writer.beginCluster(p1.getName(),
                                        getClusterAttributes(p1, Highlight.NORMAL).setRank(GvRankType.MAX));
                    writer.addNode(p1.getName(), INVIS_NODE);
                    writer.addEdge(pack.getName(), p1.getName(), INVIS_EDGE);

                    if (context.margs.isEnabled(MainArgs.Feature.COMPACT)) {
                        final int cols = (int) Math.ceil(Math.sqrt(pRefItems.size()));
                        int index = 0;
                        DItem prev = null;
                        for (final DItem item : pRefItems) {
                            if (index % cols == 0) {
                                if (index > 0) {
                                    writer.endSubgraph();
                                }
                                if (prev != null) {
                                    writer.addEdge(getId(prev), getId(item), INVIS_EDGE);
                                }
                                prev = item;
                                writer.beginSubgraph(null, new GvSubgraphAttributes().setRank(GvRankType.SAME));
                            }
                            writer.addNode(getId(item),
                                           getNodeAtts(item,
                                                       DNamePart.BASE_NAME,
                                                       Saturation.DIMMED,
                                                       Highlight.NORMAL));
                            index++;
                        }
                        writer.endSubgraph();
                    } else {
                        for (final DItem item : pRefItems) {
                            writer.addNode(getId(item),
                                           getNodeAtts(item,
                                                       DNamePart.BASE_NAME,
                                                       Saturation.DIMMED,
                                                       Highlight.NORMAL));
                        }
                    }
                    writer.endCluster();
                }
            } else {
                for (final DItem item : packageItems) {
                    writer.addNode(getId(item),
                                   getNodeAtts(item,
                                               DNamePart.NAME,
                                               Saturation.NORMAL,
                                               Highlight.NORMAL));
                }
                for (final DItem item : cleanReferencedItems) {
                    writer.addNode(getId(item),
                                   getNodeAtts(item,
                                               DNamePart.NAME,
                                               Saturation.DIMMED,
                                               Highlight.NORMAL));
                }
            }

            writer.addComment("Dependencies");
            for (final DDependency dependency : dependencies) {
                generateDependencyEdges(dependency, Saturation.NORMAL, writer);
            }

            writer.endGraph();
            writer.flush();
            replaceIfNecessary(tmp, getGvFile());
            context.generateGvImages(getGvFile());
        } catch (final IOException e) {
            DepsToGv.LOGGER.catching(e);
        }
    }
}