package cdc.deps.io.gv;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.graphs.CyclePosition;
import cdc.deps.graphs.DAnalysisCycles;
import cdc.deps.graphs.DDirection;
import cdc.deps.io.gv.styles.Decoration;
import cdc.deps.io.gv.styles.Style;
import cdc.deps.io.gv.styles.Styles;
import cdc.deps.io.xml.DAnalysisXmlLoader;
import cdc.gv.colors.GvColor;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;
import cdc.ui.swing.icons.CompoundIcon;
import cdc.ui.swing.icons.IconUtils;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.files.Files;
import cdc.util.files.Resources;
import cdc.util.files.SearchPath;
import cdc.util.lang.FailureReaction;
import cdc.util.strings.StringConversion;

/**
 * Create GraphViz representations of dependencies and converts them to images.
 *
 * @author Damien Carbonne
 */
public final class DepsToGv {
    protected static final Logger LOGGER = LogManager.getLogger(DepsToGv.class);
    /** Main program arguments. */
    final MainArgs margs;

    /** Loaded analysis. */
    final DAnalysis analysis = new DAnalysis();

    /**
     * Maximum number of dependencies of an element on other elements.
     * Use to have a edge width proportional to the number of dependencies.
     */
    protected int maxCount = 0;

    protected DAnalysisCycles cycles;
    private static final GvEngine ENGINE = GvEngine.DOT;

    /**
     * Program main arguments.
     *
     * @author Damien Carbonne
     */
    public static class MainArgs {
        /** Directories where executables / data files can be found. */
        public SearchPath paths = SearchPath.EMPTY;
        /** Name of the input xml dependency file. */
        public URL input;
        /** Name of the output directory where generated files are created. */
        public File output;
        /** Character Set to use with GraphViz. */
        public String charset;
        /** Name of the directory where images are copied. */
        public File imagesDir;
        /** Name of the xml file describing styles. */
        public URL styles;
        /** Max number of dependencies to display in tooltips. */
        public int tooltipMaxDeps;

        public final Set<String> removalPatterns = new HashSet<>();
        public final Set<String> collapsePatterns = new HashSet<>();

        public final Set<GvFormat> formats = new HashSet<>();

        protected final FeatureMask<Target> targets = new FeatureMask<>();
        protected final FeatureMask<Feature> features = new FeatureMask<>();

        public MainArgs() {
            super();
        }

        // TODO Flat/Tree files structure.
        // TODO usage

        /**
         * Enumeration of possible target diagrams.
         */
        public enum Target implements OptionEnum {
            ALL_ROOT_GROUPS("all-root-groups", "Generate one diagram of all root groups dependencies."),

            ROOT_GROUP_IN_OUT("root-group-in-out", "Generate one diagram per root group, showing all its dependencies and usages."),

            ROOT_GROUP_OUT("root-group-out", "Generate one diagram per root group, showing all its dependencies."),

            ROOT_GROUP_IN("root-group-in", "Generate one diagram per root group, showing all its usages."),

            /**
             * One diagram containing packages and their dependencies. Represented
             * packages depends on other options.
             */
            ALL_PACKAGES("all-packages", "Generate one diagram of all packages dependencies."),

            PACKAGE_IN_OUT("package-in-out", "Generate one diagram per package, showing all its dependencies and usages."),

            PACKAGE_OUT("package-out", "Generate one diagram per package, showing all its dependencies."),

            PACKAGE_IN("package-in", "Generate one diagram per package, showing all its usages."),

            // TODO Montrer les items créant des boucles de packages

            PAIR_OF_PACKAGES("pair-of-packages", "Generate one diagram per relevant pair of packages."),

            /**
             * One diagram containing items and their dependencies. Represented
             * items depend on other options.
             */
            ALL_ITEMS("all-items", "Generate one diagram of all items dependencies."),

            /**
             * One diagram per package containing items of this package and their
             * dependencies (items).
             */
            PACKAGE_ITEMS("package-items", "Generate one diagram per package showing its items dependencies."),

            ITEM_IN_OUT("item-in-out", "Generate one diagram per item, showing all its dependencies and usages."),

            ITEM_OUT("item-out", "Generate one diagram per item, showing all its dependencies."),

            ITEM_IN("item-in", "Generate one diagram per item, showing all its usages."),

            /**
             * One diagram per item containing the item and its dependencies,
             * recursively.
             */
            ITEM_CLOSURE("item-closure", "Generate one diagram per item showing its transitive closure.");

            private final String name;
            private final String description;

            private Target(String name,
                           String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /**
         * Enumeration of possible boolean options.
         */
        public enum Feature implements OptionEnum {
            VERBOSE("verbose", "Print messages"),

            /**
             * Do not show elements whose scope is UNKNOWN. This may be the case of
             * root packages that do not contain any analyzed or referenced
             * elements.
             */
            NO_UNKNOWN("no-unknown", "Do not show elements whose scope is UNKNOWN."),

            /**
             * Do not show elements whose scope is EXTERNAL. Dependencies to these
             * elements are not shown.
             */
            NO_EXTERNAL("no-external", "Do not show elements whose scope is EXTERNAL."),

            /**
             * Create a subgraph for packages and put items in them.
             */
            PACKAGED_ITEMS("packaged-items", "Group items in subgraph corresponding to their packages."),

            NO_CYCLES_HIGHLIGHT("no-cycles-highlight", "Do not highlight elements participating to cycles."),

            SHOW_ONLY_CYCLES("only-cycles", "Show only elements that belong to a cycle (local or global)."),

            SHOW_ONLY_GLOBAL_CYCLES("only-global-cycles", "Show only elements that belong to a global cycle."),

            SHOW_DEPENDENCIES_COUNTS("deps-counts", "Show number of dependencies."),

            EDGE_LABELS_AS_NODES("edge-labels-as-nodes", "Show edge labels as nodes."),

            ADAPT_EDGE_WIDH_TO_COUNT("adapt-edge-width-to-count", "Make edge width depend on counts."),

            CONCENTRATE("concentrate", "Concentrate parallel edges."),

            COMPACT("compact", "Compact items in packages."),

            MULTI_THREAD("multi-thread", "Use multiple threads."),

            FORCE_IMAGE_CREATION("force-images-creation", "Force image creation, even if they already exist."),

            INVOKE_IF_NEWER("invoke-if-newer", "Invoke engine if target files don't exist or are older than inout file."),

            MULTI("multi", "Images are used in multiple files. Impact on URLs."),

            ROOT_GROUPS_CLOSURE("root-groups-closure", "Show dependencies and transitive closure of root groups."),

            PACKAGES_CLOSURE("packages-closure", "Show dependencies and transitive closure of packages.")

            // TRANSITIVE_REDUCTION("transitive-reduction",
            // "Apply a transitive reduction on dependencies.")
            ;

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        public final File getEffectiveImagesDir() {
            if (imagesDir == null || imagesDir.length() == 0) {
                return new File(output.getPath(), "images");
            } else {
                return imagesDir;
            }
        }

        public GvFormat[] getFormats() {
            final GvFormat[] result = new GvFormat[formats.size()];
            formats.toArray(result);
            return result;
        }

        public boolean isEnabled(Target target) {
            return targets.isEnabled(target);
        }

        public void setEnabled(Target target,
                               boolean enabled) {
            targets.setEnabled(target, enabled);
        }

        public boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }

        public void setEnabled(Feature feature,
                               boolean enabled) {
            features.setEnabled(feature, enabled);
        }

    }

    private static class ImageBucket {
        public List<Decoration> decorations;
        public Style style;
        public String back;
        public String ext;

        public ImageBucket() {
            super();
        }
    }

    private final Map<String, ImageBucket> images = new ConcurrentHashMap<>();

    private DepsToGv(MainArgs margs) {
        this.margs = margs;
    }

    private void copyFiles() throws Exception {
        LOGGER.info("copyFiles()");
        Files.mkdir(margs.getEffectiveImagesDir().getPath());
        final String[] images = Resources.getResourceListing("cdc/deps/images");
        for (final String image : images) {
            Resources.copy("cdc/deps/images/" + image, margs.getEffectiveImagesDir());
        }
    }

    private void prepareContext() throws IOException {
        if (margs.styles != null) {
            LOGGER.info("Load styles " + margs.styles);
            final Styles.Loader loader = new Styles.Loader(FailureReaction.WARN);
            loader.loadXml(margs.styles);
        }

        LOGGER.info("Load analysis " + margs.input);
        final DAnalysisXmlLoader loader = new DAnalysisXmlLoader(analysis, FailureReaction.WARN);
        loader.loadXml(margs.input);

        if (!margs.removalPatterns.isEmpty()) {
            LOGGER.info("Remove matching elements");
            analysis.removeElements(margs.removalPatterns);
        }

        if (!margs.collapsePatterns.isEmpty()) {
            LOGGER.info("Collapse matching elements");
            analysis.collapseElements(margs.collapsePatterns);
        }

        LOGGER.info("Create package derived dependencies");
        analysis.createPackageDerivedDependencies();

        LOGGER.info("Create root groups derived dependencies");
        analysis.createRootGroupsDerivedDependencies();

        // Calculate max counts of dependencies
        maxCount = Math.max(1, analysis.getMaxDependenciesCount());

        if (!margs.isEnabled(MainArgs.Feature.NO_CYCLES_HIGHLIGHT)) {
            cycles = new DAnalysisCycles(analysis, DAnalysisCycles.Feature.ITEMS, DAnalysisCycles.Feature.PACKAGES);
        }
    }

    protected void execute(List<Callable<Void>> callables) {
        if (margs.isEnabled(MainArgs.Feature.MULTI_THREAD)) {
            final List<Future<Void>> futures = new ArrayList<>();
            final ExecutorService executor =
                    Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 16);
            for (final Callable<Void> callable : callables) {
                futures.add(executor.submit(callable));
            }
            for (final Future<Void> future : futures) {
                try {
                    future.get();
                } catch (final InterruptedException | ExecutionException e) {
                    LOGGER.catching(e);
                }
            }
            executor.shutdown();
        } else {
            for (final Callable<Void> callable : callables) {
                try {
                    callable.call();
                } catch (final Exception e) {
                    LOGGER.catching(e);
                }
            }
        }
    }

    private void execute() {
        LOGGER.info("execute()");
        try {
            copyFiles();
        } catch (final Exception e) {
            LOGGER.catching(e);
        }
        try {
            prepareContext();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        LOGGER.info("Generate outputs");
        final List<Callable<Void>> callables = new ArrayList<>();
        if (margs.isEnabled(MainArgs.Target.ALL_ROOT_GROUPS)) {
            LOGGER.info("Generate all root groups deps");
            AllRootGroupsGenerator.addAll(this, callables);
        }
        if (margs.isEnabled(MainArgs.Target.ALL_PACKAGES)) {
            LOGGER.info("Generate all packages deps");
            AllPackagesGenerator.addAll(this, callables);
        }
        if (margs.isEnabled(MainArgs.Target.ALL_ITEMS)) {
            LOGGER.info("Generate all items deps");
            AllItemsGenerator.addAll(this, callables);
        }
        if (margs.isEnabled(MainArgs.Target.PACKAGE_ITEMS)) {
            LOGGER.info("Generate package items deps");
            PackageItemsGenerator.addAll(this, callables);
        }
        if (margs.isEnabled(MainArgs.Target.ITEM_CLOSURE)) {
            LOGGER.info("Generate item closures");
            ItemClosureGenerator.addAll(this, callables);
        }

        if (margs.isEnabled(MainArgs.Target.ROOT_GROUP_IN_OUT)) {
            LOGGER.info("Generate root groups deps and usages");
            ElementGenerator.addAll(this,
                                    DElementKind.GROUP,
                                    DDirection.IN_OUT,
                                    margs.isEnabled(MainArgs.Feature.ROOT_GROUPS_CLOSURE),
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.ROOT_GROUP_OUT)) {
            LOGGER.info("Generate root groups deps");
            ElementGenerator.addAll(this,
                                    DElementKind.GROUP,
                                    DDirection.OUT,
                                    margs.isEnabled(MainArgs.Feature.ROOT_GROUPS_CLOSURE),
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.ROOT_GROUP_IN)) {
            LOGGER.info("Generate root groups usages");
            ElementGenerator.addAll(this,
                                    DElementKind.GROUP,
                                    DDirection.IN,
                                    margs.isEnabled(MainArgs.Feature.ROOT_GROUPS_CLOSURE),
                                    callables);
        }

        if (margs.isEnabled(MainArgs.Target.PACKAGE_IN_OUT)) {
            LOGGER.info("Generate packages deps and usages");
            ElementGenerator.addAll(this,
                                    DElementKind.PACKAGE,
                                    DDirection.IN_OUT,
                                    margs.isEnabled(MainArgs.Feature.PACKAGES_CLOSURE),
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.PACKAGE_OUT)) {
            LOGGER.info("Generate packages deps");
            ElementGenerator.addAll(this,
                                    DElementKind.PACKAGE,
                                    DDirection.OUT,
                                    margs.isEnabled(MainArgs.Feature.PACKAGES_CLOSURE),
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.PACKAGE_IN)) {
            LOGGER.info("Generate packages usages");
            ElementGenerator.addAll(this,
                                    DElementKind.PACKAGE,
                                    DDirection.IN,
                                    margs.isEnabled(MainArgs.Feature.PACKAGES_CLOSURE),
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.ITEM_IN_OUT)) {
            LOGGER.info("Generate items deps and usages");
            ElementGenerator.addAll(this,
                                    DElementKind.ITEM,
                                    DDirection.IN_OUT,
                                    false,
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.ITEM_OUT)) {
            LOGGER.info("Generate items deps");
            ElementGenerator.addAll(this,
                                    DElementKind.ITEM,
                                    DDirection.OUT,
                                    false,
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.ITEM_IN)) {
            LOGGER.info("Generate items usages");
            ElementGenerator.addAll(this,
                                    DElementKind.ITEM,
                                    DDirection.IN,
                                    false,
                                    callables);
        }
        if (margs.isEnabled(MainArgs.Target.PAIR_OF_PACKAGES)) {
            LOGGER.info("Generate packages pairs");
            PairOfPackagesGenerator.addAll(this, callables);
        }
        AbstractGenerator.execute(this, callables);
    }

    public static void execute(MainArgs margs) {
        final DepsToGv instance = new DepsToGv(margs);
        instance.execute();
    }

    private static String getStyleQName(DElement element,
                                        DElementScope scope) {
        switch (element.getKind()) {
        case GROUP:
            return element.getCategory();
        case PACKAGE:
        case ITEM:
            if (element.getCategory() == null) {
                return scope.getLabel();
            } else {
                return element.getCategory() + "." + scope.getLabel();
            }
        default:
            return null;
        }
    }

    /**
     * Returns the style to be used for an element with a given scope.
     *
     * @param element The element
     * @return The style associated top element.
     */
    protected static Style getStyle(DElement element) {
        final String qname = getStyleQName(element, element.getScope());
        return Styles.getInstance().getStyle(qname, null);
    }

    protected static GvColor getColor(Style style,
                                      Style.Mode mode,
                                      Style.Key key,
                                      GvColor def) {
        if (style == null) {
            return def;
        } else {
            final String value = Style.getEffectiveValue(style, mode, key, null, FailureReaction.WARN);
            if (value == null) {
                return def;
            } else {
                return new GvColor(value);
            }
        }
    }

    private static List<Decoration> getDecorations(DElement element,
                                                   Style style,
                                                   Style.Mode mode) {
        final ArrayList<Decoration> decorations = new ArrayList<>();
        for (final String feature : element.getSortedFeatures()) {
            final Decoration decoration = Style.getEffectiveDecoration(style,
                                                                       mode,
                                                                       feature,
                                                                       null,
                                                                       FailureReaction.DEFAULT);
            if (decoration != null) {
                decorations.add(decoration);
            }
        }
        return decorations;
    }

    private synchronized void createImages() {
        LOGGER.trace("Create images");
        for (final Map.Entry<String, ImageBucket> entry : images.entrySet()) {
            final String name = entry.getKey();
            final ImageBucket bucket = entry.getValue();
            final File file = new File(margs.getEffectiveImagesDir(), name);
            LOGGER.trace("   check " + file);
            if (!file.exists() || (margs.isEnabled(MainArgs.Feature.FORCE_IMAGE_CREATION) && !bucket.decorations.isEmpty())) {
                LOGGER.debug("   create: " + file);
                final Icon backIcon = new ImageIcon(bucket.back);
                final List<CompoundIcon.Part> parts = new ArrayList<>();
                for (final Decoration decoration : bucket.decorations) {
                    final File tmp = margs.paths.resolve(decoration.getImage());
                    if (!tmp.exists()) {
                        LOGGER.error("createImages(): can not find decoration: " + tmp + " in: " + margs.paths);
                    }
                    final Icon icon = new ImageIcon(tmp.getPath());
                    parts.add(new CompoundIcon.Part(icon, decoration.getRef0(), decoration.getRef1()));
                }

                final int widthMargin =
                        StringConversion.asInt(Style.getEffectiveValue(bucket.style,
                                                                       Style.Mode.NORMAL,
                                                                       Style.Key.IMAGE_WIDTH_MARGIN,
                                                                       "0",
                                                                       FailureReaction.DEFAULT),
                                               0,
                                               FailureReaction.WARN,
                                               FailureReaction.WARN);
                final int heightMargin =
                        StringConversion.asInt(Style.getEffectiveValue(bucket.style,
                                                                       Style.Mode.NORMAL,
                                                                       Style.Key.IMAGE_HEIGHT_MARGIN,
                                                                       "0",
                                                                       FailureReaction.DEFAULT),
                                               0,
                                               FailureReaction.WARN,
                                               FailureReaction.WARN);

                final CompoundIcon icon = new CompoundIcon(backIcon, widthMargin, heightMargin, parts);

                final BufferedImage image = IconUtils.iconToBufferedImage(icon, BufferedImage.TYPE_INT_ARGB);
                try {
                    Files.mkdir(Files.getDirname(file.getPath()));
                    LOGGER.debug("Create image file: " + file);
                    ImageIO.write(image, bucket.ext, file);
                } catch (final IOException e) {
                    LOGGER.catching(e);
                }
            }
        }
    }

    /**
     * Returns the name of the file that should contain the representation of an element.
     * This representation shall be compliant with passed style.
     * The name is built using element and its features.
     * This name is stored to be used later to create the image.
     *
     * @param element The element
     * @param style The style to use.
     * @param mode The mode.
     * @return The name of the image file that will represent this element.
     */
    protected String getImageFilename(DElement element,
                                      Style style,
                                      Style.Mode mode) {
        final File base = margs.paths.resolve(Style.getEffectiveValue(style,
                                                                      Style.Mode.NORMAL,
                                                                      Style.Key.IMAGE,
                                                                      null,
                                                                      FailureReaction.WARN));

        if (base != null) {
            final List<Decoration> decorations = getDecorations(element, style, mode);
            if (!base.exists()) {
                LOGGER.error("getImageFilename(...) failed to find background image: " + base + " with: " + margs.paths);
            }
            final String back = base.getPath();
            final String ext = Files.getExtension(back);
            final StringBuilder builder = new StringBuilder();
            builder.append(Files.getNakedBasename(back));
            for (final Decoration decoration : decorations) {
                builder.append('-');
                builder.append(decoration.getFeature().toLowerCase());
            }
            builder.append('.');
            builder.append(ext);
            final String name = builder.toString();
            if (!images.containsKey(name)) {
                final ImageBucket bucket = new ImageBucket();
                bucket.decorations = decorations;
                bucket.style = style;
                bucket.ext = ext;
                bucket.back = back;
                images.put(name, bucket);
            }
            final File file = new File(margs.getEffectiveImagesDir(), name);
            return file.getPath();
        } else {
            LOGGER.warn("getImageFilename(" + element + ", " + style + ") FAILED");
            return null;
        }
    }

    protected CyclePosition getCyclePosition(DDependency dependency) {
        if (margs.isEnabled(MainArgs.Feature.NO_CYCLES_HIGHLIGHT)) {
            return CyclePosition.NO_CYCLE;
        } else {
            return cycles.getCyclePosition(dependency);
        }
    }

    protected CyclePosition getCyclePosition(DElement element) {
        if (margs.isEnabled(MainArgs.Feature.NO_CYCLES_HIGHLIGHT)) {
            return CyclePosition.NO_CYCLE;
        } else {
            return cycles.getCyclePosition(element);
        }
    }

    protected void generateGvImages(File gvFile) {
        LOGGER.debug("generateGvImages(" + gvFile + ")");
        createImages();
        final GvToAny.MainArgs oargs = new GvToAny.MainArgs();
        oargs.input = gvFile;
        oargs.outputDir = margs.output;
        oargs.paths = oargs.paths.append(margs.paths);
        oargs.engine = ENGINE;
        oargs.formats.addAll(margs.formats);
        oargs.setEnabled(GvToAny.MainArgs.Feature.INVOKE_IF_NEWER, margs.isEnabled(MainArgs.Feature.INVOKE_IF_NEWER));
        GvToAny.execute(oargs);
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String STYLES = "styles";
        private static final String IMAGES_DIR = "images-dir";
        private static final String REMOVE = "remove";
        private static final String COLLAPSE = "collapse";
        private static final String TOOLTIP_MAX = "tooltip-max-deps";

        MainSupport() {
            super(DepsToGv.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return cdc.deps.Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(PATH)
                                    .hasArgs()
                                    .desc("Path(s) where binaries or data files can be found.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(INPUT)
                                    .hasArg()
                                    .desc("Name of the input file.")
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .hasArg()
                                    .desc("Name of the output basename (default: ./).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CHARSET)
                                    .hasArg()
                                    .desc("Charset for graphviz.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(IMAGES_DIR)
                                    .hasArg()
                                    .desc("Name of the images directory (default: output/images).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(STYLES)
                                    .hasArg()
                                    .desc("Name of the xml file describing styles (default: cdc/deps/cdc-deps-styles.xml).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(REMOVE)
                                    .hasArgs()
                                    .desc("Pattern of the form 'Name' or 'Category:Feature' used to identify elements to remove.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(COLLAPSE)
                                    .hasArgs()
                                    .desc("Pattern of the form 'Name' or 'Category:Feature' used to identify elements to collapse.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(TOOLTIP_MAX)
                                    .hasArg()
                                    .desc("Max number of dependencies to display on tooltips (default: 10).")
                                    .build());

            addNoArgOptions(options, GvFormat.class);
            addNoArgOptions(options, MainArgs.Target.class);
            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            if (cl.hasOption(PATH)) {
                for (final String value : cl.getOptionValues(PATH)) {
                    margs.paths = margs.paths.append(new File(value));
                }
            }
            margs.output = new File(cl.getOptionValue(OUTPUT, "./"));
            if (cl.hasOption(IMAGES_DIR)) {
                margs.imagesDir = new File(cl.getOptionValue(IMAGES_DIR));
            }
            margs.paths = margs.paths.append(margs.getEffectiveImagesDir());
            margs.input = Resources.getResource(cl.getOptionValue(INPUT));
            margs.charset = cl.getOptionValue(CHARSET);
            margs.styles = Resources.getResource(cl.getOptionValue(STYLES, "cdc/deps/cdc-deps-styles.xml"));

            if (cl.hasOption(REMOVE)) {
                for (final String s : cl.getOptionValues(REMOVE)) {
                    margs.removalPatterns.add(s);
                }
            }
            if (cl.hasOption(COLLAPSE)) {
                for (final String s : cl.getOptionValues(COLLAPSE)) {
                    margs.collapsePatterns.add(s);
                }
            }

            margs.tooltipMaxDeps = StringConversion.asInt(cl.getOptionValue(TOOLTIP_MAX, "10"),
                                                          10,
                                                          FailureReaction.WARN,
                                                          FailureReaction.FAIL);

            for (final GvFormat format : GvFormat.values()) {
                if (cl.hasOption(format.getName())) {
                    margs.formats.add(format);
                }
            }
            if (margs.getFormats().length == 0) {
                margs.formats.add(GvFormat.PNG);
            }
            setMask(cl, MainArgs.Target.class, margs.targets::setEnabled);
            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            DepsToGv.execute(margs);
            return null;
        }
    }
}