/**
 * Graphic styles intended to customize generation of GraphViz files.
 *
 * @author Damien Carbonne
 */
package cdc.deps.io.gv.styles;