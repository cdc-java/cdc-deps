package cdc.deps.io.gv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DPackage;
import cdc.deps.graphs.DPairOfPackagesGraph;
import cdc.deps.io.gv.DepsToGv.MainArgs;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.gv.GvWriter;
import cdc.util.files.Files;

/**
 * Generate one diagram per packages pair that have dependencies.
 */
final class PairOfPackagesGenerator extends AbstractGenerator {
    private final DPackage package1;
    private final DPackage package2;

    private PairOfPackagesGenerator(DepsToGv context,
                                    DPackage package1,
                                    DPackage package2,
                                    int index,
                                    int total) {
        super(context, index, total);
        this.package1 = package1;
        this.package2 = package2;
    }

    /**
     * Returns true when an element is accepted for graph generation.
     *
     * @param element The element
     * @return True if element is accepted for graph generation.
     */
    private static boolean isAccepted(DElement element) {
        return element.getKind() == DElementKind.PACKAGE
                && element.getScope().isExternalOrMixed();
    }

    private static List<DPackage> getPackages(DAnalysis analysis) {
        final ArrayList<DPackage> packages = new ArrayList<>();
        for (final DElement element : analysis.getElements()) {
            if (isAccepted(element)) {
                packages.add((DPackage) element);
            }
        }
        Collections.sort(packages);
        return packages;
    }

    /**
     * Returns the number of files that will be generated for analysis.
     *
     * @param packages The packages.
     * @return The number of files to generate for analysis.
     */
    private static int getTotal(List<DPackage> packages) {
        final int count = packages.size();
        return count * (count - 1) / 2;
    }

    static void addAll(DepsToGv context,
                       List<Callable<Void>> callables) {
        final List<DPackage> packages = getPackages(context.analysis);
        final int total = getTotal(packages);
        int index = 0;

        for (int index1 = 0; index1 < packages.size() - 1; index1++) {
            final DPackage package1 = packages.get(index1);
            for (int index2 = index1 + 1; index2 < packages.size(); index2++) {
                index++;
                final DPackage package2 = packages.get(index2);
                final Callable<Void> callable = new PairOfPackagesGenerator(context, package1, package2, index, total);
                callables.add(callable);
            }
        }
    }

    /**
     * Generates all files for a given analysis context.
     *
     * @param context The analysis context.
     */
    static void generateAll(DepsToGv context) {
        final List<Callable<Void>> callables = new ArrayList<>();
        addAll(context, callables);
        execute(context, callables);
    }

    @Override
    protected String getGraphName() {
        final String basename1 = Files.toValidBasename(package1.getName());
        final String basename2 = Files.toValidBasename(package2.getName());
        return "packages-pair-" + basename1 + "_" + basename2;
    }

    @Override
    protected DElement getRefElement() {
        return null;
    }

    @Override
    protected void execute() throws Exception {
        final DPairOfPackagesGraph graph = new DPairOfPackagesGraph(context.analysis, package1, package2);

        final RestrictionSubGraph<DElement, DDependency> filtered = filterCycles(graph);
        DepsToGv.LOGGER.info(format(index, total, getGraphName() + (filtered.isEmpty() ? " (skipped)" : "")));

        if (!filtered.isEmpty()) {
            final File tmp = newTmpFile();
            try (final GvWriter writer = new GvWriter(tmp)) {
                writer.beginGraph(getGraphName(), true, getGraphAttributes());

                printPackagesContent(writer,
                                     filtered.getNodes(),
                                     filtered.getEdges(),
                                     null,
                                     context.margs.isEnabled(MainArgs.Feature.PACKAGED_ITEMS) ? ItemsProcessing.PACKAGED_ITEMS
                                             : ItemsProcessing.NAKED_ITEMS,
                                     DependenciesProcessing.DEPENDENCIES);

                printInterPackagesContent(writer, filtered.getEdges());

                writer.endGraph();
                writer.flush();
                replaceIfNecessary(tmp, getGvFile());
                context.generateGvImages(getGvFile());
            } catch (final IOException e) {
                DepsToGv.LOGGER.catching(e);
            }
        }
    }
}