package cdc.deps.io.gv.styles;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;

/**
 * Definition of a Style.
 * <p>
 * A style has a name and properties.<br>
 * Properties can be:
 * <ul>
 * <li>Strings associated to keys.
 * <li>Decorations associated to features.
 * </ul>
 * Styles are organized as a hierarchy.
 * If a property is not defined on a style, it inherits it from its parents.
 *
 * @author Damien Carbonne
 *
 */
public class Style {
    private static final Logger LOGGER = LogManager.getLogger(Style.class);

    /** names separator in qualified names. */
    private static final String SEPARATOR = ".";
    private static final String DECORATION = "decoration";
    private static final String FEATURE = "feature";
    private static final String KEY = "key";
    private static final String MODE = "mode";

    /** Parent style, possibly null. */
    private final Style parent;
    /** Style local name. */
    private final String name;
    /** Style full name (name . name . ... . name) . */
    private final String qname;

    private static class Properties {
        protected Properties() {
        }

        /** (key, value) pairs of attributes associated to the style. */
        final Map<Key, String> values = new EnumMap<>(Key.class);
        /** Associated decorations. */
        final Map<String, Decoration> decorations = new HashMap<>();
    }

    private final Map<Mode, Properties> properties = new EnumMap<>(Mode.class);

    public enum Mode {
        DEFAULT,
        NORMAL,
        DIMMED;

        public boolean hasParent() {
            return this != Mode.DEFAULT;
        }
    }

    /**
     * Enumeration of attributes that can be associated to a style.
     *
     * @author Damien Carbonne
     *
     */
    public enum Key {
        IMAGE,
        IMAGE_WIDTH_MARGIN,
        IMAGE_HEIGHT_MARGIN,
        BACKGROUND_COLOR,
        FOREGROUND_COLOR;
    }

    public Style(Style parent,
                 String name) {
        this.parent = parent;
        this.name = name;
        if (parent == null) {
            this.qname = name;
        } else {
            this.qname = parent.qname + SEPARATOR + name;
        }
    }

    private Properties getOrCreateProperties(Mode mode) {
        Checks.isNotNull(mode, MODE);

        return properties.computeIfAbsent(mode, k -> new Properties());
    }

    /**
     * @return The parent style (possibly {@code null}).
     */
    public final Style getParent() {
        return parent;
    }

    /**
     * @return the local name of this style.
     */
    public final String getName() {
        return name;
    }

    /**
     * @return the qualified name of this style.
     */
    public final String getQName() {
        return qname;
    }

    /**
     * Sets the value associated to a key for a mode.
     *
     * @param mode The mode.
     * @param key The key.
     * @param value The value.
     * @throws IllegalArgumentException when {@code mode} or {@code key} is {@code null}.
     */
    public final void setLocalValue(Mode mode,
                                    Key key,
                                    String value) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(key, KEY);

        getOrCreateProperties(mode).values.put(key, value);
    }

    /**
     * Returns the value associated to a key for a mode.
     * <p>
     * <b>WARNING:</b> Only a local style and mode search is done.
     *
     * @param mode The mode.
     * @param key The key.
     * @return The value associated to {@code key} for {@code mode},
     *         {@code null} if no value is associated to {@code key} for {@code mode}.
     * @throws IllegalArgumentException when {@code mode} or {@code key} is {@code null}.
     */
    public final String getLocalValue(Mode mode,
                                      Key key) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(key, KEY);

        return getOrCreateProperties(mode).values.get(key);
    }

    /**
     * Returns the value associated to a key for a mode.
     * <p>
     * <b>WARNING:</b> Only a local style search is done.
     *
     * @param mode The mode.
     * @param key The key.
     * @return The value associated to {@code key} for {@code mode},
     *         {@code null} if no value is associated to {@code key} for {@code mode} or its parent mode.
     * @throws IllegalArgumentException when {@code mode} or {@code key} is {@code null}.
     */
    public final String getValue(Mode mode,
                                 Key key) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(key, KEY);

        final String local = getLocalValue(mode, key);
        return local == null && mode.hasParent()
                ? getLocalValue(Mode.DEFAULT, key)
                : local;
    }

    /**
     * Returns the value associated to a key, traversing mode and style hierarchy.
     *
     * @param mode The mode.
     * @param key The key.
     * @param def The default value.
     * @param reaction The reaction to adopt if no value is found.
     * @return The value associated to {@code key} or {@code def}.
     * @throws NotFoundException when no value is found and {@code reaction} is FAIL.
     * @throws IllegalArgumentException when {@code mode} or {@code key} is {@code null}.
     */
    public final String getEffectiveValue(Mode mode,
                                          Key key,
                                          String def,
                                          FailureReaction reaction) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(key, KEY);

        if (getValue(mode, key) != null) {
            return getValue(mode, key);
        } else if (getParent() != null) {
            return getParent().getEffectiveValue(mode, key, def, reaction);
        } else {
            return NotFoundException.onError("No value associated to " + key,
                                             LOGGER,
                                             reaction,
                                             def);
        }
    }

    /**
     * Returns the value associated to a key for style, traversing mode and style hierarchy.
     *
     * @param style The optional style.
     * @param mode The mode.
     * @param key The mandatory key.
     * @param def The default value.
     * @param reaction The reaction to adopt if no value is found or {@code style} is {@code null}.
     * @return The value associated to {@code key} or {@code def}.
     * @throws NotFoundException when {@code style} is {@code null}
     *             or no value is found and {@code reaction} is FAIL.
     * @throws IllegalArgumentException when {@code mode} or {@code key} is {@code null}.
     */
    public static String getEffectiveValue(Style style,
                                           Mode mode,
                                           Key key,
                                           String def,
                                           FailureReaction reaction) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(key, KEY);

        if (style == null) {
            return NotFoundException.onError("Null style",
                                             LOGGER,
                                             reaction,
                                             def);
        } else {
            return style.getEffectiveValue(mode, key, def, reaction);
        }
    }

    /**
     * Adds a decoration to this style.
     *
     * @param mode The mode.
     * @param decoration The decoration.
     */
    public final void addLocalDecoration(Mode mode,
                                         Decoration decoration) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(decoration, DECORATION);

        getOrCreateProperties(mode).decorations.put(decoration.getFeature(), decoration);
    }

    /**
     * Returns the decoration associated to a feature and a mode in this style.
     * <p>
     * <b>WARNING:</b> Only a local search is done.
     *
     * @param mode The mode.
     * @param feature The feature.
     * @return The decoration associated to {@code feature} or {@code null}.
     * @throws IllegalArgumentException when {@code mode} or {@code feature} is {@code null}.
     */
    public final Decoration getLocalDecoration(Mode mode,
                                               String feature) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(feature, FEATURE);

        return getOrCreateProperties(mode).decorations.get(feature);
    }

    public final Decoration getDecoration(Mode mode,
                                          String feature) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(feature, FEATURE);

        final Decoration local = getLocalDecoration(mode, feature);
        return local == null && mode.hasParent()
                ? getLocalDecoration(Mode.DEFAULT, feature)
                : local;
    }

    /**
     * Returns the decoration associated to a feature, traversing style hierarchy.
     *
     * @param mode The mode.
     * @param feature The feature.
     * @param def The default value.
     * @param reaction The reaction to adopt if no value is found.
     * @return The decoration associated to {@code feature} or {@code def}.
     * @throws NotFoundException when no decoration is found and {@code reaction} is FAIL.
     * @throws IllegalArgumentException when {@code mode} or {@code feature} is {@code null}.
     */
    public final Decoration getEffectiveDecoration(Mode mode,
                                                   String feature,
                                                   Decoration def,
                                                   FailureReaction reaction) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(feature, FEATURE);

        if (getDecoration(mode, feature) != null) {
            return getDecoration(mode, feature);
        } else if (getParent() != null) {
            return getParent().getEffectiveDecoration(mode, feature, def, reaction);
        } else {
            return NotFoundException.onError("No decoration associated to " + feature, LOGGER, reaction, def);
        }
    }

    /**
     *
     * @param style The optional style.
     * @param mode The mode.
     * @param feature The feature.
     * @param def The default value.
     * @param reaction The reaction to adopt if no value is found.
     * @return The decoration associated to {@code feature} for {@code style} or {@code def}.
     * @throws NotFoundException When {@code style} is {@code null}
     *             or no decoration is found and {@code reaction} is FAIL.
     * @throws IllegalArgumentException When {@code mode} or {@code feature} is {@code null}.
     */
    public static Decoration getEffectiveDecoration(Style style,
                                                    Mode mode,
                                                    String feature,
                                                    Decoration def,
                                                    FailureReaction reaction) {
        Checks.isNotNull(mode, MODE);
        Checks.isNotNull(feature, FEATURE);

        if (style == null) {
            return NotFoundException.onError("Null style",
                                             LOGGER,
                                             reaction,
                                             def);
        } else {
            return style.getEffectiveDecoration(mode, feature, def, reaction);
        }
    }

    @Override
    public String toString() {
        return getQName();
    }
}