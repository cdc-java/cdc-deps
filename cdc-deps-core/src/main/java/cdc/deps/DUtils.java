package cdc.deps;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class DUtils {
    private DUtils() {
    }

    public static Set<DPackage> getPackages(Collection<DItem> types) {
        final HashSet<DPackage> result = new HashSet<>();
        for (final DItem type : types) {
            result.add(type.getPackage());
        }
        return result;
    }
}