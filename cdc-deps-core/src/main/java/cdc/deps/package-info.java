/**
 * Root package dedicated to dependency analysis.
 * <p>
 * The definition and exploitation of an analysis is as neutral as possible.<br>
 * Specific modules can be developed to generate such analysis for different languages, usages, etc.
 *
 * <p>
 * An {@link cdc.deps.DAnalysis Analysis} may contain 3 {@link cdc.deps.DElementKind Kinds} of {@link cdc.deps.DElement Elements}:
 * <ul>
 * <li>{@link cdc.deps.DGroup Groups}. They can contain groups, packages and items.</li>
 * <li>{@link cdc.deps.DPackage Packages}. They can contain packages and items.</li>
 * <li>{@link cdc.deps.DItem Items}. They can contain other items.</li>
 * </ul>
 * Packages and Items are both considered as {@link cdc.deps.DNamespace Namespaces}.<br>
 * They can both belong at the same time to a namespace and a group.<br>
 * A group can only belong to a group.
 * <p>
 * Some restrictions can be added, depending on the context.
 * <p>
 * Each element has a {@link cdc.deps.DElementScope Scope}.
 * <p>
 * An analysis may also contain {@link cdc.deps.DDependency Dependencies} linking its elements.<br>
 * The {@link cdc.deps.DDependencyLevel Level} of a dependency indicates if it
 * is {@link cdc.deps.DDependencyLevel#PRIMITIVE PRIMITIVE} or {@link cdc.deps.DDependencyLevel#DERIVED DERIVED}.
 * <p>
 * A Java mapping could be:
 * <ul>
 * <li>Groups are mapped to jars, class files, folders.</li>
 * <li>Packages are mapped to Java packages. They can not belong to a group.</li>
 * <li>Items are mapped to classes, enums, interfaces, methods, fields, etc. They may belong to a group.</li>
 * </ul>
 *
 * @author Damien Carbonne
 */
package cdc.deps;