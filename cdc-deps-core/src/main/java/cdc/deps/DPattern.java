package cdc.deps;

// TODO add scope
/**
 * Definition of an element pattern.
 * <p>
 * It is based on name, category and features. The
 * pattern should have one of those forms:
 * <ul>
 * <li>"Name" to designate elements which have a given name.
 * <li>"Category:Feature" to designate elements which have a category and a
 * matching feature.</li>
 * <li>"Category:" to designate elements which have a category.</li>
 * <li>":Feature" to designate elements which have a matching feature.</li>
 * </ul>
 * One can filter either with name or category and feature.
 *
 * @author Damien Carbonne
 *
 */
public class DPattern {
    private final String name;
    private final String category;
    private final String feature;

    private static final String EMPTY = "";

    public DPattern(String name,
                    String category,
                    String feature) {
        this.name = name == null ? EMPTY : name;
        this.category = category == null ? EMPTY : category;
        this.feature = feature == null ? EMPTY : feature;
        if (!isEmpty(name) && (!isEmpty(category) || !isEmpty(feature))) {
            throw new IllegalArgumentException("Can not set name and category or feature at the same time.");
        }
    }

    /**
     * Create a pattern form its text representation.
     * <p>
     * ':' is searched.<br>
     * If none is found, then this is a name pattern.<br>
     * If one is found, it is a a category:feature pattern.
     *
     * @param text The text representation.
     */
    public DPattern(String text) {
        if (text == null) {
            name = "";
            category = "";
            feature = "";
        } else {
            final int pos = text.indexOf(':');
            if (pos < 0) {
                name = text;
                category = "";
                feature = "";
            } else {
                name = "";
                category = text.substring(0, pos);
                feature = text.substring(pos + 1);
            }
        }
    }

    private static boolean isEmpty(String s) {
        return EMPTY.equals(s);
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getFeature() {
        return feature;
    }

    public boolean matches(DElement element) {
        boolean success = true;
        if (name.isEmpty()) {
            // Test category and feature
            if (!category.isEmpty()) {
                success = category.equals(element.getCategory());
            }
            if (success && !feature.isEmpty()) {
                success = element.getFeatures().contains(feature);
            }
        } else {
            // Test name
            success = name.equals(element.getName());
        }
        return success;
    }

    @Override
    public String toString() {
        return !name.isEmpty() ? name : category + ":" + feature;
    }
}