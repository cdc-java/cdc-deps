package cdc.deps;

/**
 * Relative position of two elements.
 * <p>
 * This can be used to locate one package relatively to another one, or one item relatively to another one.
 *
 * @author Damien Carbonne
 *
 */
public enum DRelativePosition {
    /** The two elements have the same position. */
    SAME,
    /** The first element is strictly over the second one. */
    OVER,
    /** The first element is strictly under the second one. */
    UNDER,
    /** The two elements are considered as unrelated. */
    UNRELATED,
    /** Defining a relative position is meaningless. */
    MEANINGLESS
}