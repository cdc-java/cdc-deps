package cdc.deps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Package definition. This is a 'logical' artifact.
 *
 * @author Damien Carbonne
 *
 */
public final class DPackage extends DNamespace {
    private static final Logger LOGGER = LogManager.getLogger(DPackage.class);

    DPackage(int id,
             String name,
             DGroup group,
             DPackage parent) {
        super(id, name, group, parent);
    }

    @Override
    public final String getNamePart(DNamePart part) {
        switch (part) {
        case NAME:
            return getName();
        case BASE_NAME:
        case LOCAL_NAME:
            if (getParent() != null) {
                final String head = getParent().getName();
                return getNameTail(getName(), head);
            } else {
                return getName();
            }
        case PATH:
        case LOCAL_PATH:
            if (getParent() != null) {
                return getParent().getName();
            } else {
                return "";
            }
        default:
            return null;
        }
    }

    @Override
    protected void setScope(DElementScope scope) {
        LOGGER.trace("{}.setScope({})", this, scope);
        // Valid transitions:
        // UNKNOWN --> UNKNOWN | EXTERNAL | MIXED | INTERNAL
        // EXTERNAL --> EXTERNAL | MIXED | INTERNAL
        // MIXED --> EXTERNAL | MIXED | INTERNAL
        // INTERNAL --> EXTERNAL | MIXED | INTERNAL
        if (scope == getScope()) {
            // Ignore
        } else if (scope == DElementScope.UNKNOWN) {
            invalidScopeChange(scope);
        } else {
            super.setScope(scope);
        }
    }

    protected void refreshScope() {
        LOGGER.trace("{}.refreshScope()", this);
        final DElementScope scope = mergeItemChildrenScope(this);
        if (scope != getScope()) {
            setScope(scope);
        }
    }

    /**
     * Computes the merged scope of item children, recursively.
     *
     * @param namespace The parent namespace.
     * @return The merged scope of children (direct an indirect) on namespace.
     */
    private DElementScope mergeItemChildrenScope(DNamespace namespace) {
        DElementScope scope = DElementScope.UNKNOWN;
        for (final DNamespace child : namespace.getChildren()) {
            if (child.getKind() == DElementKind.ITEM) {
                scope = DElementScope.merge(scope, child.getScope());
                scope = DElementScope.merge(scope, mergeItemChildrenScope(child));
            }
        }
        return scope;
    }

    @Override
    public final DElementKind getKind() {
        return DElementKind.PACKAGE;
    }

    @Override
    public final DPackage getParent() {
        return (DPackage) super.getParent();
    }

    @Override
    public DPackage getRoot() {
        return (DPackage) super.getRoot();
    }

    @Override
    public final DPackage getPackage() {
        return getParent();
    }

    public final DRelativePosition getPositionRelativelyTo(DPackage other) {
        if (other == this) {
            return DRelativePosition.SAME;
        } else if (this.isAncestorOf(other)) {
            return DRelativePosition.OVER;
        } else if (other.isAncestorOf(this)) {
            return DRelativePosition.UNDER;
        } else {
            return DRelativePosition.UNRELATED;
        }
    }

    public final List<DGroup> getSortedItemGroups() {
        final Set<DItem> items = getDeepContent(DItem.class);
        final Set<DGroup> groups = new HashSet<>();
        for (final DItem item : items) {
            if (item.getOwner() != null) {
                groups.add(item.getOwner());
            }
        }
        final List<DGroup> result = new ArrayList<>();
        result.addAll(groups);
        Collections.sort(result, DElement.NAME_COMPARATOR);
        return result;
    }

    public final List<DGroup> getSortedItemRootGroups() {
        final Set<DItem> items = getDeepContent(DItem.class);
        final Set<DGroup> groups = new HashSet<>();
        for (final DItem item : items) {
            if (item.getRootOwner() != null) {
                groups.add(item.getRootOwner());
            }
        }
        final List<DGroup> result = new ArrayList<>();
        result.addAll(groups);
        Collections.sort(result, DElement.NAME_COMPARATOR);
        return result;
    }

}