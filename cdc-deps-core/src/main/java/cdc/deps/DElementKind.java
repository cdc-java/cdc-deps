package cdc.deps;

/**
 * Enumeration of possible element kinds.
 *
 * @author Damien Carbonne
 */
public enum DElementKind {

    /**
     * The element is a 'physical' artifact.<br>
     * This may be a file, a jar, etc. A group may contain other groups, items
     * or packages. When a package is a pure namespace, it should not be
     * contained in a group.
     */
    GROUP,

    /**
     * The element is a 'logical' artifact used to package other logical
     * artifacts. This may or may not be a pure namespace. A package can contain
     * other packages and items.
     */
    PACKAGE,

    /**
     * The element is a 'logical' artifact. An item can contain other items.
     */
    ITEM;

    public String getName() {
        return this.name().toLowerCase();
    }
}