package cdc.deps.graphs;

import java.util.function.Predicate;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.util.function.IterableUtils;
import cdc.util.lang.UnexpectedValueException;

/**
 * Base class that can be used to construct a graph view of a part of an
 * analysis.
 *
 * @author Damien Carbonne
 *
 */
public class DAnalysisBaseGraph implements GraphAdapter<DElement, DDependency> {
    private final DAnalysis analysis;
    /** Predicate used to check that a node belongs or not to the graph. */
    private Predicate<DElement> isMatchingNode;
    /** Predicate used to check that an edge belongs or not to the graph. */
    private Predicate<DDependency> isMatchingEdge;

    public DAnalysisBaseGraph(DAnalysis analysis) {
        this.analysis = analysis;
    }

    public final void setNodePredicate(Predicate<DElement> isMatchingNode) {
        this.isMatchingNode = isMatchingNode;
    }

    public final void setEdgePredicate(Predicate<DDependency> isMatchingEdge) {
        this.isMatchingEdge = isMatchingEdge;
    }

    @Override
    public final Iterable<DElement> getNodes() {
        return IterableUtils.filterAndConvert(DElement.class, analysis.getElements(), isMatchingNode);
    }

    @Override
    public final boolean containsNode(DElement node) {
        return analysis.getElements().contains(node) && isMatchingNode.test(node);
    }

    @Override
    public final Iterable<? extends DDependency> getEdges() {
        return IterableUtils.filter(analysis.getDependencies(), isMatchingEdge);
    }

    @Override
    public final boolean containsEdge(DDependency edge) {
        return analysis.getDependencies().contains(edge) && isMatchingEdge.test(edge);
    }

    @Override
    public final Iterable<? extends DDependency> getEdges(DElement node,
                                                          EdgeDirection direction) {
        switch (direction) {
        case INGOING:
            return IterableUtils.filter(node.getIngoingDependencies(), isMatchingEdge);
        case OUTGOING:
            return IterableUtils.filter(node.getOutgoingDependencies(), isMatchingEdge);
        default:
            throw new UnexpectedValueException(direction);
        }
    }

    @Override
    public final DElement getTip(DDependency edge,
                                 EdgeTip tip) {
        switch (tip) {
        case SOURCE:
            return edge.getSource();
        case TARGET:
            return edge.getTarget();
        default:
            throw new UnexpectedValueException(tip);
        }
    }
}