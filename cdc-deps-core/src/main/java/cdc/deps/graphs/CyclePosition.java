package cdc.deps.graphs;

/**
 * Possible position of a graph element relatively to cycles.
 *
 * @author Damien Carbonne
 *
 */
public enum CyclePosition {
    /** The element does not belong to any cycle. */
    NO_CYCLE,
    /** The element belongs to a local (inside a package, ...) cycle. */
    LOCAL_CYCLE,
    /** The element belongs to a global (between packages, ...) cycle. */
    GLOBAL_CYCLE
}