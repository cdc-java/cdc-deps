package cdc.deps.graphs;

import java.util.function.Predicate;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.util.lang.IntMasks;
import cdc.util.lang.UnexpectedValueException;

/**
 * Graph representation of an analysis. One can filter elements according to
 * their kind or scope. One can also filter dependencies.
 *
 * @author Damien Carbonne
 *
 */
public class DAnalysisGraph extends DAnalysisBaseGraph {
    /** Mask of features. */
    private int features = 0;

    /**
     * Predicate used to check that a node belongs or not to the graph.
     */
    final Predicate<DElement> isMatchingNode = value -> {
        // Check scope
        if (!hasMatchingScope(value)) {
            return false;
        }

        // Check kind
        switch (value.getKind()) {
        case PACKAGE:
            return isEnabled(Feature.PACKAGES) || isEnabled(Feature.PACKAGES_DEPS);
        case ITEM:
            return isEnabled(Feature.ITEMS) || isEnabled(Feature.ITEMS_DEPS);
        case GROUP:
            return value.isRoot() && (isEnabled(Feature.ROOT_GROUPS) || isEnabled(Feature.ROOT_GROUPS_DEPS));
        default:
            return false;
        }
    };

    /**
     * Predicate used to check that an edge belongs or not to the graph.
     */
    private final Predicate<DDependency> isMatchingEdge = (DDependency value) -> {
        // Make sure source and target are accepted
        if (!isMatchingNode.test(value.getSource())) {
            return false;
        }
        if (!isMatchingNode.test(value.getTarget())) {
            return false;
        }

        // Assume that source and target have the same kind !
        // assert value.getSource().getKind() ==
        // value.getTarget().getKind();
        // if (value.getSource().getKind() != value.getTarget().getKind()) {
        // System.out.println(getContext() + ".isMatchingEdge.apply(" +
        // value + ") Strange dependency");
        // }
        if (value.getSource().getKind() == value.getTarget().getKind()) {
            switch (value.getSource().getKind()) {
            case ITEM:
                return isEnabled(Feature.ITEMS_DEPS);
            case PACKAGE:
                return isEnabled(Feature.PACKAGES_DEPS);
            case GROUP:
                return isEnabled(Feature.ROOT_GROUPS_DEPS);
            default:
                return false;
            }
        } else {
            // No mixed dependency at the moment
            // TODO
            return false;
        }
    };

    public DAnalysisGraph(DAnalysis analysis,
                          Feature... features) {
        super(analysis);
        setNodePredicate(isMatchingNode);
        setEdgePredicate(isMatchingEdge);
        for (final Feature feature : features) {
            setEnabled(feature, true);
        }
    }

    /**
     * Construct an analysis graph with all features enabled.
     *
     * @param analysis The analysis.
     */
    public DAnalysisGraph(DAnalysis analysis) {
        this(analysis, Feature.values());
    }

    boolean hasMatchingScope(DElement element) {
        switch (element.getScope()) {
        case EXTERNAL:
            return isEnabled(Feature.EXTERNAL);
        case INTERNAL:
            return isEnabled(Feature.INTERNAL);
        case UNKNOWN:
            return isEnabled(Feature.UNKNOWN);
        case MIXED:
            return isEnabled(Feature.EXTERNAL) || isEnabled(Feature.INTERNAL);
        default:
            throw new UnexpectedValueException(element.getScope());
        }
    }

    public enum Feature {
        /** Include packages. */
        PACKAGES,
        /** Include items. */
        ITEMS,
        /** Include root groups. */
        ROOT_GROUPS,

        /** Include elements whose scope is internal. */
        INTERNAL,
        /** Include elements whose scope is unknown. */
        UNKNOWN,
        /** Include elements whose scope is external. */
        EXTERNAL,

        // TODO This is not compliant with all types of dependencies
        /** Include root groups dependencies. This implies ROOT_GROUPS. */
        ROOT_GROUPS_DEPS,
        /** Include package dependencies. This implies PACKAGES. */
        PACKAGES_DEPS,
        /** Include item dependencies. This implies ITEMS. */
        ITEMS_DEPS
    }

    public final void disableAllFeatures() {
        this.features = 0;
    }

    public final void enableAllFeatures() {
        for (final Feature feature : Feature.values()) {
            setEnabled(feature, true);
        }
    }

    public final void setEnabled(Feature feature,
                                 boolean enabled) {
        this.features = IntMasks.setEnabled(features, feature, enabled);
    }

    public final boolean isEnabled(Feature feature) {
        return IntMasks.isEnabled(features, feature);
    }
}