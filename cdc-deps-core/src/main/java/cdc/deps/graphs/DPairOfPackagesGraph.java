package cdc.deps.graphs;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DNamespace;
import cdc.deps.DPackage;
import cdc.graphs.impl.ExtensionSubGraph;

/**
 * Graph containing all items dependencies between two packages. May give an empty
 * graph !
 *
 * @author Damien Carbonne
 *
 */
public final class DPairOfPackagesGraph extends ExtensionSubGraph<DElement, DDependency> {
    private final DPackage package1;
    private final DPackage package2;

    public DPairOfPackagesGraph(DAnalysis analysis,
                                DPackage package1,
                                DPackage package2) {
        super(new DAnalysisGraph(analysis));
        final DAnalysisGraph delegate = (DAnalysisGraph) getDelegate();
        delegate.disableAllFeatures();
        delegate.setEnabled(DAnalysisGraph.Feature.INTERNAL, true);
        delegate.setEnabled(DAnalysisGraph.Feature.ITEMS, true);
        delegate.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, true);
        this.package1 = package1;
        this.package2 = package2;
        for (final DDependency dependency : delegate.getEdges()) {
            if (isRelevant(dependency)) {
                addEdge(dependency);
            }
        }
    }

    private static DPackage getPackage(DElement element) {
        if (element instanceof DNamespace) {
            return ((DNamespace) element).getPackage();
        } else {
            return null;
        }
    }

    private boolean isRelevant(DDependency dependency) {
        final DPackage spack = getPackage(dependency.getSource());
        final DPackage tpack = getPackage(dependency.getTarget());
        return (spack == package1 && tpack == package2)
                || (spack == package2 && tpack == package1);
    }

    public DPackage getPackage1() {
        return package1;
    }

    public DPackage getPackage2() {
        return package2;
    }
}