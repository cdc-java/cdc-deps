package cdc.deps.graphs;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.graphs.core.GraphTransitiveClosure;
import cdc.graphs.impl.ExplicitSubGraph;

/**
 * Graph containing an element and its dependencies closure and / or its usages closure (depending on analysis direction(s)).
 * <p>
 * All (direct and indirect) dependency edges are included.
 *
 * @author Damien Carbonne
 *
 */
public final class DElementClosureGraph extends DElementGraph {
    /**
     * Creates a graph focused on an element.
     *
     * @param analysis The analysis.
     * @param ref The reference element.
     * @param direction The direction(s) of dependency analysis.
     * @param external If {@code true}, elements with EXTERNAL scope are included.
     * @param unknown If {@code true}, elements with UNKNOWN scope are included.
     */
    public DElementClosureGraph(DAnalysis analysis,
                                DElement ref,
                                DDirection direction,
                                boolean external,
                                boolean unknown) {
        super(analysis, ref, direction, external, unknown);

        final GraphTransitiveClosure<DElement, DDependency> closureCalculator = new GraphTransitiveClosure<>(delegate);
        final ExplicitSubGraph<DElement, DDependency> closure =
                closureCalculator.computeTransitiveClosure(ref, direction.toEdgeDirection());

        for (final DElement element : closure.getNodes()) {
            addNode(element);
        }
        for (final DDependency dependency : closure.getEdges()) {
            addEdge(dependency);
        }
    }
}