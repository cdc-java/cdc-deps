/**
 * Building of graphs from a dependency analysis.
 *
 * @author Damien Carbonne
 */
package cdc.deps.graphs;