package cdc.deps.graphs;

import cdc.graphs.EdgeDirection;
import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of directions for dependency analysis.
 *
 * @author Damien Carbonne
 *
 */
public enum DDirection {
    /**
     * Ingoing dependencies: elements that depend on (use) the.reference element are included.
     */
    IN,

    /**
     * Ingoing and outgoing dependencies of the reference element are included.
     */
    IN_OUT,

    /**
     * Outgoing dependencies: the elements that the reference element depends on (uses) are included.
     */
    OUT;

    public String getName() {
        switch (this) {
        case IN:
            return "in";
        case IN_OUT:
            return "in-out";
        case OUT:
            return "out";
        default:
            throw new UnexpectedValueException(this);
        }
    }

    public EdgeDirection toEdgeDirection() {
        switch (this) {
        case IN:
            return EdgeDirection.INGOING;
        case OUT:
            return EdgeDirection.OUTGOING;
        case IN_OUT:
            return null;
        default:
            throw new UnexpectedValueException(this);
        }
    }
}