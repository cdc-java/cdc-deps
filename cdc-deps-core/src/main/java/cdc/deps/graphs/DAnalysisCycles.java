package cdc.deps.graphs;

import java.util.HashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DPackage;
import cdc.graphs.EdgeDirection;
import cdc.graphs.core.GraphCycles;
import cdc.graphs.core.GraphTransitiveClosure;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.util.lang.UnexpectedValueException;

/**
 * Class used to compute dependencies cycles.
 *
 * @author Damien Carbonne
 *
 */
public class DAnalysisCycles {
    private static final Logger LOGGER = LogManager.getLogger(DAnalysisCycles.class);

    protected ExplicitSubGraph<DElement, DDependency> cycles;
    protected GraphTransitiveClosure<DElement, DDependency> cyclesClosure;

    public enum Feature {
        /** Include packages. */
        PACKAGES,
        /** Include items. */
        ITEMS,
        /** Include root groups. */
        ROOT_GROUPS
    }

    public DAnalysisCycles(DAnalysis analysis,
                           Feature... features) {
        LOGGER.info("Create analysis graph");
        final DAnalysisGraph graph = new DAnalysisGraph(analysis);
        for (final Feature feature : features) {
            switch (feature) {
            case ITEMS:
                graph.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, true);
                break;
            case PACKAGES:
                graph.setEnabled(DAnalysisGraph.Feature.PACKAGES_DEPS, true);
                break;
            case ROOT_GROUPS:
                graph.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS_DEPS, true);
                break;
            default:
                throw new UnexpectedValueException(feature);
            }
        }

        final GraphCycles<DElement, DDependency> cyclesDetector = new GraphCycles<>(graph);
        LOGGER.info("Compute cycles");
        cycles = cyclesDetector.computeCyclesMembers();
        cyclesClosure = new GraphTransitiveClosure<>(cycles);
        LOGGER.info("Computed cycles");
    }

    public ExplicitSubGraph<DElement, DDependency> getCyclesGraph() {
        return cycles;
    }

    /**
     * Computes the position of a dependency relatively to cycles.
     * <p>
     * When cycle highlighting is disabled, returns NO_CYCLE.
     *
     * @param dependency The dependency.
     * @return The dependency cycle position.
     */
    public CyclePosition getCyclePosition(DDependency dependency) {
        if (cycles.containsEdge(dependency)) {
            if (dependency.belongsToOnePackage()) {
                return CyclePosition.LOCAL_CYCLE;
            } else {
                return CyclePosition.GLOBAL_CYCLE;
            }
        } else {
            return CyclePosition.NO_CYCLE;
        }
    }

    /**
     * Computes the position of an element relatively to cycles.
     * <p>
     * When cycle highlighting is disabled, returns NO_CYCLE.
     *
     * @param element The element.
     * @return The element cycle position.
     */
    public CyclePosition getCyclePosition(DElement element) {
        if (element instanceof DPackage || element instanceof DGroup) {
            // If the package or group is in a cycle, then it is a global cycle
            return cycles.containsNode(element) ? CyclePosition.GLOBAL_CYCLE : CyclePosition.NO_CYCLE;
        } else if (element instanceof DItem) {
            if (cycles.containsNode(element)) {
                // The item is in a cycle : local or global?
                // Compute the closure of the item.
                // If nodes in the closure belong to 2 packages (or more),
                // the cycle is global.
                final ExplicitSubGraph<DElement, DDependency> closure =
                        cyclesClosure.computeTransitiveClosure(element, EdgeDirection.OUTGOING);
                final HashSet<DPackage> packs = new HashSet<>();
                for (final DElement e : closure.getNodes()) {
                    if (e instanceof DItem) {
                        final DItem t = (DItem) e;
                        packs.add(t.getPackage());
                    }
                }
                return packs.size() == 1 ? CyclePosition.LOCAL_CYCLE : CyclePosition.GLOBAL_CYCLE;
            } else {
                return CyclePosition.NO_CYCLE;
            }
        } else {
            return CyclePosition.NO_CYCLE;
        }
    }
}