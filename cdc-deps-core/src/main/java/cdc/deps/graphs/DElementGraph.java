package cdc.deps.graphs;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.graphs.impl.ExtensionSubGraph;
import cdc.util.lang.UnexpectedValueException;

public abstract class DElementGraph extends ExtensionSubGraph<DElement, DDependency> {
    /**
     * Reference element.
     */
    protected final DElement ref;

    /**
     * Analysis direction.
     */
    protected final DDirection direction;

    /**
     * Creates a graph focused on an element.
     *
     * @param analysis The analysis.
     * @param ref The reference element.
     * @param direction The direction(s) of dependency analysis.
     * @param external If {@code true}, elements with EXTERNAL scope are included.
     * @param unknown If {@code true}, elements with UNKNOWN scope are included.
     */
    protected DElementGraph(DAnalysis analysis,
                            DElement ref,
                            DDirection direction,
                            boolean external,
                            boolean unknown) {
        super(new DAnalysisGraph(analysis));
        final DAnalysisGraph delegate = (DAnalysisGraph) getDelegate();
        delegate.disableAllFeatures();
        delegate.setEnabled(DAnalysisGraph.Feature.INTERNAL, true);
        delegate.setEnabled(DAnalysisGraph.Feature.EXTERNAL, external);
        delegate.setEnabled(DAnalysisGraph.Feature.UNKNOWN, unknown);
        switch (ref.getKind()) {
        case ITEM:
            delegate.setEnabled(DAnalysisGraph.Feature.ITEMS, true);
            delegate.setEnabled(DAnalysisGraph.Feature.ITEMS_DEPS, true);
            break;
        case PACKAGE:
            delegate.setEnabled(DAnalysisGraph.Feature.PACKAGES, true);
            delegate.setEnabled(DAnalysisGraph.Feature.PACKAGES_DEPS, true);
            break;
        case GROUP:
            delegate.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS, true);
            delegate.setEnabled(DAnalysisGraph.Feature.ROOT_GROUPS_DEPS, true);
            break;
        default:
            throw new UnexpectedValueException(ref.getKind());
        }
        this.ref = ref;
        this.direction = direction;
    }

    /**
     * @return The reference element of this graph.
     */
    public final DElement getRefElement() {
        return ref;
    }

    /**
     * @return The analysis direction this graph.
     */
    public final DDirection getDirection() {
        return direction;
    }
}