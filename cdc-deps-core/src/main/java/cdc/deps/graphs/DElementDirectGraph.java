package cdc.deps.graphs;

import cdc.deps.DAnalysis;
import cdc.deps.DDependency;
import cdc.deps.DElement;
import cdc.util.lang.UnexpectedValueException;

/**
 * Graph containing an element and its dependencies and / or its usages (depending on analysis direction(s)).
 * <p>
 * Only direct dependency edges are included.
 *
 * @author Damien Carbonne
 *
 */
public final class DElementDirectGraph extends DElementGraph {

    /**
     * Creates a graph focused on an element.
     *
     * @param analysis The analysis.
     * @param ref The reference element.
     * @param direction The direction(s) of dependency analysis.
     * @param external If {@code true}, elements with EXTERNAL scope are included.
     * @param unknown If {@code true}, elements with UNKNOWN scope are included.
     */
    public DElementDirectGraph(DAnalysis analysis,
                               DElement ref,
                               DDirection direction,
                               boolean external,
                               boolean unknown) {
        super(analysis, ref, direction, external, unknown);

        addNode(ref);
        for (final DDependency dependency : delegate.getEdges()) {
            if (isRelevant(dependency)) {
                addEdge(dependency);
            }
        }
    }

    /**
     * Returns {@code true} when a dependency is relevant for this graph.
     * <p>
     * It must be connected to the reference element with the appropriate direction.
     *
     * @param dependency The dependency.
     * @return {@code true} when {@code dependency} is relevant for this graph.
     */
    private boolean isRelevant(DDependency dependency) {
        switch (direction) {
        case IN:
            return ref == dependency.getTarget();
        case IN_OUT:
            return ref == dependency.getSource() || ref == dependency.getTarget();
        case OUT:
            return ref == dependency.getSource();
        default:
            throw new UnexpectedValueException(direction);
        }
    }
}