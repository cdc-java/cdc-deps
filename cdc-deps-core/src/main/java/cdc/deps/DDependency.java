package cdc.deps;

import java.util.Comparator;

import cdc.graphs.EdgeTip;

/**
 * Class used to declare a dependency between 2 elements.
 *
 * @author Damien Carbonne
 *
 */
public class DDependency implements Comparable<DDependency> {
    private final DElement source;
    private final DElement target;
    private int primitive = 0;
    private int derived = 0;

    public static final Comparator<DDependency> SOURCE_ID_COMPARATOR = (o1,
                                                                        o2) -> o1.getSource().getId() - o2.getSource().getId();

    public static final Comparator<DDependency> TARGET_ID_COMPARATOR = (o1,
                                                                        o2) -> o1.getTarget().getId() - o2.getTarget().getId();

    public static final Comparator<DDependency> SOURCE_NAME_COMPARATOR = (o1,
                                                                          o2) -> o1.getSource()
                                                                                   .getName()
                                                                                   .compareTo(o2.getSource().getName());

    public static final Comparator<DDependency> TARGET_NAME_COMPARATOR = (o1,
                                                                          o2) -> o1.getTarget()
                                                                                   .getName()
                                                                                   .compareTo(o2.getTarget().getName());

    /**
     * Creates a dependency with all counts set to 0.
     *
     * @param source The source element.
     * @param target The target element.
     */
    public DDependency(DElement source,
                       DElement target) {
        this.source = source;
        this.target = target;
    }

    public final void setCount(DDependencyLevel level,
                               int count) {
        if (level == DDependencyLevel.PRIMITIVE) {
            primitive = count;
        } else if (level == DDependencyLevel.DERIVED) {
            derived = count;
        }
    }

    public final void incrementCount(DDependencyLevel level) {
        if (level == DDependencyLevel.PRIMITIVE) {
            primitive++;
        } else if (level == DDependencyLevel.DERIVED) {
            derived++;
        }
    }

    /**
     * Increments the weight of this dependency at a given level.
     *
     * @param level The level.
     * @param delta The increment to add.
     */
    public final void incrementCount(DDependencyLevel level,
                                     int delta) {
        if (level == DDependencyLevel.PRIMITIVE) {
            primitive += delta;
        } else if (level == DDependencyLevel.DERIVED) {
            derived += delta;
        }
    }

    /**
     * Returns the weight of this dependency at a given level.
     * <p>
     * A null level means the total weight.
     *
     * @param level The level.
     * @return The weight of this dependency at level, or total weight.
     */
    public final int getCount(DDependencyLevel level) {
        if (level == DDependencyLevel.PRIMITIVE) {
            return primitive;
        } else if (level == DDependencyLevel.DERIVED) {
            return derived;
        } else {
            return primitive + derived;
        }
    }

    /**
     * @return The sum of primitive and derived weights.
     */
    public final int getTotalCount() {
        return primitive + derived;
    }

    /**
     * @return The source element of this dependency.
     */
    public final DElement getSource() {
        return source;
    }

    /**
     * @return The target element of this dependency.
     */
    public final DElement getTarget() {
        return target;
    }

    /**
     * Returns the source/target element of this dependency.
     *
     * @param tip The tip
     * @return The element on the tip side.
     */
    public final DElement getTip(EdgeTip tip) {
        if (tip == EdgeTip.SOURCE) {
            return source;
        } else if (tip == EdgeTip.TARGET) {
            return target;
        } else {
            throw new IllegalArgumentException("Null tip");
        }
    }

    /**
     * Return whether this dependency belongs to a package, or not.
     * <p>
     * This is true when both extremities of this dependency directly belong to
     * the package.
     *
     * @param p The tested package.
     * @return Whether this dependency belongs to package p, or not.
     */
    public final boolean belongsToPackage(DPackage p) {
        final DElement source = getSource();
        final DElement target = getTarget();
        if (source instanceof DNamespace && target instanceof DNamespace) {
            final DNamespace sourceNamespace = (DNamespace) source;
            final DNamespace targetNamespace = (DNamespace) target;
            return sourceNamespace.getPackage() == p && targetNamespace.getPackage() == p;
        } else {
            return false;
        }
    }

    /**
     * Return whether this dependency belongs to one package, whatever it is, or
     * not.
     *
     * @return Whether this dependency belongs to one package or not.
     */
    public final boolean belongsToOnePackage() {
        final DElement source = getSource();
        final DElement target = getTarget();
        if (source instanceof DNamespace && target instanceof DNamespace) {
            final DNamespace sourceNamespace = (DNamespace) source;
            final DNamespace targetNamespace = (DNamespace) target;
            final DPackage sourcePackage = sourceNamespace.getImmediatePackage();
            final DPackage targetPackage = targetNamespace.getImmediatePackage();

            return sourcePackage == targetPackage;
        } else {
            return false;
        }
    }

    public final DRelativePosition getSourceRelativePosition() {
        final DElement source = getSource();
        final DElement target = getTarget();
        if (source instanceof DNamespace && target instanceof DNamespace) {
            final DNamespace sourceNamespace = (DNamespace) source;
            final DNamespace targetNamespace = (DNamespace) target;
            final DPackage sourcePackage = sourceNamespace.getImmediatePackage();
            final DPackage targetPackage = targetNamespace.getImmediatePackage();

            return sourcePackage.getPositionRelativelyTo(targetPackage);
        } else {
            return DRelativePosition.MEANINGLESS;
        }
    }

    public final DRelativePosition getTargetRelativePosition() {
        final DElement source = getSource();
        final DElement target = getTarget();
        if (source instanceof DNamespace && target instanceof DNamespace) {
            final DNamespace sourceNamespace = (DNamespace) source;
            final DNamespace targetNamespace = (DNamespace) target;
            final DPackage sourcePackage = sourceNamespace.getImmediatePackage();
            final DPackage targetPackage = targetNamespace.getImmediatePackage();

            return targetPackage.getPositionRelativelyTo(sourcePackage);
        } else {
            return DRelativePosition.MEANINGLESS;
        }
    }

    @Override
    public boolean equals(Object other) {
        return this == other;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public int compareTo(DDependency other) {
        final int sourceCmp = getSource().compareTo(other.getSource());
        if (sourceCmp == 0) {
            return getTarget().compareTo(other.getTarget());
        } else {
            return sourceCmp;
        }
    }

    @Override
    public String toString() {
        return "[" + " " + getSource() + " -> " + getTarget() + "]";
    }
}