package cdc.deps;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Set of information related to an analysis.
 *
 * @author Damien Carbonne
 *
 */
public final class DAnalysisInfo {
    public static final class Argument {
        private final String name;
        private final String value;

        public Argument(String name,
                        String value) {
            this.name = name;
            this.value = value;
        }

        public Argument(String name) {
            this(name, null);
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    static final ThreadLocal<SimpleDateFormat> DATE_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    private Date creationDate;
    /** Name of the used analyzer. */
    private String analyzerName;
    /** Arguments used for analysis. */
    private final List<Argument> analysisArguments = new ArrayList<>();

    public Date getCreationDate() {
        return creationDate;
    }

    public static Date toDate(String s) {
        try {
            return DATE_FORMAT.get().parse(s);
        } catch (final ParseException e) {
            return null;
        }
    }

    public static String toString(Date date) {
        return DATE_FORMAT.get().format(date);
    }

    public void setCreationDate(Date date) {
        this.creationDate = date;
    }

    public String getAnalyzerName() {
        return analyzerName;
    }

    public void setAnalyzerName(String name) {
        this.analyzerName = name;
    }

    public List<Argument> getAnalysisArguments() {
        return Collections.unmodifiableList(analysisArguments);
    }

    public void addAnalysisArgument(String name,
                                    String value) {
        analysisArguments.add(new Argument(name, value));
    }

    public void addAnalysisArgument(String name) {
        analysisArguments.add(new Argument(name));
    }
}