package cdc.deps.tools;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.io.office.DAnalysisWorkbookWriter;
import cdc.deps.io.xml.DAnalysisXmlLoader;
import cdc.deps.io.xml.DAnalysisXmlWriter;
import cdc.deps.tools.DepsFilter.MainArgs.Target;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

/**
 * Class used to filter an analysis and save it to different formats.
 *
 * @author Damien Carbonne
 *
 */
public final class DepsFilter {
    protected static final Logger LOGGER = LogManager.getLogger(DepsFilter.class);
    private final MainArgs margs;
    private final DAnalysis analysis = new DAnalysis();

    public static class MainArgs {
        /** Name of the input xml dependency file. */
        public File input;
        /** Name of the output basename where generated files are created. */
        public String output;

        public final Set<String> removalPatterns = new HashSet<>();
        public final Set<String> collapsePatterns = new HashSet<>();

        protected final FeatureMask<Target> targets = new FeatureMask<>();
        protected final FeatureMask<Feature> features = new FeatureMask<>();

        /**
         * Enumeration of possible target diagrams.
         */
        public enum Target implements OptionEnum {
            XML("xml", "Save file as XML (default)."),
            CSV("csv", "Save file as CSV."),
            XLSX("xlsx", "Save file as XLSX."),
            ODS("ods", "Save file as ODS.");

            private final String name;
            private final String description;

            private Target(String name,
                           String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /**
         * Enumeration of possible boolean options.
         */
        public enum Feature implements OptionEnum {
            VERBOSE("verbose", "Print messages");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        public void setEnabled(Target target,
                               boolean enabled) {
            targets.setEnabled(target, enabled);
        }

        public boolean isEnabled(Target target) {
            return targets.isEnabled(target);
        }

        public void setEnabled(Feature feature,
                               boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }
    }

    public DepsFilter(MainArgs margs) {
        this.margs = margs;
    }

    public void execute() throws IOException {
        LOGGER.info("Load analysis " + margs.input);
        final DAnalysisXmlLoader loader = new DAnalysisXmlLoader(analysis, FailureReaction.WARN);
        loader.loadXml(margs.input);

        if (!margs.removalPatterns.isEmpty()) {
            LOGGER.info("Remove matching elements");
            analysis.removeElements(margs.removalPatterns);
        }

        if (!margs.collapsePatterns.isEmpty()) {
            LOGGER.info("Collapse matching elements");
            analysis.collapseElements(margs.collapsePatterns);
        }

        for (final MainArgs.Target target : MainArgs.Target.values()) {
            if (margs.isEnabled(target)) {
                final File file = new File(margs.output + "." + target.name().toLowerCase());
                LOGGER.info("Save filtered analysis to " + file);
                final Chronometer chrono = new Chronometer();
                chrono.start();
                if (target == Target.XML) {
                    DAnalysisXmlWriter.write(analysis, file);
                } else {
                    DAnalysisWorkbookWriter.write(analysis, file);
                }
                chrono.suspend();
                LOGGER.info("Done (" + chrono.getElapsedSeconds() + "s)");
            }
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final DepsFilter main = new DepsFilter(margs);
        main.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String REMOVE = "remove";
        private static final String COLLAPSE = "collapse";

        public MainSupport() {
            super(DepsFilter.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return cdc.deps.Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(INPUT)
                                    .hasArg()
                                    .desc("Name of the input file.")
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .hasArg()
                                    .desc("Name of the output basename.")
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(REMOVE)
                                    .hasArgs()
                                    .desc("Pattern of the form 'Name' or 'Category:Feature' used to identify elements to remove.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(COLLAPSE)
                                    .hasArgs()
                                    .desc("Pattern of the form 'Name' or 'Category:Feature' used to identify elements to collapse.")
                                    .build());

            addNoArgOptions(options, MainArgs.Target.class);
            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.output = cl.getOptionValue(OUTPUT);
            margs.input = getValueAsFile(cl, INPUT, IS_FILE);

            if (cl.hasOption(REMOVE)) {
                for (final String s : cl.getOptionValues(REMOVE)) {
                    margs.removalPatterns.add(s);
                }
            }
            if (cl.hasOption(COLLAPSE)) {
                for (final String s : cl.getOptionValues(COLLAPSE)) {
                    margs.collapsePatterns.add(s);
                }
            }

            setMask(cl, MainArgs.Target.class, margs.targets::setEnabled);
            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            if (margs.targets.isEmpty()) {
                margs.targets.add(MainArgs.Target.XML);
            }

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            DepsFilter.execute(margs);
            return null;
        }
    }
}