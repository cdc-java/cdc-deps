package cdc.deps.tools;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.AbstractDepsToHtml.MainArgs;
import cdc.deps.io.html.writer.HtmlWriterGenerator;
import cdc.deps.io.html.xslt.HtmlXsltGenerator;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.MainResult;
import cdc.util.files.Files;
import cdc.util.files.Resources;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.UnexpectedValueException;
import cdc.util.strings.StringConversion;

/**
 * Converts a dependency XML file to HTML.
 * <p>
 * The HTML can easily be navigated.
 *
 * @author Damien Carbonne
 *
 */
public final class DepsToHtml {
    protected static final Logger LOGGER = LogManager.getLogger(DepsToHtml.class);
    private final MainArgs margs;

    public DepsToHtml(MainArgs margs) {
        this.margs = margs;
    }

    public void execute() throws IOException {
        LOGGER.info("execute()");

        final AbstractDepsToHtml generator;
        switch (margs.engine) {
        case HTML_WRITER:
            generator = new HtmlWriterGenerator(margs);
            break;
        case XSLT:
            generator = new HtmlXsltGenerator(margs);
            break;
        default:
            throw new UnexpectedValueException(margs.engine);
        }
        generator.generate();
    }

    public static void execute(MainArgs margs) throws IOException {
        System.setProperty("javax.xml.transform.TransformerFactory",
                           "net.sf.saxon.TransformerFactoryImpl");
        final DepsToHtml instance = new DepsToHtml(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String STYLES = "styles";
        private static final String TOOLTIP_MAX = "tooltip-max-deps";
        private static final String TITLE = "title";
        private static final String ENGINE = "engine";

        public MainSupport() {
            super(DepsToHtml.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return cdc.deps.Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder("i")
                                    .longOpt(INPUT)
                                    .hasArg()
                                    .argName("URL")
                                    .desc("Name of the XML input file.")
                                    .required()
                                    .build());

            options.addOption(Option.builder("o")
                                    .longOpt(OUTPUT)
                                    .hasArg()
                                    .argName("DIR")
                                    .desc("Name of the output DIR where files are generated.")
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(PATH)
                                    .hasArgs()
                                    .desc("Path(s) where binaries or data files can be found.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CHARSET)
                                    .hasArg()
                                    .desc("Charset for graphviz.")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(STYLES)
                                    .hasArg()
                                    .desc("Name of the xml file describing styles (default: cdc/deps/cdc-deps-styles.xml).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(TOOLTIP_MAX)
                                    .hasArg()
                                    .desc("Max number of dependencies to display on tooltips (default: 10).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(TITLE)
                                    .hasArg()
                                    .desc("Document title (default: output file basename).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ENGINE)
                                    .hasArg()
                                    .desc("Engine used to generate HTML files. (default: " + MainArgs.Engine.XSLT + ").\n"
                                            + "One of: " + MainArgs.Engine.values())
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            margs.input = Resources.getResource(cl.getOptionValue(INPUT));
            if (margs.input == null) {
                throw new ParseException("Invalid input: " + margs.input);
            }

            margs.outputDir = new File(cl.getOptionValue(OUTPUT));
            if (margs.outputDir.exists() && !margs.outputDir.isDirectory()) {
                throw new ParseException("Invalid output: " + margs.outputDir);
            }

            if (cl.hasOption(PATH)) {
                for (final String value : cl.getOptionValues(PATH)) {
                    margs.paths = margs.paths.append(new File(value));
                }
            }
            margs.charset = cl.getOptionValue(CHARSET);
            margs.styles = Resources.getResource(cl.getOptionValue(STYLES, "cdc/deps/cdc-deps-styles.xml"));

            margs.tooltipMaxDeps = StringConversion.asInt(cl.getOptionValue(TOOLTIP_MAX, "10"),
                                                          10,
                                                          FailureReaction.WARN,
                                                          FailureReaction.FAIL);
            margs.title = cl.getOptionValue(TITLE, Files.getNakedBasename(margs.input.getFile()));

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            margs.engine = getValueAsEnum(cl, ENGINE, MainArgs.Engine.class, MainArgs.Engine.XSLT);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            DepsToHtml.execute(margs);
            return null;
        }
    }
}