/**
 * Tools dedicated to analyzes.
 * <ul>
 * <li>{@link cdc.deps.tools.DepsFilter DepsFilter} used to filter and transform an analysis.</li>
 * <li>{@link cdc.deps.tools.DepsToHtml DepsToHtml} used to produce an Html report.</li>
 * </ul>
 *
 * @author Damien Carbonne
 */
package cdc.deps.tools;