package cdc.deps;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

/**
 * Definition of a dependency analysis. An analysis is defined by a set of
 * elements and dependencies between these elements. 3 kinds of elements are
 * supported. Each element must have a unique name.
 *
 * An analysis is produced by an analyzer.
 *
 * @see DElementKind
 * @author Damien Carbonne
 *
 */
public class DAnalysis {
    private static final Logger LOGGER = LogManager.getLogger(DAnalysis.class);
    private final DAnalysisInfo info = new DAnalysisInfo();
    private int id = 0;
    /** Set of elements contained in the analysis. */
    private final Set<DElement> elements = new HashSet<>();
    /** Set of root elements. */
    private final Set<DElement> roots = new HashSet<>();
    /** Map from element names to elements. */
    private final Map<String, DElement> nameToElements = new HashMap<>();
    /** Set of dependencies. */
    private final Set<DDependency> dependencies = new HashSet<>();

    /** Map from names to aliases. */
    private final Map<String, String> namesToAlias = new HashMap<>();

    /**
     * Set of used aliases.
     * Used to check that they are not used twice.
     */
    private final Set<String> aliases = new HashSet<>();

    public DAnalysis() {
        super();
    }

    public void clear() {
        id = 0;
        elements.clear();
        roots.clear();
        nameToElements.clear();
        namesToAlias.clear();
        aliases.clear();
    }

    private void add(DElement element) {
        elements.add(element);
        nameToElements.put(element.getName(), element);
        if (element.isRoot()) {
            roots.add(element);
        }
    }

    /**
     * Associates an alias to a name.
     * <p>
     * If name has already an alias or if alias is already used, an exception is raised.
     *
     * @param name The name.
     * @param alias The alias.
     */
    public final void addAlias(String name,
                               String alias) {
        if (name == null || alias == null) {
            throw new IllegalArgumentException("Null name or alias.");
        }
        if (aliases.contains(alias)) {
            throw new IllegalArgumentException("An alias must be used at most once.");
        }
        namesToAlias.put(name, alias);
        aliases.add(alias);
    }

    /**
     * @return A set of names associated to an alias.
     */
    public final Set<String> getAliasedNames() {
        return namesToAlias.keySet();
    }

    /**
     * Returns the alias associated to a name or null.
     *
     * @param name The name.
     * @return The alias associated to name or null.
     */
    public final String getAlias(String name) {
        return namesToAlias.get(name);
    }

    /**
     * Returns the alias associated to a name or the name itself.
     *
     * @param name The name.
     * @return The alias associated to name or name if name has no associated alias.
     */
    public final String getAliasOrName(String name) {
        final String alias = getAlias(name);
        if (alias == null) {
            return name;
        } else {
            return alias;
        }
    }

    public final DAnalysisInfo getInfo() {
        return info;
    }

    /**
     * @return the set of elements.
     */
    public final Set<DElement> getElements() {
        return elements;
    }

    public final <E extends DElement> List<E> getElements(Class<E> cls) {
        return elements.stream()
                       .filter(cls::isInstance)
                       .map(cls::cast)
                       .toList();
    }

    public final <E extends DElement> List<E> getElements(Class<E> cls,
                                                          Comparator<? super E> comparator) {
        return elements.stream()
                       .filter(cls::isInstance)
                       .map(cls::cast)
                       .sorted(comparator)
                       .toList();
    }

    /**
     * @return the set of dependencies.
     */
    public final Set<DDependency> getDependencies() {
        return dependencies;
    }

    /**
     * @return the set of elements names.
     */
    public final Set<String> getElementNames() {
        return nameToElements.keySet();
    }

    /**
     * Returns the element that has a given name, or null.
     *
     * @param name The name of the searched element.
     * @return the element whose name is name or null.
     */
    public final DElement getElement(String name) {
        return nameToElements.get(name);
    }

    /**
     * Returns all roots of this analysis.
     * <p>
     * A root is a namespace that has no parent namespace or a group that has no
     * parent group.
     *
     * @return All roots of this analysis.
     */
    public final Set<DElement> getRoots() {
        return roots;
    }

    public final <E extends DElement> List<E> getSortedRoots(Class<E> cls) {
        return getRoots().stream()
                         .filter(cls::isInstance)
                         .map(cls::cast)
                         .sorted(DElement.NAME_COMPARATOR)
                         .toList();
    }

    /**
     * Returns the group that has a given name.
     * <p>
     * If no element with that name exists, returns null. If an element with that
     * name exists and is not a group, an exception will be raised.
     *
     * @param name The name.
     * @return The group that is named {@code name}.
     */
    public final DGroup getGroup(String name) {
        return (DGroup) getElement(name);
    }

    /**
     * Returns the package that has a given name.
     * <p>
     * If no element with that name exists, returns null. If an element with that
     * name exists and is not a package, an exception will be raised.
     *
     * @param name The name.
     * @return The package that is named {@code name}.
     */
    public final DPackage getPackage(String name) {
        return (DPackage) getElement(name);
    }

    /**
     * Returns the item that has a given name.
     * <p>
     * If no element with that name exists, returns null. If an element with that
     * name exists and is not an item, an exception will be raised.
     *
     * @param name The name.
     * @return The item that is named {@code name}.
     */
    public final DItem getItem(String name) {
        return (DItem) getElement(name);
    }

    private static String toString(String name,
                                   DGroup parent) {
        return "createGroup(" + name + ", " + parent + ")";
    }

    /**
     * Creates a group.
     * <p>
     * If a group with the same name already exists, or parent is invalid, an exception is raised.
     *
     * @param name Name of the group.
     * @param parent Parent group. <em>May be null.</em>
     * @return The created group.
     */
    public DGroup createGroup(String name,
                              DGroup parent) {
        LOGGER.debug(toString(name, parent));
        if (nameToElements.containsKey(name)) {
            throw new IllegalArgumentException(toString(name, parent) + " duplicate group name");
        } else if (parent != null && !elements.contains(parent)) {
            throw new IllegalArgumentException(toString(name, parent) + " unknown parent group");
        } else {
            id++;
            final DGroup result = new DGroup(id, name, parent);
            add(result);
            return result;
        }
    }

    public DGroup findOrCreateGroup(String name,
                                    DGroup parent) {
        DGroup result = getGroup(name);
        if (result == null) {
            result = createGroup(name, parent);
            if (parent != null) {
                result.setScope(parent.getScope());
            }
        } else if (parent != result.getParent()) {
            LOGGER.error("Can not change parent of {}", result);
            throw new IllegalArgumentException("findOrCreateGroup(" + name + ", " + parent
                    + ") can not change parent of existing group.");
        }
        return result;
    }

    private static String toString(String name,
                                   DGroup group,
                                   DPackage parent) {
        return "createPackage(" + name + ", " + group + ", " + parent + ")";
    }

    /**
     * Creates a package.
     * <p>
     * If a package with same name already exists, an exception is raised.
     *
     * @param name Name of the package.
     * @param group Parent group. <em>May be null.</em>
     * @param parent Parent package. <em>May be null.</em>
     * @return The created package.
     */
    public DPackage createPackage(String name,
                                  DGroup group,
                                  DPackage parent) {
        LOGGER.debug(toString(name, group, parent));
        if (nameToElements.containsKey(name)) {
            throw new IllegalArgumentException(toString(name, group, parent) + " duplicate package name");
        } else if (parent != null && !elements.contains(parent)) {
            throw new IllegalArgumentException(toString(name, group, parent) + " unknown parent package");
        } else {
            id++;
            final DPackage result = new DPackage(id, name, group, parent);
            add(result);
            return result;
        }
    }

    /**
     * Returns an existing or newly created package.
     * <p>
     * If the package already exists, set its group if possible.
     *
     * @param name The name of the searched/created package.
     * @param group Parent group (for creation). <em>May be null.</em>
     * @param parent The parent package (for creation).<em>May be null.</em>
     * @return The existing package or a newly created one.
     */
    public final DPackage findOrCreatePackage(String name,
                                              DGroup group,
                                              DPackage parent) {
        DPackage result = getPackage(name);
        if (result == null) {
            result = createPackage(name, group, parent);
        } else {
            if (parent != result.getParent()) {
                LOGGER.error("Can not change parent of {}", result);
            }
            if (group != null && group != result.getOwner()) {
                if (result.getOwner() == null) {
                    result.setGroup(group);
                } else {
                    LOGGER.error("Can not change group of {}", result);
                }
            }
        }
        return result;
    }

    public static String toString(String name,
                                  DGroup group,
                                  DNamespace parent) {
        return "createItem(" + parent + ", " + name + ", " + group + ")";
    }

    /**
     * Creates an item.
     * <p>
     * If an item with an identical name already exists, an exception is raised.
     * If group or parent is invalid, an exception is raised.
     *
     * @param name Name of the item.
     * @param group Parent group. <em>May be null.</em>
     * @param parent Parent namespace. <em>May be null.</em>
     * @return The created item.
     */
    public DItem createItem(String name,
                            DGroup group,
                            DNamespace parent) {
        LOGGER.debug(toString(name, group, parent));
        if (nameToElements.containsKey(name)) {
            throw new IllegalArgumentException(toString(name, group, parent) + " duplicate item name");
        } else if (parent != null && !elements.contains(parent)) {
            throw new IllegalArgumentException(toString(name, group, parent) + ") unknown parent namespace");
        } else if (group != null && !elements.contains(group)) {
            throw new IllegalArgumentException(toString(name, group, parent) + " unknown group");
        } else {
            id++;
            final DItem result = new DItem(id, name, group, parent);
            add(result);
            return result;
        }
    }

    /**
     * Returns an existing or newly created item.<br>
     * If the item already exists, set its group.
     *
     * @param parent The parent namespace (for creation).<em>May be null.</em>
     * @param name The name of the searched/created item.
     * @param group Parent group (for creation). <em>May be null.</em>
     * @return The existing item or a newly created one.
     */
    public final DItem findOrCreateItem(DNamespace parent,
                                        String name,
                                        DGroup group) {
        LOGGER.debug("findOrCreateItem({}, {}, {})", parent, name, group);
        DItem result = getItem(name);
        if (result == null) {
            result = createItem(name, group, parent);
        } else if (group != null && result.getOwner() == null) {
            result.setGroup(group);
        }
        return result;
    }

    /**
     * Removes an element from this analysis.
     *
     * @param element The element.
     */
    public void remove(DElement element) {
        removeChildren(element);
        removeDependencies(element);

        switch (element.getKind()) {
        case PACKAGE, ITEM:
            final DNamespace namespace = (DNamespace) element;
            removeFromParentNamespace(namespace);
            break;

        case GROUP:
        default:
            // Ignore
            break;
        }
        removeFromOwningGroup(element);
        removeFromAnalysis(element);
    }

    /**
     * Removes all references of this analysis to an element.
     *
     * @param element The element.
     */
    private void removeFromAnalysis(DElement element) {
        {
            final boolean removed = elements.remove(element);
            assert removed;
        }
        {
            final boolean removed = nameToElements.remove(element.getName()) != null;
            assert removed;
        }
        if (element.isRoot()) {
            final boolean removed = roots.remove(element);
            assert removed;
        }
    }

    /**
     * Removes reference to an element in its owning group.
     *
     * @param element The element.
     */
    private static void removeFromOwningGroup(DElement element) {
        final DGroup owner = element.getOwner();
        if (owner != null) {
            final boolean removed = owner.children.remove(element);
            assert removed;
        }
    }

    /**
     * Removes reference to a namespace in its parent namespace.
     *
     * @param child The namespace.
     */
    private static void removeFromParentNamespace(DNamespace child) {
        final DNamespace parent = child.getParent();
        if (parent != null) {
            final boolean removed = parent.children.remove(child);
            assert removed;
        }
    }

    /**
     * Recursively removes all children of an element. When the element is a
     * group, only its children groups are removed.
     *
     * @param element The element.
     */
    private void removeChildren(DElement element) {
        final ArrayList<DElement> children = new ArrayList<>();
        children.addAll(element.getChildren());
        for (final DElement child : children) {
            if (element.getKind() != DElementKind.GROUP || child.getKind() == DElementKind.GROUP) {
                remove(child);
            }
        }
    }

    /**
     * Removes all dependencies related to an element.
     *
     * @param element The element.
     */
    private void removeDependencies(DElement element) {
        final ArrayList<DDependency> toBeRemoved = new ArrayList<>();
        toBeRemoved.addAll(element.getIngoingDependencies());
        toBeRemoved.addAll(element.getOutgoingDependencies());
        for (final DDependency dependency : toBeRemoved) {
            remove(dependency);
        }
    }

    /**
     * Moves dependencies of an element to its parent, and remove it if it is not
     * a root.
     * <p>
     * Level counts are moved.
     *
     * @param element The element.
     */
    public final void collapse(DElement element) {
        // First collapse children if any
        if (!element.getChildren().isEmpty()) {
            final ArrayList<DElement> children = new ArrayList<>();
            children.addAll(element.getChildren());
            for (final DElement child : children) {
                collapse(child);
            }
        }
        Checks.assertTrue(element.getChildren().isEmpty(), "Implementation error, no children should remain.");

        final DElement parent = element.getParent();

        if (parent != null) {
            // Collapse outgoing dependencies
            for (final DDependency dependency : element.getOutgoingDependencies()) {
                final DElement target = dependency.getTarget();
                final DDependency dep = addDependency(parent, target);
                if (dep != null) {
                    final int primitive = dependency.getCount(DDependencyLevel.PRIMITIVE);
                    final int derived = dependency.getCount(DDependencyLevel.DERIVED);
                    dep.incrementCount(DDependencyLevel.PRIMITIVE, primitive);
                    dep.incrementCount(DDependencyLevel.DERIVED, derived);
                }
            }

            // Collapse ingoing dependencies
            for (final DDependency dependency : element.getIngoingDependencies()) {
                final DElement source = dependency.getSource();
                final DDependency dep = addDependency(source, parent);
                if (dep != null) {
                    final int primitive = dependency.getCount(DDependencyLevel.PRIMITIVE);
                    final int derived = dependency.getCount(DDependencyLevel.DERIVED);
                    dep.incrementCount(DDependencyLevel.PRIMITIVE, primitive);
                    dep.incrementCount(DDependencyLevel.DERIVED, derived);
                }
            }

            // Now remove the element
            remove(element);
        }
    }

    private static boolean hasMatchingAncestor(DElement element,
                                               Predicate<DElement> predicate) {
        final DElement parent = element.getParent();
        if (parent != null) {
            if (predicate.test(parent)) {
                return true;
            } else {
                return hasMatchingAncestor(parent, predicate);
            }
        } else {
            return false;
        }
    }

    /**
     * Collapses all elements that match a predicate. Their dependencies are
     * propagated. It the element is a root, it won't be removed.
     *
     * @param predicate The predicate.
     */
    public final void collapse(Predicate<DElement> predicate) {
        final ArrayList<DElement> candidates = new ArrayList<>();
        for (final DElement element : getElements()) {
            if (predicate.test(element) && !hasMatchingAncestor(element, predicate)) {
                candidates.add(element);
            }
        }

        for (final DElement element : candidates) {
            LOGGER.info("COLLAPSE: {}", element);
            collapse(element);
        }
    }

    /**
     * Removes all elements that match a predicate. Their dependencies are not
     * propagated.
     *
     * @param predicate The predicate.
     */
    public final void remove(Predicate<DElement> predicate) {
        final ArrayList<DElement> candidates = new ArrayList<>();
        for (final DElement element : getElements()) {
            if (predicate.test(element) && !hasMatchingAncestor(element, predicate)) {
                candidates.add(element);
            }
        }

        for (final DElement element : candidates) {
            LOGGER.info("REMOVE: {}", element);
            remove(element);
        }
    }

    private static class PatternMatcher implements Predicate<DElement> {
        private final Set<DPattern> patterns = new HashSet<>();

        public PatternMatcher(Iterable<DPattern> patterns) {
            for (final DPattern pattern : patterns) {
                this.patterns.add(pattern);
            }
        }

        @Override
        public boolean test(DElement value) {
            for (final DPattern pattern : patterns) {
                if (pattern.matches(value)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Collapses all elements that match a pattern among a set of patterns.
     *
     * @param patterns The set of patterns.
     */
    public final void collapseElements(Iterable<String> patterns) {
        final List<DPattern> tmp = new ArrayList<>();
        for (final String s : patterns) {
            tmp.add(new DPattern(s));
        }
        collapse(new PatternMatcher(tmp));
    }

    /**
     * Collapses all elements that match a pattern among a set of patterns.
     *
     * @param patterns The set of patterns.
     */
    public final void collapseElements(String... patterns) {
        final List<DPattern> tmp = new ArrayList<>();
        for (final String s : patterns) {
            tmp.add(new DPattern(s));
        }
        collapse(new PatternMatcher(tmp));
    }

    /**
     * Removes all elements that match a pattern among a set of patterns.
     *
     * @param patterns The set of patterns.
     */
    public final void removeElements(Iterable<String> patterns) {
        final List<DPattern> tmp = new ArrayList<>();
        for (final String s : patterns) {
            tmp.add(new DPattern(s));
        }
        remove(new PatternMatcher(tmp));
    }

    /**
     * Removes all elements that match a pattern among a set of patterns.
     *
     * @param patterns The set of patterns.
     */
    public final void removeElements(String... patterns) {
        final List<DPattern> tmp = new ArrayList<>();
        for (final String s : patterns) {
            tmp.add(new DPattern(s));
        }
        remove(new PatternMatcher(tmp));
    }

    public final DDependency addDependency(DElement source,
                                           DElement target) {
        return addDependency(source, target, null);
    }

    /**
     * If (source, target) pair is not valid, returns null. Otherwise, if no
     * corresponding dependency exists, creates one. Increment the PRIMITIVE
     * count of the created/reused dependency by 1 and return it.
     *
     * @param source The source element.
     * @param target The target element.
     * @return The created or modified dependency, or null.
     */
    public final DDependency addPrimitiveDependency(DElement source,
                                                    DElement target) {
        return addDependency(source, target, DDependencyLevel.PRIMITIVE);
    }

    /**
     * If (source, target) pair is not valid, returns null. Otherwise, if no
     * corresponding dependency exists, creates one. Increment the DERIVED count
     * of the created/reused dependency by 1 and return it.
     *
     * @param source The source element.
     * @param target The target element.
     * @return The created or modified dependency, or null.
     */
    public final DDependency addDerivedDependency(DElement source,
                                                  DElement target) {
        return addDependency(source, target, DDependencyLevel.DERIVED);
    }

    /**
     * If (source, target) pair is valid, creates a dependency between source and
     * target, with count of 1, or increment the count of the existing
     * dependency by 1. Otherwise, returns null.
     *
     * @param source The source element.
     * @param target The target element.
     * @param level The dependency level.
     * @return The (source, target) dependency.
     */
    private final DDependency addDependency(DElement source,
                                            DElement target,
                                            DDependencyLevel level) {
        LOGGER.debug("addDependency({}, {}, {})", source, target, level);
        if (source != null && target != null && source != target) {
            DDependency dep = source.getOutgoingDependency(target);
            if (dep == null) {
                dep = new DDependency(source, target);
                dependencies.add(dep);
                source.outgoings.add(dep);
                target.ingoings.add(dep);
            }
            if (level != null) {
                dep.incrementCount(level);
            }
            return dep;
        } else {
            return null;
        }
    }

    /**
     * Removes a dependency from this analysis. After that, it is no more
     * referenced by this analysis or one of its elements.
     *
     * @param dependency The dependency.
     */
    public final void remove(DDependency dependency) {
        // Remove from analysis
        {
            final boolean removed = dependencies.remove(dependency);
            assert removed;
        }
        // Remove from source
        {
            final boolean removed = dependency.getSource().outgoings.remove(dependency);
            assert removed;
        }
        // Remove from target
        {
            final boolean removed = dependency.getTarget().ingoings.remove(dependency);
            assert removed;
        }
    }

    public void clearDerivedDependencies(DElementKind sourceKind,
                                         DElementKind targetKind) {
        LOGGER.info("clearDerivedDependencies({}, {})", sourceKind, targetKind);
        final Set<DDependency> toBeRemoved = new HashSet<>();
        for (final DDependency dependency : dependencies) {
            if ((sourceKind == null || dependency.getSource().getKind() == sourceKind)
                    && (targetKind == null || dependency.getTarget().getKind() == targetKind)) {
                dependency.setCount(DDependencyLevel.DERIVED, 0);
                if (dependency.getTotalCount() == 0) {
                    toBeRemoved.add(dependency);
                }
            }
        }
        for (final DDependency dependency : toBeRemoved) {
            remove(dependency);
        }
    }

    /**
     * Creates package to package dependencies, deduced from items dependencies.
     * <p>
     * Package to package dependencies are first cleared.
     */
    public void createPackageDerivedDependencies() {
        LOGGER.info("createPackageDerivedDependencies()");
        clearDerivedDependencies(DElementKind.PACKAGE, DElementKind.PACKAGE);

        // Clone list of dependencies to avoid self modification during
        // iteration
        final List<DDependency> deps = new ArrayList<>();
        deps.addAll(dependencies);
        for (final DDependency dependency : deps) {
            final DElement source = dependency.getSource();
            final DElement target = dependency.getTarget();
            if (source.getKind() == DElementKind.ITEM && target.getKind() == DElementKind.ITEM) {
                final DDependency dep =
                        addDerivedDependency(((DItem) source).getPackage(), ((DItem) target).getPackage());
                // dep may be null if
                // - source.getPackage() == target.getPackage()
                // - source.getPackage() == null
                // - target.getPackage() == null
                if (dep != null) {
                    final int total = dependency.getTotalCount();
                    dep.incrementCount(DDependencyLevel.DERIVED, total - 1);
                }
            }
        }
    }

    /**
     * Creates root groups to rot groups dependencies, deduced from item dependencies.
     */
    public void createRootGroupsDerivedDependencies() {
        LOGGER.info("createRootGroupsDerivedDependencies()");
        clearDerivedDependencies(DElementKind.GROUP, DElementKind.GROUP);

        // Clone list of dependencies to avoid self modification during
        // iteration
        final List<DDependency> deps = new ArrayList<>();
        deps.addAll(dependencies);
        for (final DDependency dependency : deps) {
            final DElement source = dependency.getSource();
            final DElement target = dependency.getTarget();
            if (source.getKind() == DElementKind.ITEM && target.getKind() == DElementKind.ITEM) {
                final DDependency dep =
                        addDerivedDependency(source.getRootOwner(), target.getRootOwner());
                // dep may be null if
                // - source.getRootOwner() == target.getRootOwner()
                // - source.getRootOwner() == null
                // - target.getRootOwner() == null
                if (dep != null) {
                    final int total = dependency.getTotalCount();
                    dep.incrementCount(DDependencyLevel.DERIVED, total - 1);
                }
            }
        }
    }

    /**
     * Returns the maximum count value of dependencies, at a given level.
     *
     * @param level The level.
     * @return The maximum count value of dependencies, at a given level.
     */
    public int getMaxDependenciesCount(DDependencyLevel level) {
        int result = 0;
        for (final DDependency dependency : dependencies) {
            result = Math.max(result, dependency.getCount(level));
        }
        return result;
    }

    public int getMaxDependenciesCount() {
        int result = 0;
        for (final DDependency dependency : dependencies) {
            result = Math.max(result, dependency.getTotalCount());
        }
        return result;
    }

    public void checkScopes() {
        for (final DElement element : getElements()) {
            if (element.getScope() == DElementScope.UNKNOWN && element.getKind() != DElementKind.PACKAGE) {
                LOGGER.warn("Invalid scope for: {}", element);
            }
        }
    }

    public void checkCategory() {
        for (final DElement element : getElements()) {
            if (element.getCategory() == null) {
                LOGGER.warn("Invalid category for: {}", element);
            }
        }
    }
}