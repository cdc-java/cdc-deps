package cdc.deps;

/**
 * Scope of an element relatively to an analysis.<br>
 * <em>Order of declarations (UNKNOWN, EXTERNAL, INTERNAL) matters.</em>
 *
 * @author Damien Carbonne
 *
 */
public enum DElementScope {
    /** The scope of the element is unknown. */
    UNKNOWN("Unknown"),

    /**
     * The element was only referenced during the analysis. It does not belong
     * to analyzed elements.
     */
    EXTERNAL("External"),

    /**
     * The element is at the same time internal and external.
     * This is a bad situation.
     * Should only happen to Packages if things are wrongly done.
     */
    MIXED("Mixed"),

    /**
     * The element has been analyzed.
     */
    INTERNAL("Internal");

    private final String label;

    private DElementScope(String label) {
        this.label = label;
    }

    public final String getLabel() {
        return label;
    }

    public boolean isMixedOrInternal() {
        return this == INTERNAL || this == MIXED;
    }

    public boolean isExternalOrMixed() {
        return this == EXTERNAL || this == MIXED;
    }

    public static DElementScope merge(DElementScope left,
                                      DElementScope right) {
        switch (left) {
        case UNKNOWN:
            return right;
        case EXTERNAL:
            return right.isMixedOrInternal() ? MIXED : EXTERNAL;
        case INTERNAL:
            return right.isExternalOrMixed() ? MIXED : INTERNAL;
        default:
            return MIXED;
        }
    }
}