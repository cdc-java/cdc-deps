package cdc.deps;

/**
 * Enumeration of the different parts that can be extracted from a namespace.
 *
 * @author Damien Carbonne
 *
 */
public enum DNamePart {
    /**
     * The complete name.
     */
    NAME,

    /**
     * For an Item, this is the items part (removing packages part).
     * <ul>
     * <li>{@code package1/package2/Item1$Item2 --> Item1$Item2}</li>
     * </ul>
     * <p>
     * For a package, this is the name of the package, excluding its
     * parents.
     * <ul>
     * <li>{@code package1/package2 --> package2}</li>
     * </ul>
     */
    BASE_NAME,

    /**
     * For an Item, this is the packages part.
     * <ul>
     * <li>{@code package1/package2/Item1$Item2 --> package1/package2}</li>
     * <li>{@code Item1$Item2 --> ""}</li>
     * </ul>
     * <p>
     * For a package, this is the parent name.
     * <ul>
     * <li>{@code package1/package2/package3 --> package1/package2}</li>
     * <li>{@code package1 --> ""}</li>
     * </ul>
     */
    PATH,

    /**
     * In all cases, this is the name of the namespace, excluding its
     * parents.
     * <ul>
     * <li>{@code package1/package2/Item1$Item2 --> Item2}</li>
     * </ul>
     * Local name of a package equals its base name.
     * <ul>
     * <li>{@code package1/package2/package3 --> package3}</li>
     * </ul>
     */
    LOCAL_NAME,

    /**
     * TODO Local path of an Item is ???
     * <ul>
     * <li>{@code package1/package2/Item1$Item2 --> ???}</li>
     * </ul>
     * Local path of a package equals its path.<br>
     * <ul>
     * <li>{@code package1/package2/package3 --> package1/package2}</li>
     * </ul>
     */
    LOCAL_PATH
}