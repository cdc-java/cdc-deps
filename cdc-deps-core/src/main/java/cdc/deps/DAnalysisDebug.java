package cdc.deps;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.util.function.IterableUtils;

public final class DAnalysisDebug {
    private DAnalysisDebug() {
    }

    public static final void print(DAnalysis analysis,
                                   PrintStream out) {
        // Number of elements per kind.
        final int[] elementsCounts = new int[DElementKind.values().length];
        // Number of dependencies per level and kind.
        final int[] elementsDeps = new int[DElementKind.values().length];

        // Count elements and dependencies
        for (final DElement element : analysis.getElements()) {
            elementsCounts[element.getKind().ordinal()]++;
            elementsDeps[element.getKind().ordinal()] += IterableUtils.size(element.getOutgoingDependencies());
        }

        // Print counts
        for (final DElementKind kind : DElementKind.values()) {
            out.print(kind.name() + ": " + elementsCounts[kind.ordinal()]);
            out.print(" " + elementsDeps[kind.ordinal()]);
            out.println();
        }

        out.println();

        // Recursively print elements (as trees)
        final List<DElement> sortedRoots = new ArrayList<>();
        sortedRoots.addAll(analysis.getRoots());
        Collections.sort(sortedRoots);
        for (final DElement element : sortedRoots) {
            if (element.getKind() == DElementKind.GROUP) {
                printGroup((DGroup) element, out, 0);
            }
        }
        for (final DElement element : sortedRoots) {
            if (element.getKind() == DElementKind.PACKAGE) {
                printNamespace(analysis, (DPackage) element, out, 0);
            }
        }
    }

    private static void indent(PrintStream out,
                               int level) {
        for (int index = 0; index < 3 * level; index++) {
            out.print(" ");
        }
    }

    private static final void printGrouped(DElement grouped,
                                           PrintStream out,
                                           int level) {
        if (grouped instanceof DGroup) {
            printGroup((DGroup) grouped, out, level);
        } else {
            indent(out, level);
            out.println(grouped);
        }
    }

    private static final void printGroup(DGroup group,
                                         PrintStream out,
                                         int level) {
        indent(out, level);
        out.println(group + " " + group.getScope() + " " + group.getSortedFeatures());
        final List<DElement> sortedItems = new ArrayList<>();
        for (final DElement item : group.getChildren()) {
            sortedItems.add(item);
        }
        Collections.sort(sortedItems);

        for (final DElement item : sortedItems) {
            printGrouped(item, out, level + 1);
        }
    }

    private static final void printNamespace(DAnalysis analysis,
                                             DNamespace ns,
                                             PrintStream out,
                                             int level) {
        indent(out, level);
        out.print(ns + " " + ns.getScope() + " " + ns.getSortedFeatures());
        if (ns instanceof DItem) {
            final DItem type = (DItem) ns;
            if (type.getOwner() != null) {
                out.print(" <<< ");
                out.print(type.getOwner());
            }
        }
        out.println();
        final ArrayList<DDependency> sortedDeps = new ArrayList<>();
        sortedDeps.addAll(ns.getOutgoingDependencies());
        Collections.sort(sortedDeps);
        for (final DDependency dep : sortedDeps) {
            indent(out, level + 1);
            out.println(" --> " + dep.getTarget());
        }

        final List<DNamespace> sortedChildren = new ArrayList<>();
        sortedChildren.addAll(ns.getChildren());
        Collections.sort(sortedChildren);
        for (final DNamespace child : sortedChildren) {
            printNamespace(analysis, child, out, level + 1);
        }
    }
}