package cdc.deps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Base abstract element of an analysis.<br>
 * There are 3 variants of elements:
 * <ul>
 * <li>GROUP: It is a 'physical' element that contains 'logical' elements or
 * other groups.</li>
 * <li>PACKAGE: It is a 'logical' element that can contains items or other
 * packages.</li>
 * <li>ITEM: It is also a 'logical' element that can contain other items.</li>
 * </ul>
 * The distinction between PACKAGE and ITEM is done to facilitate analysis
 * exploitation.<br>
 * <br>
 *
 * All elements share the following characteristics:
 * <ul>
 * <li>An element has a name.</li>
 * <li>An element may have a category. This is used to specialize elements for
 * specific usages.</li>
 * <li>An element may have any number of features.</li>
 * </ul>
 * In Java, this could be used like this:
 * <ul>
 * <li>GROUP
 * <ul>
 * <li>categories: jar file, class file</li>
 * </ul>
 * </li>
 * <li>PACKAGE
 * <ul>
 * <li>categories: package</li>
 * </ul>
 * </li>
 * <li>ITEM
 * <ul>
 * <li>categories: class, interface, enum, method, field, ...</li>
 * <li>features: PUBLIC, PRIVATE, PACKAGE, STATIC, FINAL, ...</li>
 * </ul>
 * </li>
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public abstract class DElement implements Comparable<DElement> {
    private final int id;

    /** Name of the element (Mandatory). */
    private final String name;

    /** Scope of the element, relatively to the analysis. */
    private DElementScope scope = DElementScope.UNKNOWN;

    /** Optional category. */
    private String category = null;

    /** Optional features. */
    private Set<String> features;

    private static final Set<String> EMPTY_FEATURES = Collections.unmodifiableSet(new HashSet<String>());

    /** Outgoing dependencies. */
    final List<DDependency> outgoings = new ArrayList<>();

    /** Ingoing dependencies. */
    final List<DDependency> ingoings = new ArrayList<>();

    public static final Comparator<DElement> ID_COMPARATOR = (o1,
                                                              o2) -> o1.getId() - o2.getId();

    public static final Comparator<DElement> NAME_COMPARATOR = (o1,
                                                                o2) -> o1.getName().compareTo(o2.getName());

    protected DElement(int id,
                       String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("DElement.<init>(" + id + ", " + name + ") Invalid name");
        }
        this.id = id;
        this.name = name;
    }

    protected String getContext() {
        return getClass().getCanonicalName();
    }

    public final int getId() {
        return id;
    }

    /**
     * Return the kind of the element.
     *
     * @return The kind of the element.
     */
    public abstract DElementKind getKind();

    /**
     * Return whether this element is a root or not.
     * <ul>
     * <li>A group is a root if it has no parent group.</li>
     * <li>A namespace (package or item) is a root if it has no parent
     * namespace.
     * </li>
     * </ul>
     *
     * @return Whether this element is a root or not.
     */
    public abstract boolean isRoot();

    /**
     * Returns the group that owns this element.
     * <p>
     * The ability for an element to have an owner depends on the mapping.<br>
     * In Java, e.g., a package can normally not belong to a group.
     * It is feasible to declare 2 classes in the same package, but not the same folder, or jar.
     *
     * @return The group that owns this element.
     */
    public abstract DGroup getOwner();

    /**
     * Returns the root group that owns this element.
     * <p>
     * This may be null.
     *
     * @return The root group that owns this element.
     */
    public final DGroup getRootOwner() {
        final DGroup owner = getOwner();
        return owner == null ? null : owner.getRoot();
    }

    /**
     * Returns the element that owns this element.
     * <p>
     * For a Group, getOwner() == getParent().
     * <ul>
     * <li>A group's parent is a group.</li>
     * <li>A package's parent is a package.</li>
     * <li>An item's parent is a namespace (another item or a package).</li>
     * </ul>
     *
     * @return The element that owns this element.
     */
    public abstract DElement getParent();

    /**
     * Returns the root ancestor reached with getParent().
     * <p>
     * If this element has no parent, then returns this element.
     *
     * @return The root ancestor of this element.
     */
    public DElement getRoot() {
        DElement index = this;
        while (index.getParent() != null) {
            index = index.getParent();
        }
        return index;
    }

    /**
     * Return a list of children elements of this element.
     *
     * @return A list of children elements of this element.
     */
    public abstract List<? extends DElement> getChildren();

    public final <E extends DElement> List<E> getSortedChildren(Class<E> klass) {
        final List<E> result = new ArrayList<>();
        for (final DElement child : getChildren()) {
            if (klass.isInstance(child)) {
                result.add(klass.cast(child));
            }
        }
        Collections.sort(result, DElement.NAME_COMPARATOR);
        return result;
    }

    /**
     * Returns the number of children of a given class.
     *
     * @param <E> The element type.
     * @param klass The class
     * @return The number of children whose class is klass.
     */
    public final <E extends DElement> int getChildrenCount(Class<E> klass) {
        int count = 0;
        for (final DElement child : getChildren()) {
            if (klass.isInstance(child)) {
                count++;
            }
        }
        return count;
    }

    public final <E extends DElement> int getDeepContentCount(Class<E> klass) {
        int count = 0;
        for (final DElement child : getChildren()) {
            if (klass.isInstance(child)) {
                count++;
                count += child.getDeepContentCount(klass);
            }
        }
        return count;
    }

    public final <E extends DElement> List<E> getSortedDeepContent(Class<E> klass) {
        final Set<E> content = new HashSet<>();
        addDeepContent(klass, content);
        final List<E> result = new ArrayList<>();
        result.addAll(content);
        Collections.sort(result, DElement.NAME_COMPARATOR);
        return result;
    }

    public final <E extends DElement> Set<E> getDeepContent(Class<E> klass) {
        final Set<E> content = new HashSet<>();
        addDeepContent(klass, content);
        return content;
    }

    private <E extends DElement> void addDeepContent(Class<E> klass,
                                                     Set<E> content) {
        for (final DElement child : getChildren()) {
            if (klass.isInstance(child)) {
                content.add(klass.cast(child));
                child.addDeepContent(klass, content);
            }
        }
    }

    /**
     * Returns the number of children of a given class and belonging to a group.
     *
     * @param <E> The namespace type.
     * @param klass The class.
     * @param owner The owning group.
     * @return The number of children whose class is klass and which belong to owner group.
     */
    public final <E extends DNamespace> int getChildrenCountInGroup(Class<E> klass,
                                                                    DGroup owner) {
        int count = 0;
        for (final DElement child : getChildren()) {
            if (klass.isInstance(child) && child.getOwner() == owner) {
                count++;
            }
        }
        return count;
    }

    /**
     * Returns the name of the element.
     *
     * @return The name of the element.
     */
    public final String getName() {
        return name;
    }

    protected void setScope(DElementScope scope) {
        this.scope = scope;
    }

    protected void invalidScopeChange(DElementScope scope) {
        throw new IllegalArgumentException("Can not change scope from " + getScope() + " to " + scope + " for " + getKind());
    }

    /**
     * Returns the scope of the element, relatively to the analysis.
     *
     * @return The scope of the element.
     */
    public final DElementScope getScope() {
        return scope;
    }

    /**
     * Sets the category of the element.
     *
     * @param category The category.
     */
    public final void setCategory(String category) {
        this.category = category;
    }

    /**
     * Returns the (optional) category of the element.
     *
     * @return The category of the element, possibly null
     */
    public final String getCategory() {
        return category;
    }

    /**
     * Enables of disables a feature.
     *
     * @param feature The feature.
     * @param enabled If true, enable feature. Otherwise, disable it.
     */
    public final void setEnabled(String feature,
                                 boolean enabled) {
        if (feature != null) {
            if (enabled) {
                if (features == null) {
                    features = new HashSet<>();
                }
                features.add(feature);
            } else {
                if (features != null) {
                    features.remove(feature);
                }
            }
        }
    }

    /**
     * Returns a set of enabled features.
     *
     * @return A set of enabled features.
     */
    public final Set<String> getFeatures() {
        if (features == null) {
            return EMPTY_FEATURES;
        } else {
            return features;
        }
    }

    public final List<String> getSortedFeatures() {
        final ArrayList<String> result = new ArrayList<>();
        result.addAll(getFeatures());
        Collections.sort(result);
        return result;
    }

    /**
     * Returns whether a feature is enabled or not.
     *
     * @param feature The feature.
     * @return Whether feature is enabled or not.
     */
    public final boolean isEnabled(String feature) {
        if (features != null) {
            return features.contains(feature);
        } else {
            return false;
        }
    }

    /**
     * Return a list of all outgoing dependencies of this element.
     *
     * @return A list of all outgoing dependencies of this element.
     */
    public final List<DDependency> getOutgoingDependencies() {
        return outgoings;
    }

    /**
     * Returns the outgoing dependency from this element to a target element, or
     * null.
     *
     * @param target The target element.
     * @return The corresponding outgoing dependency, or null.
     */
    public final DDependency getOutgoingDependency(DElement target) {
        for (final DDependency dependency : outgoings) {
            if (dependency.getTarget() == target) {
                return dependency;
            }
        }
        return null;
    }

    /**
     * Returns a list of all ingoing dependencies of this element.
     *
     * @return A list of all ingoing dependencies of this element.
     */
    public final List<DDependency> getIngoingDependencies() {
        return ingoings;
    }

    /**
     * Returns the ingoing dependency from a source element to this element, or
     * null.
     *
     * @param source The source element.
     * @return The corresponding ingoing dependency, or null.
     */
    public final DDependency getIngoingDependency(DElement source) {
        for (final DDependency dependency : ingoings) {
            if (dependency.getSource() == source) {
                return dependency;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object other) {
        return this == other;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(DElement other) {
        final int cmpKind = getKind().compareTo(other.getKind());
        if (cmpKind == 0) {
            return getName().compareTo(other.getName());
        } else {
            return cmpKind;
        }
    }

    @Override
    public String toString() {
        return "[" + getKind() + " " + getScope() + " " + getName() + " " + getCategory() + "]";
    }
}