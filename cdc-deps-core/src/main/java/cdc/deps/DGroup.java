package cdc.deps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Group definition, a 'physical' artifact that can contains other 'physical' or
 * 'logical' artifacts.<br>
 * A DGroup can correspond to a file, an archive, a project, ...
 *
 * @author Damien Carbonne
 *
 */
public final class DGroup extends DElement {
    private static final Logger LOGGER = LogManager.getLogger(DGroup.class);
    /** Parent group. */
    private final DGroup owner;

    /** Children elements. */
    List<DElement> children = null;

    private static final List<DElement> EMPTY = Collections.unmodifiableList(new ArrayList<DElement>());

    DGroup(int id,
           String name,
           DGroup owner) {
        super(id, name);
        this.owner = owner;
        if (owner != null) {
            owner.addChild(this);
        }
    }

    protected void addChild(DElement item) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(item);
    }

    @Override
    public void setScope(DElementScope scope) {
        LOGGER.trace(this + ".setScope(" + scope + ")");
        // Valid transitions:
        // ??? --> ???
        // UNKNOWN --> INTERNAL
        if (scope == getScope()) {
            // Ignore
        } else if (scope != DElementScope.INTERNAL || getScope() == DElementScope.INTERNAL) {
            invalidScopeChange(scope);
        }
        super.setScope(scope);
    }

    @Override
    public final DElementKind getKind() {
        return DElementKind.GROUP;
    }

    @Override
    public final boolean isRoot() {
        return getOwner() == null;
    }

    @Override
    public final DGroup getOwner() {
        return owner;
    }

    @Override
    public final DGroup getParent() {
        return owner;
    }

    @Override
    public final DGroup getRoot() {
        return (DGroup) super.getRoot();
    }

    @Override
    public final List<DElement> getChildren() {
        if (children == null) {
            return EMPTY;
        } else {
            return children;
        }
    }

    /**
     * Returns a map given the number of items contained in this group and that are associated to a package.
     *
     * @return A count of owned items in packages.
     */
    public Map<DPackage, Integer> getItemsCount() {
        final Map<DPackage, Integer> result = new HashMap<>();
        accumulateItems(result);
        return result;
    }

    private void accumulateItems(Map<DPackage, Integer> map) {
        for (final DElement child : getChildren()) {
            if (child instanceof DItem) {
                final DItem item = (DItem) child;
                final DPackage p = item.getPackage();
                if (map.containsKey(p)) {
                    map.put(p, map.get(p) + 1);
                } else {
                    map.put(p, 1);
                }
            } else if (child instanceof DGroup) {
                ((DGroup) child).accumulateItems(map);
            }
            // Ignore if child is a package
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}