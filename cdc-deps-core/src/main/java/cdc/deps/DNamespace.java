package cdc.deps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Base class of objects that can be considered as namespaces: Packages and
 * Items.
 *
 * @see DPackage
 * @see DItem
 * @author Damien Carbonne
 *
 */
public abstract class DNamespace extends DElement {
    /** Parent group. */
    private DGroup group;

    /** Parent namespace. */
    private final DNamespace parent;

    /** Children namespaces. */
    List<DNamespace> children = null;

    private static final List<DNamespace> EMPTY = Collections.unmodifiableList(new ArrayList<DNamespace>());

    protected DNamespace(int id,
                         String name,
                         DGroup group,
                         DNamespace parent) {
        super(id, name);
        this.parent = parent;
        if (parent != null) {
            parent.addChildNamespace(this);
        }
        if (group != null) {
            setGroup(group);
        }
    }

    protected static String getNameTail(String name,
                                        String head) {
        if (name.startsWith(head)) {
            return name.substring(head.length() + 1);
        } else {
            return name;
        }
    }

    private void addChildNamespace(DNamespace namespace) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(namespace);
    }

    @Override
    public final boolean isRoot() {
        return getParent() == null;
    }

    /**
     * Return the group that owns this element.
     * <p>
     * The ability for an element to have an owner depends on the mapping.<br>
     * In Java, e.g., a package can normally not belong to a group.
     * It is feasible to declare 2 classes in the same package, but not the same folder, or jar.
     *
     * @return The group that owns this element.
     */
    @Override
    public final DGroup getOwner() {
        return group;
    }

    /**
     * Sets the group that owns this namespace.
     * <p>
     * The Group if a Namespace can be defined lately. This can be done only if
     * current group is null.
     *
     * @param group The owning group.
     */
    public final void setGroup(DGroup group) {
        if (this.group == null) {
            this.group = group;
            if (group != null) {
                group.addChild(this);
            }
        } else {
            throw new IllegalStateException(getContext() + ".setGroup(" + group + ") FAILED");
        }
    }

    /**
     * Returns a part of the name of this namespace.
     *
     * @param part The part top return.
     * @return The section of the name that corresponds to part.
     */
    public abstract String getNamePart(DNamePart part);

    @Override
    public DNamespace getParent() {
        return parent;
    }

    @Override
    public DNamespace getRoot() {
        return (DNamespace) super.getRoot();
    }

    @Override
    public final List<DNamespace> getChildren() {
        if (children == null) {
            return EMPTY;
        } else {
            return children;
        }
    }

    /**
     * Returns true when this namespace is an ancestor of another one.
     *
     * @param other The other namespace.
     * @return True when this namespace is an ancestor of other namespace.
     */
    public final boolean isAncestorOf(DNamespace other) {
        DNamespace index = other;
        while (index != null) {
            if (index == this) {
                return true;
            }
            index = index.getParent();
        }
        return false;
    }

    /**
     * Returns the package that contains this namespace.
     * <p>
     * <em>Result may be null.</em><br>
     * {@code package1 --> null}<br>
     * {@code package1/package2 --> package1}<br>
     * {@code package1/Item1@Item2 --> package1}<br>
     * {@code Item1@Item2 --> null}
     *
     * @return the package that contains this namespace.
     */
    public abstract DPackage getPackage();

    /**
     * Returns this namespace if it is a package, otherwise, return the first
     * package over this item.
     * <p>
     * <em>Result may be null.</em><br>
     * {@code package1 --> package1}<br>
     * {@code package1/package2 --> package1/package2}<br>
     * {@code package1/Item1@Item2 --> package1}<br>
     * {@code Item1@Item2 --> null}
     *
     * @return The first package over this element, possibly the element itself.
     */
    public final DPackage getImmediatePackage() {
        if (getKind() == DElementKind.PACKAGE) {
            return (DPackage) this;
        } else {
            return getPackage();
        }
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}