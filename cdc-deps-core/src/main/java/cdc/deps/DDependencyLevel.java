package cdc.deps;

/**
 * Level of a dependency.
 *
 * @author Damien Carbonne
 *
 */
public enum DDependencyLevel {
    /** The dependency is directly present in the analyzed data. */
    PRIMITIVE,
    /** The dependency is deduced from other dependencies. */
    DERIVED
}