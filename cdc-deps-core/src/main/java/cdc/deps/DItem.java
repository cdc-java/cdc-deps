package cdc.deps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Item definition. This is a 'logical' artifact.
 *
 * @author Damien Carbonne
 *
 */
public final class DItem extends DNamespace {
    private static final Logger LOGGER = LogManager.getLogger(DItem.class);

    DItem(int id,
          String name,
          DGroup group,
          DNamespace parent) {
        super(id, name, group, parent);
    }

    @Override
    public final String getNamePart(DNamePart part) {
        switch (part) {
        case NAME:
            return getName();
        case BASE_NAME:
            if (getPackage() != null) {
                final String head = getPackage().getName();
                return getNameTail(getName(), head);
            } else {
                return getName();
            }
        case LOCAL_NAME:
            if (getParent() != null) {
                final String head = getParent().getName();
                return getNameTail(getName(), head);
            } else {
                return getName();
            }
        case PATH:
            if (getPackage() != null) {
                return getPackage().getName();
            } else {
                return "";
            }
        case LOCAL_PATH:
            if (getParent() != null) {
                return getParent().getName(); // TODO ???
            } else {
                return "";
            }
        default:
            return null;
        }
    }

    @Override
    public void setScope(DElementScope scope) {
        LOGGER.trace(this + ".setScope(" + scope + ")");
        // Valid transitions:
        // UNKNOWN  --> UNKNOWN | EXTERNAL | MIXED | INTERNAL
        // EXTERNAL -->           EXTERNAL | MIXED | INTERNAL
        // MIXED    -->                      MIXED | INTERNAL
        // INTERNAL -->                              INTERNAL

        if (scope == getScope()) {
            // Ignore
        } else {
            switch (getScope()) {
            case UNKNOWN:
                break;
            case EXTERNAL:
            case MIXED:
                if (!scope.isMixedOrInternal()) {
                    invalidScopeChange(scope);
                }
                break;
            default:
                invalidScopeChange(scope);
            }
            super.setScope(scope);

            final DPackage p = getPackage();
            if (p != null) {
                p.refreshScope();
            }

            if (scope.isMixedOrInternal() && getParent().getKind() == DElementKind.ITEM) {
                final DItem parent = (DItem) getParent();
                parent.fixOuterScope();
            }
        }
    }

    private void fixOuterScope() {
        LOGGER.trace(this + ".fixOuterScope()");
        if (getScope() == DElementScope.EXTERNAL) {
            super.setScope(DElementScope.MIXED);
            if (getParent().getKind() == DElementKind.ITEM) {
                ((DItem) getParent()).fixOuterScope();
            }
        }
    }

    @Override
    public final DElementKind getKind() {
        return DElementKind.ITEM;
    }

    @Override
    public final DPackage getPackage() {
        DNamespace index = getParent();
        while (index != null && index.getKind() != DElementKind.PACKAGE) {
            index = index.getParent();
        }
        return (DPackage) index;
    }
}