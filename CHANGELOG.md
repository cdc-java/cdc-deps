# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- Created tests with demos.
 
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - cdc-office-0.60.1
    - org.junit-5.12.0
- Used `ASM9` instead of `ASM7` in `AsmDependencyVisitor` to support analysis of records.  
  An exception is thrown in a demo. It is handled, but should be understood and fixed.


## [0.52.3] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.3
    - cdc-gv-0.100.3
    - cdc-io-0.53.1
    - cdc-office-0.58.1
    - cdc-ui-0.31.3
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3
    - org.ow2.asm-9.7.1


## [0.52.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.2
    - cdc-gv-0.100.2
    - cdc-io-0.52.1
    - cdc-office-0.57.2
    - cdc-ui-0.31.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - org.junit-5.11.1
    - saxon-12.5
- Updated maven plugins.


## [0.52.1] - 2024-05-19
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.1
    - cdc-gv-0.100.1
    - cdc-io-0.52.0
    - cdc-office-0.55.0
    - cdc-ui-0.31.1
    - cdc-util-0.52.1
    - commons-cli-1.7.0
    - org.apache.log4j-2.23.1
    - org.ow2.asm-9.7
- Updated maven plugins.


## [0.52.0] - 2024-02-17
### Changed
- Updated dependencies:
    - cdc-office-0.53.0
    - org.junit-5.10.2

### Removed
- Removed htmlflow code. It was incomplete and depracated.


## [0.51.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.0
    - cdc-gv-0.100.0
    - cdc-io-0.51.0
    - cdc-office-0.52.0
    - cdc-ui-0.31.0
    - cdc-util-0.52.0
    - saxon-12.4
- Added `exec()` and call `System.exit()` in `DepsFilter`, `DepsToGv`, `DepsToHtml`, `JavaDeps` and `VbaDeps`.
- Updated maven plugins.


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-graphs-0.70.0
    - cdc-gv-0.99.0
    - cdc-io-0.50.0
    - cdc-office-0.50.0
    - cdc-tuples-1.4.0
    - cdc-ui-0.30.0
    - cdc-util-0.50.0


## [0.21.12] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-graphs-0.65.5
    - cdc-gv-0.96.9
    - cdc-io-0.27.3
    - cdc-office-0.31.0
    - cdc-tuples-1.3.0
    - cdc-ui-0.20.15
    - cdc-util-0.33.2
    - commons-cli-1.6.0
    - org.junit-5.10.1


## [0.21.11] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-graphs-0.65.2
    - cdc-gv-0.96.8
    - cdc-io-0.27.2
    - cdc-office-0.30.2
    - cdc-ui-0.20.14
    - cdc-util-0.33.1
    - org.junit-5.10.0
    - org.ow2.asm-9.6
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.21.10] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-graphs-0.65.1
    - cdc-gv-0.96.7
    - cdc-io-0.27.0
    - cdc-office-0.30.1
    - cdc-ui-0.20.13
    - cdc-util-0.33.0
    - saxon-12.3


## [0.21.9] - 2023-04-22
### Changed
- Updated dependencies:
    - cdc-office-0.29.4
    - cdc-ui-0.20.12


## [0.21.8] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-graphs-0.64.5
    - cdc-gv-0.96.5
    - cdc-io-0.26.0
    - cdc-office-0.29.3
    - cdc-ui-0.20.11
    - org.ow2.asm-9.5
    - saxon-12.1


## [0.21.7] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-graphs-0.64.4
    - cdc-gv-0.96.4
    - cdc-io-0.25.1
    - cdc-office-0.29.1
    - cdc-ui-0.20.10
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.21.6] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-graphs-0.64.3
    - cdc-gv-0.96.3
    - cdc-io-0.24.0
    - cdc-office-0.29.0
    - cdc-ui-0.20.9
    - cdc-util-0.29.0
    - org.junit-5.9.2
    - saxon-12.0


## [0.21.5] - 2023-01-02
### Changed
- Updated dependencies:
    - cdc-graphs-0.64.2
    - cdc-gv-0.96.2
    - cdc-io-0.23.2
    - cdc-office-0.28.0
    - cdc-ui-0.20.8
    - cdc-util-0.28.2


## [0.21.4] - 2022-11-12
### Changed
- Updated dependencies:
    - cdc-graphs-0.64.1
    - cdc-gv-0.96.1
    - cdc-io-0.23.1
    - cdc-office-0.27.0
    - cdc-ui-0.20.7
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1
    - org.ow2.asm-9.4


## [0.21.3] - 2022-08-24
### Changed
- Updated dependencies:
    - cdc-graphs-0.63.2
    - cdc-gv-0.96.0
    - cdc-io-0.23.0
    - cdc-office-0.24.0
    - cdc-tuples-1.2.0
    - cdc-ui-0.20.6
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.21.2] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-graphs-0.63.1
    - cdc-gv-0.95.5
    - cdc-io-0.22.0
    - cdc-office-0.23.1
    - cdc-ui-0.20.5
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.21.1] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-graphs-0.63.0
    - cdc-gv-0.95.4
    - cdc-io-0.21.3
    - cdc-office-0.23.0
    - cdc-ui-0.20.4
    - cdc-util-0.26.0

### Fixed
- Fixed the issue with `odfdom-java`. The fix was done in `cdc-office`. #17


## [0.21.0] - 2022-05-21
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-graphs-0.61.1
    - cdc-gv-0.95.3
    - cdc-io-0.21.2
    - cdc-office-0.22.0
    - cdc-ui-0.20.3
    - cdc-util-0.25.0
    - org.apache.log4j-2.17.2
    - org.ow2.asm-9.3
    - saxon-11.3
- `Config` data is now retrieved from Manifest.


## [0.20.0] - 2022-02-05
### Changed
- Upgraded to Java 11
- Updated dependencies:
    - cdc-graphs-0.60.0
    - cdc-gv-0.95.0
    - cdc-io-0.20.0
    - cdc-office-0.20.0
    - cdc-ui-0.20.0
    - htmlflow-3.9
    - saxon-11.1.1
- Removed runtime dependency on cdc-office-ss-odf. That creates a conflict with an old version of ASM.


## [0.10.2] - 2022-01-29
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-office-0.15.0


## [0.10.1] - 2022-01-15
### Security
- Updated dependencies:
    - cdc-graphs-0.52.2
    - cdc-gv-0.92.2
    - cdc-io-0.13.2
    - cdc-office-0.14.2
    - cdc-ui-0.13.2
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1. #16


## [0.10.0] - 2021-12-28
### Added
- Created CHANGELOG.

### Security
- Updated dependencies:
    - asm-9.2
    - cdc-graphs-0.52.1
    - cdc-gv-0.92.1
    - cdc-io-0.13.1
    - cdc-office-0.14.1
    - cdc-ui-0.13.1
    - cdc-util-0.14.1
    - commons-cli-1.5.0
    - com.github.xmlet.htmlflow-3.8
    - net.sf.saxon-10.6
    - org.apache.log4j-2.17.0. #16


## [0.9.0] - 2021-03-29
### Changed
- Updated dependencies.

### Fixed
- Javadoc


## [0.8.0] - 2021-03-07
### Changed
- Updated dependencies.


## [0.7.0] - 2021-03-01
### Changed
- Updated dependencies.


## [0.6.0] - 2021-02-22
### Changed
- Updated dependencies.


## [0.5.0] - 2021-02-07
### Changed
- Updated dependencies.


## [0.4.0] - 2021-01-17
### Changed
- Updated dependencies.


## [0.3.0] - 2020-12-13
### Changed
- Updated dependencies.

### Fixed
- Warnings.


## [0.2.0] - 2020-10-06
### Changed
- Updated dependencies.


## [0.1.1] - 2020-05-06
### Changed
- Updated dependencies.

### Removed
- eclipse generated files.


## [0.1.0] - 2020-04-19
### Changed
- Updated dependencies.

## [0.0.22] - 2020-02-03


## [0.0.21] - 2020-01-12


## [0.0.20] - 2019-11-24


## [0.0.19] - 2019-10-30


## [0.0.18] - 2019-10-20


## [0.0.17] - 2019-09-29


## [0.0.15] - 2019-07-06


## [0.0.14] - 2019-05-28


## [0.0.12] - 2019-03-25


## [0.0.11.3] - 2019-03-09


## [0.0.11.2] - 2019-03-01


## [0.0.11] - 2019-01-20


## [0.0.10] - 2018-12-09


## [0.0.9] - 2018-10-10


## [0.0.8] - 2018-09-08


## [0.0.7] - 2018-05-21


## [0.0.6] - 2018-04-30


## [0.0.5] - 2018-03-27


## [0.0.4] - 2018-02-13


## [0.0.3] - 2017-12-10


## [0.0.2] - 2017-10-30


## [0.0.1] - 2017-10-15
### Added
- First release.

