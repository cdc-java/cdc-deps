package cdc.deps.java.p1;

public class A {
    public static class StaticNested1 {
        public static class StaticNested2 {
            public static class StaticNested3 {
                //
            }
        }
    }

    protected abstract class ProtectedAbstract {
        //
    }
}