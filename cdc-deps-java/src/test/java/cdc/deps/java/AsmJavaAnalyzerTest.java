package cdc.deps.java;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.deps.DAnalysisDebug;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.java.asm.AsmJavaAnalyzer;

class AsmJavaAnalyzerTest {
    private static final Logger LOGGER = LogManager.getLogger(AsmJavaAnalyzerTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private static final File TARGET = new File("./target");
    private static final File TEST_CLASSES = new File(TARGET, "test-classes");

    @Test
    void testAAnalysis() throws Exception {
        final AsmJavaAnalyzer analyzer = new AsmJavaAnalyzer();
        analyzer.setEnabled(DJavaAnalyzer.Feature.IGNORE_JDK_TYPES, true);
        final DJavaAnalysis analysis = analyzer.getAnalysis();

        analyzer.analyzeClassFile(new File(TEST_CLASSES, "cdc/deps/java/p1/A.class"));
        analyzer.postAnalysis();
        DAnalysisDebug.print(analysis, OUT);

        {
            final DElement element = analysis.getElement("cdc");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.UNKNOWN, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        // {
        // final DElement element = analysis.getElement("cdc/test");
        // assertNotNull(element);
        // assertEquals(DElementKind.PACKAGE, element.getKind());
        // assertEquals(DElementScope.UNKNOWN, element.getScope());
        // assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        // }

        {
            final DElement element = analysis.getElement("cdc/deps");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.UNKNOWN, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.MIXED, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1/A");
            assertNotNull(element);
            assertEquals(DElementKind.ITEM, element.getKind());
            assertEquals(DElementScope.INTERNAL, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_CLASS, element.getCategory());
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_PUBLIC));
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1/A$StaticNested1");
            assertNotNull(element);
            assertEquals(DElementKind.ITEM, element.getKind());
            assertEquals(DElementScope.MIXED, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_CLASS, element.getCategory());
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_NESTED));
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_PUBLIC));
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_STATIC));
        }

        analyzer.analyzeClassFile(new File(TEST_CLASSES, "cdc/deps/java/p1/A$StaticNested1.class"));
        analyzer.postAnalysis();
        DAnalysisDebug.print(analyzer.getAnalysis(), OUT);

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1/A$StaticNested1");
            assertNotNull(element);
            assertEquals(DElementKind.ITEM, element.getKind());
            assertEquals(DElementScope.INTERNAL, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_CLASS, element.getCategory());
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_NESTED));
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_PUBLIC));
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_STATIC));
        }
    }

    @Test
    void testCAnalysis() throws Exception {
        final AsmJavaAnalyzer analyzer = new AsmJavaAnalyzer();
        final DJavaAnalysis analysis = analyzer.getAnalysis();
        analyzer.setEnabled(DJavaAnalyzer.Feature.IGNORE_JDK_TYPES, true);

        analyzer.analyzeClassFile(new File(TEST_CLASSES, "cdc/deps/java/p1/C.class"));
        analyzer.postAnalysis();

        DAnalysisDebug.print(analysis, OUT);

        {
            final DElement element = analysis.getElement("cdc");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.UNKNOWN, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        // {
        // final DElement element = analysis.getElement("cdc/test");
        // assertNotNull(element);
        // assertEquals(DElementKind.PACKAGE, element.getKind());
        // assertEquals(DElementScope.UNKNOWN, element.getScope());
        // assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        // }

        {
            final DElement element = analysis.getElement("cdc/deps");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.UNKNOWN, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1");
            assertNotNull(element);
            assertEquals(DElementKind.PACKAGE, element.getKind());
            assertEquals(DElementScope.MIXED, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_PACKAGE, element.getCategory());
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1/C");
            assertNotNull(element);
            assertEquals(DElementKind.ITEM, element.getKind());
            assertEquals(DElementScope.INTERNAL, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_CLASS, element.getCategory());
            assertTrue(element.getFeatures().contains(DJavaAnalysis.FEATURE_PUBLIC));
        }

        {
            final DElement element = analysis.getElement("cdc/deps/java/p1/CLocal");
            assertNotNull(element);
            assertEquals(DElementKind.ITEM, element.getKind());
            assertEquals(DElementScope.EXTERNAL, element.getScope());
            assertEquals(DJavaAnalysis.CATEGORY_UNKNOWN, element.getCategory());
        }
    }
}