package cdc.deps.java;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.deps.DElementScope;

class DElementScopeTest {
    private static final DElementScope U = DElementScope.UNKNOWN;
    private static final DElementScope E = DElementScope.EXTERNAL;
    private static final DElementScope I = DElementScope.INTERNAL;
    private static final DElementScope M = DElementScope.MIXED;

    @Test
    void testMerge() {
        assertEquals(U, DElementScope.merge(U, U));
        assertEquals(E, DElementScope.merge(U, E));
        assertEquals(I, DElementScope.merge(U, I));
        assertEquals(M, DElementScope.merge(U, M));

        assertEquals(E, DElementScope.merge(E, U));
        assertEquals(E, DElementScope.merge(E, E));
        assertEquals(M, DElementScope.merge(E, I));
        assertEquals(M, DElementScope.merge(E, M));

        assertEquals(I, DElementScope.merge(I, U));
        assertEquals(M, DElementScope.merge(I, E));
        assertEquals(I, DElementScope.merge(I, I));
        assertEquals(M, DElementScope.merge(I, M));

        assertEquals(M, DElementScope.merge(M, U));
        assertEquals(M, DElementScope.merge(M, E));
        assertEquals(M, DElementScope.merge(M, I));
        assertEquals(M, DElementScope.merge(M, M));
    }
}