package cdc.deps.java;

/**
 * Enumeration of possible Java Dependencies analyzers.
 *
 * @author Damien Carbonne
 *
 */
public enum Engine {
    /**
     * Analyzer built with ASM.
     */
    ASM
}