package cdc.deps.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.util.cli.OptionEnum;
import cdc.util.files.Files;
import cdc.util.lang.ExceptionWrapper;
import cdc.util.lang.IntMasks;

/**
 * Base class for analyzers of Java dependencies.
 *
 * @author Damien Carbonne
 */
public abstract class DJavaAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(DJavaAnalyzer.class);
    private final DJavaAnalysis analysis;
    private int features = 0;

    protected DJavaAnalyzer() {
        this.analysis = new DJavaAnalysis();
    }

    public void log(String message) {
        if (isEnabled(Feature.VERBOSE)) {
            LOGGER.info(message);
        }
    }

    public enum Feature implements OptionEnum {
        VERBOSE("verbose", "Be verbose."),

        DEBUG("debug", "Print debug infos."),

        /** If enabled, annotation are ignored. */
        IGNORE_ANNOTATIONS("no-annotation-types", "Do not generate info related to annotation types."),

        /** If enabled, standard Java types are ignored. */
        IGNORE_JDK_TYPES("no-jdk-types", "Do not generate info related to jdk types."),

        IGNORE_PACKAGE_INFO("no-package-info", "Do not generate info related to package-info.java."),

        COLLAPSE_ANONYMOUS_CLASSES("no-anonymous-classes", "Do not show anonymous classes."),

        COLLAPSE_HIDDEN_TYPES("no-hidden-types", "Do not show hidden (anonymous classes or private nested classes) types."),

        COLLAPSE_TYPES("no-types", "Do not show types. They are collapsed into packages."),

        ADD_ROOT_GROUP_DEPENDENCIES("root-group-deps", "Create root group dependencies (computed from classes dependencies)."),

        ADD_PACKAGE_DEPENDENCIES("package-deps", "Create package dependencies (computed from classes dependencies).");

        private final String name;
        private final String description;

        private Feature(String name,
                        String description) {
            this.name = name;
            this.description = description;
        }

        @Override
        public final String getName() {
            return name;
        }

        @Override
        public final String getDescription() {
            return description;
        }
    }

    public final void setEnabled(Feature feature,
                                 boolean enabled) {
        features = IntMasks.setEnabled(features, feature, enabled);
    }

    public final boolean isEnabled(Feature feature) {
        return IntMasks.isEnabled(features, feature);
    }

    public final DJavaAnalysis getAnalysis() {
        return analysis;
    }

    /**
     * Analyzes one class file.
     * <p>
     * A CLASS_FILE group is associated to analysis result.
     *
     * @param file File to analyze.
     */
    public final void analyzeClassFile(File file) {
        log("analyzeClassFile(" + file + ")");
        if (file.isFile()) {
            final DGroup refGroup = analysis.createGroup(analysis.getAliasOrName(Files.normalize(file.getPath())), null);
            refGroup.setScope(DElementScope.INTERNAL);
            refGroup.setCategory(DJavaAnalysis.CATEGORY_CLASS_FILE);
            analyzeClassFile(refGroup, file);
        } else {
            LOGGER.warn("analyzeClassFile({}): invalid file", file);
        }

    }

    /**
     * Analyzes one class file and sets its group.
     *
     * @param group The group.
     * @param file The file
     */
    private final void analyzeClassFile(DGroup group,
                                        File file) {
        try (final FileInputStream is = new FileInputStream(file)) {
            analyzeClassFile(group, is);
        } catch (final IOException e) {
            LOGGER.error("analyzeClassFile({}, {}) FAILED, {}", group, file, e);
        }
    }

    /**
     * Analyzes all class files contained (directly or not) under a directory.
     *
     * @param classpath Name of the directory to analyze.
     */
    public final void analyzeClassPath(File classpath) {
        log("analyzeClassPath(" + classpath + ")");
        if (classpath.isDirectory()) {
            final DGroup refGroup = analysis.createGroup(analysis.getAliasOrName(Files.normalize(classpath.getPath())), null);
            refGroup.setScope(DElementScope.INTERNAL);
            refGroup.setCategory(DJavaAnalysis.CATEGORY_FOLDER);
            analyzeClassPathRec(refGroup, classpath, null);
        } else {
            LOGGER.warn("analyzeClassPath({}): invalid path", classpath);
        }
    }

    /**
     * Analyzes classes that are located in a directory, and recurses.
     *
     * @param refGroup The reference group.
     * @param classpath The class path.
     * @param relativePath The path, relative to classpath, into which the analysis is done.
     */
    private final void analyzeClassPathRec(DGroup refGroup,
                                           File classpath,
                                           File relativePath) {
        log("analyzeClassPathRec(" + refGroup + ", " + classpath + ", " + relativePath + ")");
        final File fullPath = relativePath == null ? classpath : new File(classpath, relativePath.getPath());
        if (fullPath.isDirectory()) {
            final String[] files = fullPath.list();
            if (files != null) {
                for (final String file : files) {
                    final File nextRelativePath = new File(relativePath, file);
                    if (nextRelativePath.getPath().endsWith(".class")) {
                        final DGroup group = analysis.findOrCreateGroup(refGroup, nextRelativePath);
                        group.setScope(DElementScope.INTERNAL);
                        group.setCategory(DJavaAnalysis.CATEGORY_CLASS);
                        analyzeClassFile(group, new File(classpath, nextRelativePath.getPath()));
                    } else {
                        final File nextFullPath = new File(classpath, nextRelativePath.getPath());
                        if (nextFullPath.isDirectory()) {
                            analyzeClassPathRec(refGroup, classpath, nextRelativePath);
                        }
                    }
                }
            }
        } else {
            LOGGER.error("Invalid directory: {}", fullPath);
        }
    }

    /**
     * Analyzes all class files contained in a jar file.
     *
     * @param jar Jar file.
     */
    public final void analyzeJarFile(File jar) {
        log("analyzeJarFile(" + jar + ")");
        if (jar.isFile()) {
            final DGroup refGroup = analysis.createGroup(analysis.getAliasOrName(Files.normalize(jar.getPath())), null);
            refGroup.setScope(DElementScope.INTERNAL);
            refGroup.setCategory(DJavaAnalysis.CATEGORY_JAR);
            try (final ZipFile archive = new ZipFile(jar)) {
                final Enumeration<? extends ZipEntry> iter = archive.entries();
                while (iter.hasMoreElements()) {
                    final ZipEntry entry = iter.nextElement();
                    final String name = entry.getName();
                    if (name.endsWith(".class")) {
                        if (name.startsWith("META-INF/version")) {
                            LOGGER.warn("Ignore multi version class {}", name);
                        } else {
                            final DGroup group = analysis.findOrCreateGroup(refGroup, new File(name));
                            group.setScope(DElementScope.INTERNAL);
                            group.setCategory(DJavaAnalysis.CATEGORY_CLASS);
                            analyzeClassFile(group, archive.getInputStream(entry));
                        }
                    }
                }
            } catch (final RuntimeException e) {
                LOGGER.catching(e);
                throw e;
            } catch (final IOException e) {
                LOGGER.catching(e);
                throw new ExceptionWrapper(e);
            }
        } else {
            LOGGER.warn("analyzeJarFile({}): invalid jar", jar);
        }
    }

    public void postAnalysis() {
        // Scope is improperly set in some rare cases that are not well understood
        // So we do this fix that should not be necessary
        // analysis.fixScopes();

        analysis.fixGroupCategory();
        analysis.addAnonymousFeature();
        analysis.addNestedFeature();

        analysis.checkScopes();
        analysis.checkCategory();

        if (isEnabled(Feature.COLLAPSE_ANONYMOUS_CLASSES)) {
            analysis.collapseAnonymousClasses();
        }
        if (isEnabled(Feature.COLLAPSE_HIDDEN_TYPES)) {
            analysis.collapseHiddenTypes();
        }
        if (isEnabled(Feature.COLLAPSE_TYPES)) {
            analysis.collapseTypes();
        }
        if (isEnabled(Feature.ADD_ROOT_GROUP_DEPENDENCIES)) {
            analysis.createRootGroupsDerivedDependencies();
        }
        if (isEnabled(Feature.ADD_PACKAGE_DEPENDENCIES)) {
            analysis.createPackageDerivedDependencies();
        }
    }

    /**
     * Analyzes a class file.
     *
     * @param group Group associated to the class file. Its scope should be set to INTERNAL
     *            and its category to CATEGORY_CLASS_FILE.
     * @param is InputStream of the class file.
     */
    protected abstract void analyzeClassFile(DGroup group,
                                             InputStream is);
}