package cdc.deps.java;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.io.xml.DAnalysisXmlWriter;
import cdc.deps.java.asm.AsmJavaAnalyzer;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.files.Files;

/**
 * Program used to analyze Java classes or jars and generate dependencies.
 *
 * @author Damien Carbonne
 *
 */
public final class JavaDeps {
    static final Logger LOGGER = LogManager.getLogger(JavaDeps.class);
    private final MainArgs margs;
    private static final String ALIAS_SEPARATOR = "::";
    private static final String CLASS_FILE = "class";
    private static final String CLASS_PATH = "class-path";
    private static final String JAR_FILE = "jar";

    public static class MainArgs {
        /**
         * Jar files directly specified on command line.
         */
        public final List<File> jarFiles = new ArrayList<>();

        /**
         * Class paths directly specified on command line.
         */
        public final List<File> classPaths = new ArrayList<>();

        /**
         * Class files directly specified on command line.
         */
        public final List<File> classFiles = new ArrayList<>();

        public final Map<String, String> aliases = new HashMap<>();

        /**
         * Optional output file.
         */
        public File output;

        /**
         * Optional Java analyzer engine.
         */
        public Engine engine;

        protected final FeatureMask<DJavaAnalyzer.Feature> features = new FeatureMask<>();

        public void setEnabled(DJavaAnalyzer.Feature feature,
                               boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public boolean isEnabled(DJavaAnalyzer.Feature feature) {
            return features.isEnabled(feature);
        }
    }

    private JavaDeps(MainArgs margs) {
        this.margs = margs;
    }

    private void execute() throws Exception {
        final DJavaAnalyzer analyzer;
        analyzer = new AsmJavaAnalyzer();
        for (final DJavaAnalyzer.Feature feature : DJavaAnalyzer.Feature.values()) {
            analyzer.setEnabled(feature, margs.isEnabled(feature));
        }
        for (final Map.Entry<String, String> entry : margs.aliases.entrySet()) {
            analyzer.getAnalysis().addAlias(entry.getKey(), entry.getValue());
        }

        // First, analyze jars
        for (final File file : margs.jarFiles) {
            analyzer.analyzeJarFile(file);
        }
        // Then, analyze class paths
        for (final File path : margs.classPaths) {
            analyzer.analyzeClassPath(path);
        }
        // Finally, analyze class files
        for (final File file : margs.classFiles) {
            analyzer.analyzeClassFile(file);
        }
        analyzer.postAnalysis();
        final String path = margs.output.getParent();
        if (path != null) {
            Files.mkdir(path);
        }
        setInfo(analyzer);
        analyzer.log("Generate  " + margs.output);
        DAnalysisXmlWriter.write(analyzer.getAnalysis(), margs.output);
        analyzer.log("Generated  " + margs.output);
    }

    private static void addAnalysisArgument(DJavaAnalysis analysis,
                                            String name,
                                            String value) {
        if (analysis.getAlias(value) != null) {
            analysis.getInfo().addAnalysisArgument(name, value + ALIAS_SEPARATOR + analysis.getAlias(value));
        } else {
            analysis.getInfo().addAnalysisArgument(name, value);
        }
    }

    private void setInfo(DJavaAnalyzer analyzer) {
        final DJavaAnalysis analysis = analyzer.getAnalysis();
        for (final DJavaAnalyzer.Feature feature : DJavaAnalyzer.Feature.values()) {
            if (margs.isEnabled(feature)) {
                analysis.getInfo().addAnalysisArgument(feature.getName());
            }
        }
        for (final File file : margs.jarFiles) {
            addAnalysisArgument(analysis, JAR_FILE, file.getPath());
        }
        for (final File file : margs.classFiles) {
            addAnalysisArgument(analysis, CLASS_FILE, file.getPath());
        }
        for (final File path : margs.classPaths) {
            addAnalysisArgument(analysis, CLASS_PATH, path.getPath());
        }
    }

    public static void execute(MainArgs margs) throws Exception {
        final JavaDeps main = new JavaDeps(margs);
        main.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String ENGINE = "engine";

        MainSupport() {
            super(JavaDeps.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return cdc.deps.Config.VERSION;
        }

        @Override
        protected String getHelpFooter() {
            return "\nAt least one of " + CLASS_FILE + ", " + CLASS_PATH + " or " + JAR_FILE + " option must be set.";
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .hasArg()
                                    .desc("Optional name of the output filename (default deps.xml).")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CLASS_FILE)
                                    .hasArgs()
                                    .desc("Optional name(s) of one or several class files. Has the form: filename["
                                            + ALIAS_SEPARATOR + "alias]")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CLASS_PATH)
                                    .hasArgs()
                                    .desc("Optional name(s) of one or several class paths. Has the form: dirname[" + ALIAS_SEPARATOR
                                            + "alias]")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(JAR_FILE)
                                    .hasArgs()
                                    .desc("Optional name(s) of one or several jar files. Has the form: jar[" + ALIAS_SEPARATOR
                                            + "alias]")
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ENGINE)
                                    .hasArg()
                                    .desc("Optional name of the engine to use (default ASM).")
                                    .build());

            addNoArgOptions(options, DJavaAnalyzer.Feature.class);
        }

        private static String getName(String s) {
            return getPart(s, ALIAS_SEPARATOR, 0);
        }

        private static String getAlias(String s) {
            return getPart(s, ALIAS_SEPARATOR, 1);
        }

        private static void analyze(String s,
                                    List<File> names,
                                    Map<String, String> aliases) {
            final String name = Files.normalize(new File(getName(s)).getPath());
            final String alias = getAlias(s);
            names.add(new File(name));
            if (alias != null) {
                aliases.put(name, alias);
            }
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            margs.output = new File(cl.getOptionValue(OUTPUT, "deps.xml"));
            margs.engine = getValueAsEnum(cl, ENGINE, Engine.class, Engine.ASM);

            if (cl.getOptionValues(JAR_FILE) != null) {
                for (final String s : cl.getOptionValues(JAR_FILE)) {
                    analyze(s, margs.jarFiles, margs.aliases);
                }
            }
            if (cl.getOptionValues(CLASS_FILE) != null) {
                for (final String s : cl.getOptionValues(CLASS_FILE)) {
                    analyze(s, margs.classFiles, margs.aliases);
                }
            }
            if (cl.getOptionValues(CLASS_PATH) != null) {
                for (final String s : cl.getOptionValues(CLASS_PATH)) {
                    analyze(s, margs.classPaths, margs.aliases);
                }
            }
            if (margs.classFiles.isEmpty() && margs.classPaths.isEmpty() && margs.jarFiles.isEmpty()) {
                throw new ParseException("Missing class, class-path or jar.");
            }

            setMask(cl, DJavaAnalyzer.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            JavaDeps.execute(margs);
            return null;
        }
    }
}