package cdc.deps.java;

import java.io.File;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DAnalysis;
import cdc.deps.DElement;
import cdc.deps.DElementKind;
import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.DPackage;
import cdc.util.files.Files;
import cdc.util.strings.StringRecognizer;

/**
 * Specialization of Dependency analysis for Java.<br>
 * At the moment analysis does not detail methods or fields. It focuses on types
 * (class, interface and enum) and packages.
 *
 * @author Damien Carbonne
 *
 */
public class DJavaAnalysis extends DAnalysis {
    public static final Logger LOGGER = LogManager.getLogger(DJavaAnalysis.class);
    public static final char PACKAGE_SEPARATOR = '/';
    public static final char CLASS_SEPARATOR = '$';

    /** Item: Java unknown */
    public static final String CATEGORY_UNKNOWN = "Java.Unknown";
    /** Item: Java class */
    public static final String CATEGORY_CLASS = "Java.Class";
    /** Item: Java interface */
    public static final String CATEGORY_INTERFACE = "Java.Interface";
    /** Item: Java enum */
    public static final String CATEGORY_ENUM = "Java.Enum";
    /** Group: Java class file */
    public static final String CATEGORY_CLASS_FILE = "Java.ClassFile";
    /** Group: Java jar file */
    public static final String CATEGORY_JAR = "Java.Archive";
    /** Group: Java folder (of a class file) */
    public static final String CATEGORY_FOLDER = "Java.Folder";
    /** Package: Java package */
    public static final String CATEGORY_PACKAGE = "Java.Package";

    public static final String FEATURE_ABSTRACT = "ABSTRACT";
    public static final String FEATURE_PUBLIC = "PUBLIC";
    public static final String FEATURE_PROTECTED = "PROTECTED";
    public static final String FEATURE_PRIVATE = "PRIVATE";
    public static final String FEATURE_PACKAGE = "PACKAGE";
    public static final String FEATURE_STATIC = "STATIC";
    public static final String FEATURE_FINAL = "FINAL";
    public static final String FEATURE_NESTED = "NESTED";
    public static final String FEATURE_ANONYMOUS = "ANONYMOUS";
    public static final String FEATURE_DEPRECATED = "DEPRECATED";
    public static final String FEATURE_GENERIC = "GENERIC";

    private static final Predicate<DElement> IS_ANONYMOUS_CLASS =
            value -> value.getKind() == DElementKind.ITEM && value.isEnabled(FEATURE_ANONYMOUS);
    private static final Predicate<DElement> IS_HIDDEN_TYPE =
            value -> value.getKind() == DElementKind.ITEM && (value.isEnabled(FEATURE_ANONYMOUS)
                    || value.isEnabled(FEATURE_NESTED) && value.isEnabled(FEATURE_PRIVATE));
    private static final Predicate<DElement> IS_TYPE =
            value -> value.getKind() == DElementKind.ITEM;

    public DJavaAnalysis() {
        super();
    }

    public DGroup findOrCreateGroup(DGroup refGroup,
                                    File relativePath) {
        LOGGER.trace("createOrFindGroup({}, {})", refGroup, relativePath);
        final File relativeParent = relativePath.getParentFile();
        final DGroup parentGroup;
        if (relativeParent == null) {
            parentGroup = refGroup;
        } else {
            parentGroup = findOrCreateGroup(refGroup, relativeParent);
        }
        final File tmp = new File(parentGroup.getName(), relativePath.getName());
        return findOrCreateGroup(Files.normalize(tmp.getPath()), parentGroup);
    }

    public DPackage findOrCreatePackage(String name,
                                        boolean hasClass) {
        LOGGER.trace("createOrFindPackage({}, {})", name, hasClass);
        if (hasClass) {
            final int pos = name.lastIndexOf(PACKAGE_SEPARATOR);
            if (pos >= 0) {
                return findOrCreatePackage(name.substring(0, pos));
            } else {
                return findOrCreatePackage("");
            }
        } else {
            return findOrCreatePackage(name);
        }
    }

    public DItem findOrCreateItem(String name,
                                  DGroup group) {
        LOGGER.trace("createOrFindItem({}, {})", name, group);
        DItem result = getItem(name);
        if (result == null) {
            final int pos = name.lastIndexOf(CLASS_SEPARATOR);
            if (pos >= 0) {
                // Nested item
                final DItem parent = findOrCreateItem(name.substring(0, pos), null);
                result = createItem(name, group, parent);
                result.setEnabled(DJavaAnalysis.FEATURE_NESTED, true);
                result.setCategory(CATEGORY_UNKNOWN);
                if (parent.getScope() == DElementScope.INTERNAL) {
                    result.setScope(DElementScope.MIXED);
                } else {
                    result.setScope(parent.getScope());
                }
            } else {
                final DPackage parent = findOrCreatePackage(name, true);
                result = createItem(name, group, parent);
                result.setCategory(CATEGORY_UNKNOWN);
                result.setScope(DElementScope.EXTERNAL);
            }
        } else if (group != null) {
            if (result.getOwner() == null) {
                result.setGroup(group);
            } else {
                LOGGER.error("{} is already attached to group {}", result, result.getOwner());
                LOGGER.error("   Can not set it to {}", group);
                LOGGER.error("   Do not specify the same group twice.");
            }
        }
        return result;
    }

    private DPackage findOrCreatePackage(String name) {
        LOGGER.trace("createOrFindPackage({})", name);
        DPackage result = getPackage(name);
        if (result == null) {
            final int pos = name.lastIndexOf(PACKAGE_SEPARATOR);
            if (pos >= 0) {
                final DPackage parent = findOrCreatePackage(name.substring(0, pos));
                result = createPackage(name, null, parent);
            } else {
                result = createPackage(name, null, null);
            }
            result.setCategory(CATEGORY_PACKAGE);
        }
        return result;
    }

    public void fixGroupCategory() {
        for (final DElement element : getElements()) {
            if (element.getKind() == DElementKind.GROUP) {
                final DGroup group = (DGroup) element;
                if (group.getCategory() == null) {
                    group.setCategory(CATEGORY_FOLDER);
                }
            }
        }
    }

    public final void addAnonymousFeature() {
        for (final DElement element : getElements()) {
            if (element.getKind() == DElementKind.ITEM) {
                final DItem type = (DItem) element;
                if (isAnonymousClass(type.getName())) {
                    type.setEnabled(FEATURE_ANONYMOUS, true);
                }
            }
        }
    }

    public final void addNestedFeature() {
        for (final DElement element : getElements()) {
            if (element.getKind() == DElementKind.ITEM) {
                final DItem type = (DItem) element;
                if (isNestedClass(type.getName())) {
                    type.setEnabled(FEATURE_NESTED, true);
                }
            }
        }
    }

    public final void collapseAnonymousClasses() {
        LOGGER.info("collapseAnonymousClasses()");
        collapse(IS_ANONYMOUS_CLASS);
    }

    public final void collapseHiddenTypes() {
        LOGGER.info("collapseHiddenTypes()");
        collapse(IS_HIDDEN_TYPE);
    }

    public final void collapseTypes() {
        LOGGER.info("collapseTypes()");
        collapse(IS_TYPE);
    }

    public static boolean isStandardJavaName(String name) {
        return name.startsWith("java") || name.startsWith("sun");
    }

    public static boolean isPackageInfo(String name) {
        return name.endsWith("package-info");
    }

    public static String getParent(String name) {
        int pos = name.lastIndexOf(CLASS_SEPARATOR);
        if (pos >= 0) {
            return name.substring(0, pos);
        } else {
            pos = name.lastIndexOf(PACKAGE_SEPARATOR);
            if (pos >= 0) {
                return name.substring(0, pos);
            } else {
                return "";
            }
        }
    }

    /**
     * Returns the package part of a qualified name.
     * <p>
     * If name is {@code "n1"}, returns {@code ""}.<br>
     * If name is {@code "n1/n2"}, returns {@code "n1"}.<br>
     * If name is {@code "n1/n2/n3"}, returns {@code "n1/n2"}.<br>
     * If name is {@code "n1/n2/n3/n4$n5"}, returns {@code "n1/n2/n3"}.<br>
     *
     * @param name The name.
     * @return The package part of {@code name}.
     */
    public static String getPackagePart(String name) {
        final int pos = name.lastIndexOf(PACKAGE_SEPARATOR);
        if (pos >= 0) {
            return name.substring(0, pos);
        } else {
            return "";
        }
    }

    /**
     * Returns the class part of a qualified name.
     * <p>
     * If name is {@code "n1"}, returns {@code "n1"}.<br>
     * If name is {@code "n1/n2"}, returns {@code "n2"}.<br>
     * If name is {@code "n1/n2/n3"}, returns {@code "n3"}.<br>
     * If name is {@code "n1/n2/n3/n4$n5"}, returns {@code "n4$n5"}.<br>
     *
     * @param name The name.
     * @return The class part of {@code name}.
     */
    public static String getClassPart(String name) {
        final int pos = name.lastIndexOf(PACKAGE_SEPARATOR);
        if (pos >= 0) {
            return name.substring(pos + 1);
        } else {
            return name;
        }
    }

    /**
     * Returns the name of the most nested class.
     * <p>
     * If name is {@code "n1"}, returns {@code "n1"}.<br>
     * If name is {@code "n1/n2"}, returns {@code "n2"}.<br>
     * If name is {@code "n1/n2/n3"}, returns {@code "n3"}.<br>
     * If name is {@code "n1/n2/n3/n4$n5"}, returns {@code "n5"}.<br>
     *
     * @param name The name.
     * @return The name of the most nested class in {@code name}.
     */
    public static String getLastClass(String name) {
        final int pos = name.lastIndexOf(CLASS_SEPARATOR);
        if (pos >= 0) {
            return name.substring(pos + 1);
        } else {
            return getClassPart(name);
        }
    }

    /**
     * Returns {@code true} when a name is an anonymous class name.
     * <p>
     * A class is anonymous when its local name is an integer.
     *
     * @param name The name.
     * @return {@code true} when {@code name} is an anonymous class name.
     */
    public static boolean isAnonymousClass(String name) {
        final String last = getLastClass(name);
        return StringRecognizer.isInt(last);
    }

    public static boolean isNamedClass(String name) {
        return !isAnonymousClass(name);
    }

    /**
     * Returns {@code true} when a name is the name of an nested class.
     * <p>
     * This is the case when the name contains {@link #CLASS_SEPARATOR}.
     *
     * @param name The name.
     * @return {@code true} when {@code name} is the name of an nested class.
     */
    public static boolean isNestedClass(String name) {
        final int pos = name.lastIndexOf(CLASS_SEPARATOR);
        return pos >= 0;
    }

    public static String getOwningNamedClass(String name) {
        String tmp = name;
        while (isAnonymousClass(tmp)) {
            tmp = getParent(tmp);
        }
        return tmp;
    }
}