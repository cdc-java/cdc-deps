package cdc.deps.java.asm;

import java.util.Arrays;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.ModuleVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.signature.SignatureReader;
import org.objectweb.asm.signature.SignatureVisitor;

import cdc.deps.DElementScope;
import cdc.deps.DGroup;
import cdc.deps.DItem;
import cdc.deps.java.DJavaAnalysis;
import cdc.deps.java.DJavaAnalyzer.Feature;
import cdc.util.debug.CallStack;
import cdc.util.lang.Procedure;

/**
 * ClassVisitor dedicated to dependency analysis. This implementation is based
 * on ASM example written by Eugene Kuleshov.
 *
 * @author Damien Carbonne
 * @author Eugene Kuleshov
 *
 */
class AsmDependencyVisitor extends ClassVisitor {
    protected static final Logger LOGGER = LogManager.getLogger(AsmDependencyVisitor.class);

    /**
     * ASM version to use in this class.
     */
    private static final int ASM_VERSION = Opcodes.ASM9;

    protected static final SignatureVisitor VOID_SIGNATURE_VISITOR = new SignatureVisitor(ASM_VERSION) {
        // Empty
    };

    protected final AsmJavaAnalyzer analyzer;
    private final DGroup group;
    private DItem type;

    public AsmDependencyVisitor(AsmJavaAnalyzer analyzer,
                                DGroup group) {
        super(ASM_VERSION);
        this.analyzer = analyzer;
        this.group = group;
    }

    public AsmDependencyVisitor(AsmJavaAnalyzer analyzer,
                                DGroup group,
                                ClassVisitor v) {
        super(ASM_VERSION, v);
        this.analyzer = analyzer;
        this.group = group;
    }

    private static boolean isSet(int mask,
                                 int code) {
        return (mask & code) != 0;
    }

    private static void addAccess(StringBuilder builder,
                                  int access,
                                  int opcode,
                                  String s) {
        if (isSet(access, opcode)) {
            builder.append(" ");
            builder.append(s);
        }
    }

    private enum AccessContext {
        CLASS,
        FIELD,
        INNER,
        METHOD,
        PARAMETER,
        MODULE
    }

    private void wrap(String message,
                      Procedure procedure) {
        try {
            procedure.invoke();
        } catch (final RuntimeException e) {
            LOGGER.error("{}", group);
            LOGGER.error("   exception: {}", e.getMessage());
            LOGGER.error("   context: {}", message);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.catching(e);
            }
        }
    }

    private static String accessToString(int access,
                                         AccessContext context) {
        final StringBuilder builder = new StringBuilder();
        builder.append("0x" + Integer.toHexString(access));
        builder.append(" [");
        builder.append(context);
        builder.append("]");

        // class, field, method
        addAccess(builder, access, Opcodes.ACC_PUBLIC, "public");
        addAccess(builder, access, Opcodes.ACC_PRIVATE, "private");
        addAccess(builder, access, Opcodes.ACC_PROTECTED, "protected");

        // field, method
        addAccess(builder, access, Opcodes.ACC_STATIC, "static");

        // class, field, method, parameter
        addAccess(builder, access, Opcodes.ACC_FINAL, "final");

        if (context == AccessContext.CLASS) {
            // class
            addAccess(builder, access, Opcodes.ACC_SUPER, "super");
        } else if (context == AccessContext.METHOD) {
            // method
            addAccess(builder, access, Opcodes.ACC_SYNCHRONIZED, "synchronized");
        } else if (context == AccessContext.MODULE) {
            // module
            addAccess(builder, access, Opcodes.ACC_OPEN, "open");
        } else {
            // module requires
            addAccess(builder, access, Opcodes.ACC_TRANSITIVE, "transitive");
        }

        if (context == AccessContext.FIELD) {
            // field
            addAccess(builder, access, Opcodes.ACC_VOLATILE, "volatile");
        } else if (context == AccessContext.METHOD) {
            // method
            addAccess(builder, access, Opcodes.ACC_BRIDGE, "bridge");
        } else {
            // module requires
            addAccess(builder, access, Opcodes.ACC_STATIC_PHASE, "static-phase");
        }

        if (context == AccessContext.METHOD) {
            // method
            addAccess(builder, access, Opcodes.ACC_VARARGS, "varargs");
        } else {
            // field
            addAccess(builder, access, Opcodes.ACC_TRANSIENT, "transient");
        }

        // method
        addAccess(builder, access, Opcodes.ACC_NATIVE, "native");
        // class
        addAccess(builder, access, Opcodes.ACC_INTERFACE, "interface");
        // class, method
        addAccess(builder, access, Opcodes.ACC_ABSTRACT, "abstract");
        // method
        addAccess(builder, access, Opcodes.ACC_STRICT, "strict");
        // class, field, method, parameter, module *
        addAccess(builder, access, Opcodes.ACC_SYNTHETIC, "synthetic");
        // class
        addAccess(builder, access, Opcodes.ACC_ANNOTATION, "annotation");
        // class(?) field inner
        addAccess(builder, access, Opcodes.ACC_ENUM, "enum");

        if (context == AccessContext.CLASS) {
            // class
            addAccess(builder, access, Opcodes.ACC_MODULE, "module");
        } else {
            // parameter, module, module *
            addAccess(builder, access, Opcodes.ACC_MANDATED, "mandated");
        }

        // class, field, method
        addAccess(builder, access, Opcodes.ACC_DEPRECATED, "deprecated");
        return builder.toString();
    }

    boolean debugEnabled() {
        return analyzer.isEnabled(Feature.DEBUG);
    }

    void enter(String message) {
        if (debugEnabled()) {
            CallStack.enter(Level.DEBUG, message);
        }
    }

    void leave() {
        if (debugEnabled()) {
            CallStack.leave();
        }
    }

    @Override
    public void visit(int version,
                      int access,
                      String name,
                      String signature,
                      String superName,
                      String[] interfaces) {
        if (debugEnabled()) {
            enter("ASM.visit(version:" + version + ", access:" + accessToString(access, AccessContext.CLASS) + ", name:" + name
                    + ", sig:" + signature + ", super:" + superName + ", inter:" + Arrays.toString(interfaces) + ")");
        }
        this.type = createOrFindType(name, group);
        if (this.type != null) {
            this.type.setScope(DElementScope.INTERNAL);
            if (isSet(access, Opcodes.ACC_INTERFACE)) {
                type.setCategory(DJavaAnalysis.CATEGORY_INTERFACE);
            } else if (isSet(access, Opcodes.ACC_ENUM)) {
                type.setCategory(DJavaAnalysis.CATEGORY_ENUM);
            } else {
                type.setCategory(DJavaAnalysis.CATEGORY_CLASS);
            }

            if (!DJavaAnalysis.isNestedClass(name)) {
                analyseAccess(type, access, AccessContext.CLASS);
            }

            if (signature != null && isSignatureOfGenericClass(signature)) {
                type.setEnabled(DJavaAnalysis.FEATURE_GENERIC, true);
            }

            parseInternalName(superName);
            parseInternalNames(interfaces);
            parseSignature(signature);
        }
        leave();
    }

    @Override
    public void visitSource(String source,
                            String debug) {
        if (debugEnabled()) {
            enter("ASM.visitSource(source:" + source + ", debug:" + debug + ")");
            leave();
        }
    }

    // >= ASM6
    @Override
    public ModuleVisitor visitModule(String name,
                                     int access,
                                     String version) {
        LOGGER.warn("ASM.visitModule() NYI");
        // TODO
        return super.visitModule(name, access, version);
    }

    // >= ASM7
    @Override
    public void visitNestHost(String nestHost) {
        LOGGER.warn("ASM.visitNestHost() NYI");
        // TODO
        super.visitNestHost(nestHost);
    }

    // >= ASM7
    @Override
    public void visitNestMember(String nestMember) {
        LOGGER.warn("ASM.visitNestMember() NYI");
        // TODO
        super.visitNestMember(nestMember);
    }

    @Override
    public void visitOuterClass(String owner,
                                String name,
                                String desc) {
        if (debugEnabled()) {
            enter("ASM.visitOuterClass(owner:" + owner + ", name:" + name + ", desc:" + desc + ")");
            leave();
        }
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc,
                                             boolean visible) {
        if (debugEnabled()) {
            enter("ASM.visitAnnotation(desc:" + desc + "visible:" + visible + ")");
        }
        try {
            if (analyzer.isEnabled(Feature.IGNORE_ANNOTATIONS)) {
                return null;
            } else {
                parseDesc(desc);
                return new AnnotationDependencyVisitor();
            }
        } finally {
            leave();
        }
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef,
                                                 TypePath typePath,
                                                 String desc,
                                                 boolean visible) {
        LOGGER.warn("ASM.visitTypeAnnotation() NYI");
        // TODO
        return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
    }

    @Override
    public void visitAttribute(Attribute attr) {
        if (debugEnabled()) {
            enter("ASM.visitAttribute(attrs:" + attr + ")");
            leave();
        }
    }

    @Override
    public void visitInnerClass(String name,
                                String outerName,
                                String innerName,
                                int access) {
        if (debugEnabled()) {
            enter("ASM.visitInnerClass(name:" + name + ", outer:" + outerName + ", inner:" + innerName
                    + ", access:" + accessToString(access, AccessContext.INNER) + ")");
        }
        final DItem inner = createOrFindType(name, null);
        analyseAccess(inner, access, AccessContext.INNER);

        leave();
    }

    @Override
    public FieldVisitor visitField(final int access,
                                   final String name,
                                   final String desc,
                                   final String signature,
                                   final Object value) {
        if (debugEnabled()) {
            enter("ASM.visitField(access:" + accessToString(access, AccessContext.FIELD) + ", name:" + name + ", desc:" + desc
                    + ", sig:" + signature + ", value:" + value + ")");
        }
        try {
            if (signature == null) {
                parseDesc(desc);
            } else {
                parseSignature(signature);
            }
            if (value instanceof Type) {
                parseType((Type) value);
            }
            return new FieldDependencyVisitor();
        } finally {
            leave();
        }
    }

    @Override
    public MethodVisitor visitMethod(int access,
                                     String name,
                                     String desc,
                                     String signature,
                                     String[] exceptions) {
        if (debugEnabled()) {
            enter("ASM.visitMethod(access:" + accessToString(access, AccessContext.METHOD) + ", name:" + name + ", desc:" + desc
                    + ", sig:" + signature + ", exceptions:" + Arrays.toString(exceptions) + ")");
        }
        try {
            if (signature == null) {
                parseMethodDesc(desc);
            } else {
                parseSignature(signature);
            }
            parseInternalNames(exceptions);
            return new MethodDependencyVisitor();
        } finally {
            leave();
        }
    }

    @Override
    public void visitEnd() {
        if (debugEnabled()) {
            enter("ASM.visitEnd()");
            leave();
        }
    }

    private void analyseAccess(DItem type,
                               int access,
                               AccessContext context) {
        if (debugEnabled()) {
            enter("analyseAccess(" + type + ", " + accessToString(access, context) + ")");
        }
        if (type != null) {
            if (isSet(access, Opcodes.ACC_PUBLIC)) {
                type.setEnabled(DJavaAnalysis.FEATURE_PUBLIC, true);
                type.setEnabled(DJavaAnalysis.FEATURE_PACKAGE, false);
            } else if (isSet(access, Opcodes.ACC_PROTECTED)) {
                type.setEnabled(DJavaAnalysis.FEATURE_PROTECTED, true);
                type.setEnabled(DJavaAnalysis.FEATURE_PACKAGE, false);
            } else if (isSet(access, Opcodes.ACC_PRIVATE)) {
                type.setEnabled(DJavaAnalysis.FEATURE_PRIVATE, true);
                type.setEnabled(DJavaAnalysis.FEATURE_PACKAGE, false);
            } else {
                type.setEnabled(DJavaAnalysis.FEATURE_PACKAGE, true);
            }

            if (isSet(access, Opcodes.ACC_ABSTRACT)) {
                type.setEnabled(DJavaAnalysis.FEATURE_ABSTRACT, true);
            }
            if (isSet(access, Opcodes.ACC_STATIC)) {
                type.setEnabled(DJavaAnalysis.FEATURE_STATIC, true);
            }
            if (isSet(access, Opcodes.ACC_FINAL)) {
                type.setEnabled(DJavaAnalysis.FEATURE_FINAL, true);
            }
            if (isSet(access, Opcodes.ACC_DEPRECATED)) {
                type.setEnabled(DJavaAnalysis.FEATURE_DEPRECATED, true);
            }

            if (context == AccessContext.INNER) {
                if (isSet(access, Opcodes.ACC_INTERFACE)) {
                    type.setCategory(DJavaAnalysis.CATEGORY_INTERFACE);
                } else if (isSet(access, Opcodes.ACC_ENUM)) {
                    type.setCategory(DJavaAnalysis.CATEGORY_ENUM);
                } else {
                    type.setCategory(DJavaAnalysis.CATEGORY_CLASS);
                }
            }
        }
        leave();
    }

    private DItem createOrFindType(String typename,
                                   DGroup group) {
        if (debugEnabled()) {
            enter("createOrFindType(" + typename + ", " + group + ")");
        }
        try {
            if ((analyzer.isEnabled(Feature.IGNORE_JDK_TYPES) && DJavaAnalysis.isStandardJavaName(typename))
                    || (analyzer.isEnabled(Feature.IGNORE_PACKAGE_INFO) && DJavaAnalysis.isPackageInfo(typename))) {
                return null;
            } else {
                return analyzer.getAnalysis().findOrCreateItem(typename, group);
            }
        } finally {
            leave();
        }
    }

    /**
     * Creates a dependency between current type and a target type.
     * <p>
     * Target Java type is created if necessary.
     *
     * @param typename The Java type name.
     */
    private void createDependency(String typename) {
        if (debugEnabled()) {
            enter("createDependency(" + typename + ")");
        }
        if (typename != null) {
            final DItem depType = createOrFindType(typename, null);
            if (depType != null) {
                enter("addDependency(" + type + ", " + depType + ")");
                analyzer.getAnalysis().addPrimitiveDependency(type, depType);
                leave();
            }
        }
        leave();
    }

    void parseDesc(String desc) {
        if (debugEnabled()) {
            enter("parseDesc(desc:" + desc + ")");
        }
        parseType(Type.getType(desc));
        leave();
    }

    /**
     * Parse an internal name.
     * <p>
     * The name is converted to Java type name, then a corresponding DItem is created if necessary.
     * Finally, a dependency to that type is added.
     *
     * @param name The internal name.
     */
    void parseInternalName(String name) {
        if (debugEnabled()) {
            enter("parseInternalName(name:" + name + ")");
        }
        parseType(Type.getObjectType(name));
        leave();
    }

    private void parseInternalNames(final String[] names) {
        if (debugEnabled()) {
            enter("parseInternalNames(names:" + Arrays.toString(names) + ")");
        }
        if (names != null) {
            for (int i = 0; i < names.length; i++) {
                parseInternalName(names[i]);
            }
        }
        leave();
    }

    void parseType(Type type) {
        if (debugEnabled()) {
            enter("parseType(type:" + type + ")");
        }
        switch (type.getSort()) {
        case Type.ARRAY:
            parseType(type.getElementType());
            break;
        case Type.OBJECT:
            createDependency(type.getInternalName());
            break;
        case Type.METHOD:
            parseMethodDesc(type.getDescriptor());
            break;
        default:
            // Ignore
            break;
        }
        leave();
    }

    void parseMethodDesc(String desc) {
        final String context = "parseMethodDesc(desc:" + desc + ")";
        if (debugEnabled()) {
            enter("parseMethodDesc(desc:" + desc + ")");
        }
        // FIXME This throws exception in some cases
        wrap(context + " Type.getReturnType(desc)",
             () -> parseType(Type.getReturnType(desc)));
        wrap(context + " Type.getArgumentTypes(desc)",
             () -> {
                 final Type[] types = Type.getArgumentTypes(desc);
                 for (final Type type : types) {
                     parseType(type);
                 }
             });
        leave();
    }

    /**
     * Parse a generic signature.
     *
     * @param signature The optional signature.
     */
    void parseSignature(String signature) {
        if (debugEnabled()) {
            enter("parseSignature(signature:" + signature + ")");
        }
        try {
            if (signature != null) {
                new SignatureReader(signature).accept(new SignatureDependencyVisitor());
            }
        } finally {
            leave();
        }
    }

    boolean isSignatureOfGenericClass(String signature) {
        if (debugEnabled()) {
            enter("isSignatureOfGenericClass(signature:" + signature + ")");
        }
        try {
            if (signature == null) {
                return false;
            } else {
                final SignatureAnalyzerVisitor visitor = new SignatureAnalyzerVisitor();
                new SignatureReader(signature).accept(visitor);
                return visitor.foundGenericClass();
            }
        } finally {
            leave();
        }
    }

    void parseConstant(Object constant) {
        if (debugEnabled()) {
            enter("parseConstant(constant:" + constant + ")");
        }
        if (constant instanceof Type) {
            parseType((Type) constant);
        } else if (constant instanceof Handle) {
            final Handle handle = (Handle) constant;
            parseInternalName(handle.getOwner());
            parseMethodDesc(handle.getDesc());
        }
        leave();
    }

    /**
     * Annotation visitor.
     *
     * @author Damien Carbonne
     */
    private class AnnotationDependencyVisitor extends AnnotationVisitor {
        public AnnotationDependencyVisitor() {
            super(ASM_VERSION);
        }

        private String getContext() {
            return getClass().getSimpleName();
        }

        @Override
        public void visit(final String name,
                          final Object value) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visit(name:" + name + ", value:" + value + ")");
            }
            if (value instanceof Type) {
                parseType((Type) value);
            }
            leave();
        }

        @Override
        public void visitEnum(final String name,
                              final String desc,
                              final String value) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitEnum(name:" + name + ", desc:" + desc + ", value:" + value + ")");
            }
            parseDesc(desc);
            leave();
        }

        @Override
        public AnnotationVisitor visitAnnotation(final String name,
                                                 final String desc) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitAnnotation(name:" + name + ", desc:" + desc + ")");
            }
            try {
                parseDesc(desc);
                return this;
            } finally {
                leave();
            }
        }

        @Override
        public AnnotationVisitor visitArray(final String name) {
            return this;
        }

        @Override
        public void visitEnd() {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitEnd()");
                leave();
            }
        }
    }

    /**
     * Field visitor.
     *
     * @author Damien Carbonne
     */
    private class FieldDependencyVisitor extends FieldVisitor {
        public FieldDependencyVisitor() {
            super(ASM_VERSION);
        }

        private String getContext() {
            return getClass().getSimpleName();
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc,
                                                 boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitAnnotation(desc:" + desc + ", visible:" + visible + ")");
            }
            try {
                parseDesc(desc);
                return new AnnotationDependencyVisitor();
            } finally {
                leave();
            }
        }
    }

    /**
     * Method visitor.
     *
     * @author Damien Carbonne
     */
    private class MethodDependencyVisitor extends MethodVisitor {
        public MethodDependencyVisitor() {
            super(ASM_VERSION);
        }

        private String getContext() {
            return getClass().getSimpleName();
        }

        @Override
        public AnnotationVisitor visitAnnotationDefault() {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitAnnotationDefault()");
            }
            try {
                return new AnnotationDependencyVisitor();
            } finally {
                leave();
            }
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc,
                                                 boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitAnnotation(desc:" + desc + ", visible:" + visible + ")");
            }
            try {
                parseDesc(desc);
                return new AnnotationDependencyVisitor();
            } finally {
                leave();
            }
        }

        @Override
        public AnnotationVisitor visitParameterAnnotation(int parameter,
                                                          String desc,
                                                          boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitParameterAnnotation(parameter:" + parameter + ", desc:" + desc
                        + ", visible:" + visible + ")");
            }
            try {
                parseDesc(desc);
                return new AnnotationDependencyVisitor();
            } finally {
                leave();
            }
        }

        @Override
        public void visitTypeInsn(int opcode,
                                  String type) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitTypeInsn(opcode:" + opcode + ", type:" + type + ")");
            }
            parseType(Type.getObjectType(type));
            leave();
        }

        @Override
        public AnnotationVisitor visitTypeAnnotation(int typeRef,
                                                     TypePath typePath,
                                                     String desc,
                                                     boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitTypeAnnotation(..., desc:" + desc + ", ...)");
            }
            try {
                if (analyzer.isEnabled(Feature.IGNORE_ANNOTATIONS)) {
                    return null;
                } else {
                    parseDesc(desc);
                    return new AnnotationDependencyVisitor();
                }
            } finally {
                leave();
            }
        }

        @Override
        public void visitFieldInsn(int opcode,
                                   String owner,
                                   String name,
                                   String desc) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitFieldInsn(opcode:" + opcode + ", ...)");
            }
            parseInternalName(owner);
            parseDesc(desc);
            leave();
        }

        // >= ASM5
        @Override
        public void visitMethodInsn(int opcode,
                                    String owner,
                                    String name,
                                    String desc,
                                    boolean itf) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitMethodInsn(opcode:" + opcode + ", ...)");
            }
            parseInternalName(owner);
            parseMethodDesc(desc);
            leave();
        }

        // ASM4
        @Override
        @Deprecated
        public void visitMethodInsn(int opcode,
                                    String owner,
                                    String name,
                                    String desc) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitMethodInsn(opcode:" + opcode + ", ...)");
            }
            parseInternalName(owner);
            parseMethodDesc(desc);
            leave();
        }

        @Override
        public void visitInvokeDynamicInsn(String name,
                                           String desc,
                                           Handle bsm,
                                           Object... bsmArgs) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitInvokeDynamicInsn(name:" + name + ", ...)");
            }
            parseMethodDesc(desc);
            parseConstant(bsm);
            for (final Object bsmArg : bsmArgs) {
                parseConstant(bsmArg);
            }
            leave();
        }

        @Override
        public void visitLdcInsn(Object constant) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitLdcInsn(constant:" + constant + ")");
            }
            parseConstant(constant);
            leave();
        }

        @Override
        public void visitMultiANewArrayInsn(String desc,
                                            int dims) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitMultiANewArrayInsn(desc:" + desc + ", dims:" + dims + ")");
            }
            parseDesc(desc);
            leave();
        }

        @Override
        public AnnotationVisitor visitInsnAnnotation(int typeRef,
                                                     TypePath typePath,
                                                     String desc,
                                                     boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitInsnAnnotation(..., desc:" + desc + ", ...)");
            }
            try {
                if (analyzer.isEnabled(Feature.IGNORE_ANNOTATIONS)) {
                    return null;
                } else {
                    parseDesc(desc);
                    return new AnnotationDependencyVisitor();
                }
            } finally {
                leave();
            }
        }

        @Override
        public void visitLocalVariable(String name,
                                       String desc,
                                       String signature,
                                       Label start,
                                       Label end,
                                       int index) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitLocalVariable(name:" + name + ", desc:" + desc + ", ...)");
            }
            parseSignature(signature);
            leave();
        }

        @Override
        public AnnotationVisitor visitLocalVariableAnnotation(int typeRef,
                                                              TypePath typePath,
                                                              Label[] start,
                                                              Label[] end,
                                                              int[] index,
                                                              String desc,
                                                              boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitLocalVariableAnnotation(..., desc:" + desc + ", ...)");
            }
            try {
                if (analyzer.isEnabled(Feature.IGNORE_ANNOTATIONS)) {
                    return null;
                } else {
                    parseDesc(desc);
                    return new AnnotationDependencyVisitor();
                }
            } finally {
                leave();
            }
        }

        @Override
        public void visitTryCatchBlock(Label start,
                                       Label end,
                                       Label handler,
                                       String type) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitTryCatchBlock(...)");
            }
            if (type != null) {
                parseInternalName(type);
            }
            leave();
        }

        @Override
        public AnnotationVisitor visitTryCatchAnnotation(int typeRef,
                                                         TypePath typePath,
                                                         String desc,
                                                         boolean visible) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitTryCatchAnnotation(..., desc:" + desc + ", ...)");
            }
            try {
                if (analyzer.isEnabled(Feature.IGNORE_ANNOTATIONS)) {
                    return null;
                } else {
                    parseDesc(desc);
                    return new AnnotationDependencyVisitor();
                }
            } finally {
                leave();
            }
        }
    }

    /**
     * Generic signature visitor.
     * <p>
     * ClassSignature = ( visitFormalTypeParameter visitClassBound? visitInterfaceBound* )* ( visitSuperclass visitInterface* )<br>
     * MethodSignature = ( visitFormalTypeParameter visitClassBound? visitInterfaceBound* )* ( visitParameterType* visitReturnType
     * visitExceptionType* )<br>
     * TypeSignature = visitBaseType | visitTypeVariable | visitArrayType | ( visitClassType visitTypeArgument* (
     * visitInnerClassType visitTypeArgument* )* visitEnd ) )
     * <p>
     * Systematically forking is probably not necessary. But not doing this raises issues in some corner cases.
     *
     * @author Damien Carbonne
     */
    private class SignatureDependencyVisitor extends SignatureVisitor {
        private String signatureClassName;

        public SignatureDependencyVisitor() {
            super(ASM_VERSION);
        }

        private String getContext() {
            return getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode(this));
        }

        private SignatureVisitor fork(String name) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".fork(" + name + ")");
            }
            try {
                return new SignatureDependencyVisitor();
            } finally {
                leave();
            }
        }

        @Override
        public SignatureVisitor visitClassBound() {
            return fork("visitClassBound()");
        }

        @Override
        public SignatureVisitor visitInterfaceBound() {
            return fork("visitInterfaceBound()");
        }

        @Override
        public SignatureVisitor visitSuperclass() {
            return fork("visitSuperclass()");
        }

        @Override
        public SignatureVisitor visitInterface() {
            return fork("visitInterface()");
        }

        @Override
        public SignatureVisitor visitParameterType() {
            return fork("visitParameterType()");
        }

        @Override
        public SignatureVisitor visitReturnType() {
            return fork("visitReturnType()");
        }

        @Override
        public SignatureVisitor visitExceptionType() {
            return fork("visitExceptionType()");
        }

        @Override
        public SignatureVisitor visitArrayType() {
            return fork("visitArrayType()");
        }

        @Override
        public void visitClassType(String name) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitClassType(name:" + name + ")");
            }
            signatureClassName = name;
            parseInternalName(name);
            leave();
        }

        @Override
        public SignatureVisitor visitTypeArgument(char wildcard) {
            return fork("visitTypeArgument()");
        }

        @Override
        public void visitInnerClassType(String name) {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitInnerClassType(name:" + name + ")");
            }
            parseInternalName(signatureClassName + "$" + name);
            leave();
        }

        @Override
        public void visitEnd() {
            if (debugEnabled()) {
                enter("ASM." + getContext() + ".visitEnd()");
            }
            signatureClassName = null;
            leave();
        }
    }

    private class SignatureAnalyzerVisitor extends SignatureVisitor {
        private boolean hasFormalParameters = false;

        public SignatureAnalyzerVisitor() {
            super(ASM_VERSION);
        }

        public boolean foundGenericClass() {
            return hasFormalParameters;
        }

        @Override
        public void visitFormalTypeParameter(String name) {
            hasFormalParameters = true;
        }

        @Override
        public SignatureVisitor visitClassBound() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitInterfaceBound() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitSuperclass() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitInterface() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitParameterType() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitReturnType() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitExceptionType() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitArrayType() {
            return VOID_SIGNATURE_VISITOR;
        }

        @Override
        public SignatureVisitor visitTypeArgument(char wildcard) {
            return VOID_SIGNATURE_VISITOR;
        }
    }
}