package cdc.deps.java.asm;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.util.TraceClassVisitor;

import cdc.deps.DGroup;
import cdc.deps.java.DJavaAnalyzer;

/**
 * Implementation of Java analyzer based on ASM library.
 *
 * @author Damien Carbonne
 *
 */
public class AsmJavaAnalyzer extends DJavaAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(AsmJavaAnalyzer.class);

    public AsmJavaAnalyzer() {
        super();
        getAnalysis().getInfo().setCreationDate(new Date());
        getAnalysis().getInfo().setAnalyzerName(AsmJavaAnalyzer.class.getCanonicalName());
    }

    @Override
    protected void analyzeClassFile(DGroup group,
                                    InputStream is) {
        try {
            final AsmDependencyVisitor v = new AsmDependencyVisitor(this, group, null);
            if (isEnabled(Feature.DEBUG)) {
                final PrintStream out = IoBuilder.forLogger(LOGGER)
                                                 .setLevel(Level.DEBUG)
                                                 .buildPrintStream();
                final PrintWriter pw = new PrintWriter(out);
                final TraceClassVisitor tcv = new TraceClassVisitor(v, pw);
                LOGGER.debug("------------------------------------------------------------------");
                LOGGER.debug("analyzeClassFile({})", group);
                new ClassReader(is).accept(tcv, 0);
            } else {
                new ClassReader(is).accept(v, 0);
            }
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
    }
}