Utilities for analysis and display of dependencies.

It contains 3 modules:
- **Core module**, used to describe and display a dependency analysis
- **Java module**, that can build a dependency analysis from Java byte code
- **Vba module**, that can build a dependency analysis from Vba data- 

# Core module
## Dependency Analysis
A dependency analysis is built using several concepts:
- **Items** and **Packages**, representing *logical* units / artifacts,
- **Groups**, representing *physical* units / artifacts,
- **Dependencies**, representing any dependency between above artifacts.

They were initialy created to support anaysis of Java dependencies.  
Vba analysis was added in a second phase.  

Other languages such as C++ or Ada were also taken into account, even if no
module was created for them.

An artifact has several properties that allow a more detailed description:
- a **Category** used to refine the artifact classification.
- any number of **Features**.

## Graph generation
Several graphs can be generated from a analysis.  
The are converted to images using GraphViz.

## HTML export
An analysis can be exported to HTML.  
This HTML can include graph images.  
One can either generate one file, or several files (similar to JavaDoc).  
In the first case, browsers can have problems to display the file when it
contains many images.

# Java module
This module can analyze Java class files or jar files and construct an analysis
using the following mapping:
- Java **classes**, **enums** and **interfaces** are mapped to Items.
- Java **packages** are mapped to Packages.
- Java **class files**, **folders** and **jars** are mapped to Groups.

Java specific categories (Java.Class, Java.Jar, ...) have been created.  
Java specific features include Static, Final, Abstract, Public, Private, ...

Java fields, methods or constructors are not mapped. It is possible, but
was not useful for the initial objective.

![Image](cdc-deps-java/screenshots/cdc-deps-overview.png)

# Vba module
