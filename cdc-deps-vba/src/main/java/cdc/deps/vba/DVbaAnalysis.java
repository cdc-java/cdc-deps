package cdc.deps.vba;

import cdc.deps.DAnalysis;

public class DVbaAnalysis extends DAnalysis {
    public static final String CATEGORY_GLOBAL = "VBA.Global";
    public static final String CATEGORY_CONTROL = "VBA.Control";
    public static final String CATEGORY_EVENT = "VBA.Event";
    public static final String CATEGORY_FORM = "VBA.Form";
    public static final String CATEGORY_MODULE = "VBA.Module";
    // FormModule ClassModule StdModule ReportModule
    public static final String CATEGORY_PROCEDURE = "VBA.Procedure";
    // Missing Private Public
    public static final String CATEGORY_QUERY = "VBA.Query";
    public static final String CATEGORY_SECTION = "VBA.Section";
    public static final String CATEGORY_SQL = "VBA.SQL";
    public static final String CATEGORY_TABLE = "VBA.Table";
    public static final String CATEGORY_TABLE_FIELD = "VBA.TableField";

    public static final String FEATURE_PUBLIC = "PUBLIC";
    public static final String FEATURE_PRIVATE = "PRIVATE";
    public static final String FEATURE_CLASS = "CLASS";
    public static final String FEATURE_STANDARD = "STANDARD";
    public static final String FEATURE_REPORT = "REPORT";
    public static final String FEATURE_FORM = "FORM";
}