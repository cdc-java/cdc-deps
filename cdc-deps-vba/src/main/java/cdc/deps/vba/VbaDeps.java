package cdc.deps.vba;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.io.xml.DAnalysisXmlWriter;
import cdc.deps.vba.adc.AdcVbaAnalyzer;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.files.Files;

public final class VbaDeps {
    private static final Logger LOGGER = LogManager.getLogger(VbaDeps.class);
    private final MainArgs margs;

    public static final class MainArgs {
        public String objectsFile;
        public String dependenciesFile;
        public Charset charset;
        public String output;
        public Engine engine;

        final FeatureMask<DVbaAnalyzer.Feature> features = new FeatureMask<>();

        public void setEnabled(DVbaAnalyzer.Feature feature,
                               boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public boolean isEnabled(DVbaAnalyzer.Feature feature) {
            return features.isEnabled(feature);
        }
    }

    private VbaDeps(MainArgs margs) {
        this.margs = margs;
    }

    private void execute() throws IOException {
        final DVbaAnalyzer analyzer;
        if (margs.engine == Engine.ADC) {
            analyzer = new AdcVbaAnalyzer();
        } else {
            analyzer = null;
        }
        if (analyzer != null) {
            for (final DVbaAnalyzer.Feature feature : DVbaAnalyzer.Feature.values()) {
                analyzer.setEnabled(feature, margs.isEnabled(feature));
            }
            analyzer.analyze(margs.objectsFile, margs.dependenciesFile, margs.charset);
            analyzer.postAnalysis();
            final File output = new File(margs.output);
            final String path = output.getParent();
            if (path != null) {
                Files.mkdir(path);
            }
            DAnalysisXmlWriter.write(analyzer.getAnalysis(), margs.output);
            LOGGER.info("Generated: {}", margs.output);
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final VbaDeps instance = new VbaDeps(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String OBJECTS = "objects";
        private static final String DEPENDENCIES = "dependencies";
        private static final String ENGINE = "engine";

        public MainSupport() {
            super(VbaDeps.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return cdc.deps.Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option
                                    .builder()
                                    .longOpt(OUTPUT)
                                    .hasArg()
                                    .desc("Optional name of the output filename (default deps.xml).")
                                    .build());

            options.addOption(Option
                                    .builder()
                                    .longOpt(OBJECTS)
                                    .hasArg()
                                    .required()
                                    .desc("Objects input CSV filename.")
                                    .build());

            options.addOption(Option
                                    .builder()
                                    .longOpt(DEPENDENCIES)
                                    .hasArg()
                                    .desc("Dependencies input CSV filename.")
                                    .build());

            options.addOption(Option
                                    .builder()
                                    .longOpt(CHARSET)
                                    .hasArg()
                                    .desc("Charset for input CSV files.")
                                    .build());

            options.addOption(Option
                                    .builder()
                                    .longOpt(ENGINE)
                                    .hasArg()
                                    .desc("Optional engine (default ADC).")
                                    .build());

            addNoArgOptions(options, DVbaAnalyzer.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.output = cl.getOptionValue(OUTPUT, "deps.xml");
            margs.engine = getValueAsEnum(cl, ENGINE, Engine.class, Engine.ADC);
            margs.objectsFile = cl.getOptionValue(OBJECTS);
            margs.dependenciesFile = cl.getOptionValue(DEPENDENCIES);
            margs.charset = getValueAsCharset(cl, CHARSET);

            setMask(cl, DVbaAnalyzer.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            VbaDeps.execute(margs);
            return null;
        }
    }
}