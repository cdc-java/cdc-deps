package cdc.deps.vba.adc;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.DElement;
import cdc.deps.DElementScope;
import cdc.deps.DItem;
import cdc.deps.DNamespace;
import cdc.deps.DPackage;
import cdc.deps.vba.DVbaAnalysis;
import cdc.deps.vba.DVbaAnalyzer;
import cdc.office.csv.CsvParser;
import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.Rows;
import cdc.office.tables.TableHandler;
import cdc.util.function.Evaluation;

/**
 * Implementation of Vba analyzer based on ADC (Access Dependency Checker).
 *
 * @author Damien Carbonne
 *
 */
public class AdcVbaAnalyzer extends DVbaAnalyzer {
    protected static final Logger LOGGER = LogManager.getLogger(DVbaAnalyzer.class);
    final Map<String, DElement> idToElement = new HashMap<>();

    public AdcVbaAnalyzer(DVbaAnalysis analysis) {
        super(analysis);
    }

    public AdcVbaAnalyzer() {
        super();
    }

    @Override
    public void analyze(String objectsFilename,
                        String dependenciesFilename,
                        Charset charset) {
        final CsvParser parser = CsvParser.builder().separator(';').build();
        try {
            idToElement.clear();
            createPackage("0", null, "Global", DVbaAnalysis.CATEGORY_GLOBAL, null);
            parser.parse(new File(objectsFilename),
                         charset,
                         new ObjectsProcessor(),
                         1);
            parser.parse(new File(dependenciesFilename),
                         charset,
                         new DependenciesProcessor(),
                         1);
        } catch (final Exception e) {
            LOGGER.catching(e);
        }
    }

    <T extends DElement> T getElement(String id,
                                      Class<T> klass) {
        final DElement element;
        if (id != null) {
            element = idToElement.get(id);
            if (element == null) {
                LOGGER.error("Could not find element for: {}", id);
            }
        } else {
            element = null;
        }
        if (element != null && !klass.isInstance(element)) {
            LOGGER.error("Problem, can not convert: {} to: {}", element, klass.getCanonicalName());
            return null;
        } else {
            return klass.cast(element);
        }
    }

    private static String toPath(DElement element) {
        if (element == null || DVbaAnalysis.CATEGORY_GLOBAL.equals(element.getCategory())) {
            return "";
        } else {
            return element.getName() + "/";
        }
    }

    static String toFeature(String subtype) {
        final String s = subtype.replace(" ", "").trim();
        if (s.length() == 0) {
            return null;
        } else {
            return s;
        }
    }

    DPackage createPackage(String id,
                           String pid,
                           String name,
                           String category,
                           String feature) {
        final DPackage parent = getElement(pid, DPackage.class);
        final DPackage result = AdcVbaAnalyzer.this.getAnalysis().createPackage(toPath(parent) + name, null, parent);
        assert result != null;
        result.setCategory(category);
        if (feature != null) {
            result.setEnabled(feature, true);
        }
        idToElement.put(id, result);
        return result;
    }

    DItem createItem(String id,
                     String pid,
                     String name,
                     String category,
                     String feature) {
        final DNamespace parent = getElement(pid, DNamespace.class);
        final DItem result = AdcVbaAnalyzer.this.getAnalysis().createItem(toPath(parent) + name, null, parent);
        assert result != null;
        result.setCategory(category);
        if (feature != null) {
            result.setEnabled(feature, true);
        }
        result.setScope(DElementScope.INTERNAL);
        idToElement.put(id, result);
        return result;
    }

    private final class ObjectsProcessor implements TableHandler {
        private int sqlIndex = 0;

        public ObjectsProcessor() {
            super();
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            // Ignore
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            // TODO check header
            return Evaluation.CONTINUE;
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            // ID;Type;SubType;Name;Name4Display;Content;Content4Display;Missing_;ParentID
            final String id = data.getValue(0, null);
            final String type = data.getValue(1, null);
            final String subtype = data.getValue(2, "???");
            final String name = data.getValue(3, "???");
            final String pid = data.getValue(8, null);
            if (pid != null && !"0".equals(pid)) {
                final DElement parent = idToElement.get(pid);
                if (parent == null) {
                    LOGGER.error("Could not find element for: {}", id);
                }
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(Rows.toExtract(data, 4));
            }
            switch (type) {
            case "Module":
                createPackage(id, pid, name, DVbaAnalysis.CATEGORY_MODULE, toFeature(subtype));
                break;

            case "Table":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_TABLE, toFeature(subtype));
                break;

            case "TableField":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_TABLE_FIELD, toFeature(subtype));
                break;

            case "Event":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_EVENT, toFeature(subtype));
                break;

            case "Control":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_CONTROL, toFeature(subtype));
                break;

            case "Query":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_QUERY, toFeature(subtype));
                break;

            case "Form":
                createPackage(id, pid, name, DVbaAnalysis.CATEGORY_FORM, toFeature(subtype));
                break;

            case "Section":
                createPackage(id, pid, name, DVbaAnalysis.CATEGORY_SECTION, toFeature(subtype));
                break;

            case "Procedure":
                createItem(id, pid, name, DVbaAnalysis.CATEGORY_PROCEDURE, toFeature(subtype));
                break;

            case "SQL":
                // Several SQL can have the same name
                sqlIndex++;
                createItem(id, pid, "SQL" + sqlIndex, DVbaAnalysis.CATEGORY_SQL, toFeature(subtype));
                break;

            default:
                LOGGER.error("{} {}  NYI", id, type);
                break;
            }

            return Evaluation.CONTINUE;
        }

        @Override
        public void processEndTable(String name) {
            // Ignore
        }
    }

    private class DependenciesProcessor implements TableHandler {
        public DependenciesProcessor() {
            super();
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            // Ignore
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            return Evaluation.CONTINUE;
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            // Top.ID;Top.Type;Top.SubType;Top.Name;Bottom.ID;Bottom.Type;Bottom.SubType;Bottom.Name;IsTarget;IsChild;Order
            final String isChild = data.getValue(9, null);

            if (!"X".equals(isChild)) {
                final String sid = data.getValue(0, null);
                final String tid = data.getValue(4, null);
                final DElement source = getElement(sid, DElement.class);
                final DElement target = getElement(tid, DElement.class);
                if (source != null && target != null) {
                    getAnalysis().addPrimitiveDependency(source, target);
                } else {
                    // TODO
                    LOGGER.error("Can not link: {} -> {}", sid, tid);
                }
            }

            return Evaluation.CONTINUE;
        }

        @Override
        public void processEndTable(String name) {
            // Ignore
        }
    }
}