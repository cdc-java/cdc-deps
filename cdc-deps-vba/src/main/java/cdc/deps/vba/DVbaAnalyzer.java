package cdc.deps.vba;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import cdc.util.cli.OptionEnum;
import cdc.util.lang.IntMasks;

public abstract class DVbaAnalyzer {
    private final DVbaAnalysis analysis;
    private int features = 0;

    public enum Feature implements OptionEnum {
        VERBOSE("verbose", "Be verbose."),

        DEBUG("debug", "Print debug infos."),

        REMOVE_CONTROLS("remove-controls", "Remove controls and their dependencies."),

        REMOVE_EVENTS("remove-events", "Remove events and their dependencies."),

        REMOVE_FORMS("remove-forms", "Remove forms and their dependencies."),

        REMOVE_PROCEDURES("remove-procedures", "Remove procedures and their dependencies."),

        REMOVE_QUERIES("remove-queries", "Remove queries and their dependencies."),

        REMOVE_SQL("remove-sql", "Remove sql and their dependencies."),

        REMOVE_TABLE_FIELDS("remove-table-fields", "Remove table fields and their dependencies."),

        REMOVE_TABLES("remove-tables", "Remove controls and their dependencies."),

        REMOVE_SECTIONS("remove-sections", "Remove sections and their dependencies."),

        COLLAPSE_CONTROLS("collapse-controls", "Remove non-root controls and propagate their dependencies."),

        COLLAPSE_EVENTS("collapse-events", "Remove non-root events and propagate their dependencies."),

        COLLAPSE_FORMS("collapse-forms", "Remove non-root forms and propagate their dependencies."),

        COLLAPSE_PROCEDURES("collapse-procedures", "Remove non-root procedures and propagate their dependencies."),

        COLLAPSE_QUERIES("collapse-queries", "Remove non-root queries and propagate their dependencies."),

        COLLAPSE_SQL("collapse-sql", "Remove non-root sql and propagate their dependencies."),

        COLLAPSE_TABLE_FIELDS("collapse-table-fields",
                              "Remove non-root table fields and propagate their dependencies."),

        COLLAPSE_TABLES("collapse-tables", "Remove non-root tables and propagate their dependencies."),

        COLLAPSE_SECTIONS("collapse-sections", "Remove non-root sections and propagate their dependencies.");

        private final String name;
        private final String description;

        private Feature(String name,
                        String description) {
            this.name = name;
            this.description = description;
        }

        @Override
        public final String getName() {
            return name;
        }

        @Override
        public final String getDescription() {
            return description;
        }
    }

    protected DVbaAnalyzer(DVbaAnalysis analysis) {
        this.analysis = analysis;
    }

    protected DVbaAnalyzer() {
        this(new DVbaAnalysis());
    }

    public final void setEnabled(Feature feature,
                                 boolean enabled) {
        features = IntMasks.setEnabled(features, feature, enabled);
    }

    public final boolean isEnabled(Feature feature) {
        return IntMasks.isEnabled(features, feature);
    }

    public final DVbaAnalysis getAnalysis() {
        return analysis;
    }

    public abstract void analyze(String objectsFilename,
                                 String dependenciesFilename,
                                 Charset charset);

    public void postAnalysis() {
        final List<String> collapsed = new ArrayList<>();
        if (isEnabled(Feature.COLLAPSE_CONTROLS)) {
            collapsed.add(DVbaAnalysis.CATEGORY_CONTROL + ":");
        }
        if (isEnabled(Feature.COLLAPSE_EVENTS)) {
            collapsed.add(DVbaAnalysis.CATEGORY_EVENT + ":");
        }
        if (isEnabled(Feature.COLLAPSE_FORMS)) {
            collapsed.add(DVbaAnalysis.CATEGORY_FORM + ":");
        }
        if (isEnabled(Feature.COLLAPSE_PROCEDURES)) {
            collapsed.add(DVbaAnalysis.CATEGORY_PROCEDURE + ":");
        }
        if (isEnabled(Feature.COLLAPSE_QUERIES)) {
            collapsed.add(DVbaAnalysis.CATEGORY_QUERY + ":");
        }
        if (isEnabled(Feature.COLLAPSE_SECTIONS)) {
            collapsed.add(DVbaAnalysis.CATEGORY_SECTION + ":");
        }
        if (isEnabled(Feature.COLLAPSE_SQL)) {
            collapsed.add(DVbaAnalysis.CATEGORY_SQL + ":");
        }
        if (isEnabled(Feature.COLLAPSE_TABLES)) {
            collapsed.add(DVbaAnalysis.CATEGORY_TABLE + ":");
        }
        if (isEnabled(Feature.COLLAPSE_TABLE_FIELDS)) {
            collapsed.add(DVbaAnalysis.CATEGORY_TABLE_FIELD + ":");
        }

        final List<String> removed = new ArrayList<>();
        if (isEnabled(Feature.REMOVE_CONTROLS)) {
            removed.add(DVbaAnalysis.CATEGORY_CONTROL + ":");
        }
        if (isEnabled(Feature.REMOVE_EVENTS)) {
            removed.add(DVbaAnalysis.CATEGORY_EVENT + ":");
        }
        if (isEnabled(Feature.REMOVE_FORMS)) {
            removed.add(DVbaAnalysis.CATEGORY_FORM + ":");
        }
        if (isEnabled(Feature.REMOVE_PROCEDURES)) {
            removed.add(DVbaAnalysis.CATEGORY_PROCEDURE + ":");
        }
        if (isEnabled(Feature.REMOVE_QUERIES)) {
            removed.add(DVbaAnalysis.CATEGORY_QUERY + ":");
        }
        if (isEnabled(Feature.REMOVE_SECTIONS)) {
            removed.add(DVbaAnalysis.CATEGORY_SECTION + ":");
        }
        if (isEnabled(Feature.REMOVE_SQL)) {
            removed.add(DVbaAnalysis.CATEGORY_SQL + ":");
        }
        if (isEnabled(Feature.REMOVE_TABLES)) {
            removed.add(DVbaAnalysis.CATEGORY_TABLE + ":");
        }
        if (isEnabled(Feature.REMOVE_TABLE_FIELDS)) {
            removed.add(DVbaAnalysis.CATEGORY_TABLE_FIELD);
        }

        if (!removed.isEmpty()) {
            analysis.removeElements(removed);
        }

        if (!collapsed.isEmpty()) {
            analysis.collapseElements(collapsed);
        }
    }
}
