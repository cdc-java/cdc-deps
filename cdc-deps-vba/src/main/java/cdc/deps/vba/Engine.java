package cdc.deps.vba;

/**
 * Enumeration of possible VBA Dependencies analyzers.
 *
 * @author Damien Carbonne
 *
 */
public enum Engine {
    /**
     * Analyzer built with ADC (Access Dependency Checker).
     */
    ADC
}