package cdc.deps.demos;

import java.io.File;

import cdc.deps.io.html.AbstractDepsToHtml;
import cdc.deps.io.html.AbstractDepsToHtml.MainArgs.Engine;
import cdc.deps.java.DJavaAnalyzer;
import cdc.deps.java.JavaDeps;
import cdc.deps.tools.DepsToHtml;

public class CdcCodeAnalysisDemo {
    private static final File CDCJ = new File("/home/damien/.m2/repository/com/gitlab/cdc-java");

    private static File jar(String domain,
                            String name,
                            String version) {
        return new File(CDCJ, domain + "/cdc-" + domain + "-" + name + "/" + version + "/cdc-" + domain + "-" + name + "-" + version
                + ".jar");
    }

    private static void addJars(JavaDeps.MainArgs margs,
                                String domain,
                                String version,
                                String... names) {
        for (final String name : names) {
            final File file = jar(domain, name, version);
            margs.jarFiles.add(file);
            margs.aliases.put(file.getPath(), "cdc-" + domain + "-" + name);
        }
    }

    private static void analyseCode() throws Exception {
        final JavaDeps.MainArgs margsa = new JavaDeps.MainArgs();
        margsa.output = new File("target/cdc-code-analysis.xml");

        margsa.setEnabled(DJavaAnalyzer.Feature.ADD_PACKAGE_DEPENDENCIES, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.ADD_ROOT_GROUP_DEPENDENCIES, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.COLLAPSE_ANONYMOUS_CLASSES, false);
        margsa.setEnabled(DJavaAnalyzer.Feature.COLLAPSE_HIDDEN_TYPES, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.COLLAPSE_TYPES, false);
        margsa.setEnabled(DJavaAnalyzer.Feature.DEBUG, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.IGNORE_ANNOTATIONS, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.IGNORE_JDK_TYPES, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.IGNORE_PACKAGE_INFO, true);
        margsa.setEnabled(DJavaAnalyzer.Feature.VERBOSE, true);

        // addJars(margsa,
        // "gv",
        // "0.91.0",
        // "api",
        // "tools");
        //
        // addJars(margsa,
        // "office",
        // "0.10.0",
        // "csv",
        // "ss",
        // "ss-csv",
        // "ss-excel",
        // "ss-odf",
        // "tables",
        // "tools");
        //
        // addJars(margsa,
        // "graphs",
        // "0.51.0",
        // "api",
        // "core",
        // "gv",
        // "impl");
        //
        addJars(margsa,
                "util",
                "0.54.0",
                "cli",
                "core");

        addJars(margsa,
                "kernel",
                "0.51.3",
                "args",
                "args-io",
                "converters",
                "converters-io",
                "enums",
                "informers",
                "informers-io",
                "prefs",
                "rids",
                "validation",
                "validation-io");
        //
        // addJars(margsa,
        // "io",
        // "0.10.0",
        // "compress",
        // "data",
        // // "html",
        // "json",
        // "tools",
        // "txt",
        // "utils",
        // "xml");

        // addJars(margsa,
        // "issues",
        // "0.8.0",
        // "api",
        // "core");

        // addJars(margsa,
        // "impex",
        // "0.9.0",
        // "api",
        // "core",
        // "core-json",
        // "core-workbooks",
        // "core-xml",
        // "db");

        addJars(margsa,
                "ui",
                "0.31.3",
                "core",
                "fx",
                "swing");

        JavaDeps.execute(margsa);

        final AbstractDepsToHtml.MainArgs margsb = new AbstractDepsToHtml.MainArgs();
        margsb.input = margsa.output.toURI().toURL();
        margsb.title = "Demo";
        margsb.styles = new File("../cdc-deps-core/src/main/resources/cdc/deps/cdc-deps-styles.xml").toURI().toURL();
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.MULTI, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.MULTI_THREAD, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.NO_EXTERNAL, false);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.NO_UNKNOWN, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.PACKAGES_CLOSURE, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.ROOT_GROUPS_CLOSURE, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.WITH_IMAGES, true);
        margsb.setEnabled(AbstractDepsToHtml.MainArgs.Feature.INVOKE_IF_NEWER, true);

        margsb.outputDir = new File("target/html");
        margsb.engine = Engine.XSLT;
        DepsToHtml.execute(margsb);

        margsb.outputDir = new File("target/htmlf");
        margsb.engine = Engine.HTML_FLOW;
        // DepsToHtml.execute(margsb);

        margsb.outputDir = new File("target/htmlw");
        margsb.engine = Engine.HTML_WRITER;
        DepsToHtml.execute(margsb);
    }

    public static void main(String... args) throws Exception {
        analyseCode();
    }
}