package cdc.deps.demos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.deps.java.DJavaAnalysis;

public final class DJavaAnalysisDemo {
    private static final Logger LOGGER = LogManager.getLogger(DJavaAnalysisDemo.class);

    private DJavaAnalysisDemo() {
    }

    public static void main(String... args) {
        final String[] names = {
                "cdc/util/A$B$1",
                "cdc/util/A",
                "cdc/A",
                "cdc/A$1$2",
                "A1",
                "A"
        };
        for (final String name : names) {
            LOGGER.debug("---------------------------");
            LOGGER.debug(name);
            LOGGER.debug("package: {}", DJavaAnalysis.getPackagePart(name));
            LOGGER.debug("class: {}", DJavaAnalysis.getClassPart(name));
            LOGGER.debug("last class: {}", DJavaAnalysis.getLastClass(name));
            LOGGER.debug("parent: {}", DJavaAnalysis.getParent(name));
            LOGGER.debug("is anonymous: {}", DJavaAnalysis.isAnonymousClass(name));
            LOGGER.debug("is nested: {}", DJavaAnalysis.isNestedClass(name));
            LOGGER.debug("named owner: {}", DJavaAnalysis.getOwningNamedClass(name));
        }
    }
}