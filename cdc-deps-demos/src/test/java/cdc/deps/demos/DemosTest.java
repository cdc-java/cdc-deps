package cdc.deps.demos;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class DemosTest {
    @Test
    void testCdcCodeAnalysisDemo() throws Exception {
        CdcCodeAnalysisDemo.main();
        assertTrue(true);
    }

    @Test
    void testDJavaAnalysisDemo() {
        DJavaAnalysisDemo.main();
        assertTrue(true);
    }
}